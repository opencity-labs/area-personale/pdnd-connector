# Pdnd Connector 

## Contesto

Questo componente è parte del sistema di fruizione dei servizi erogati tramite PDND.  

## Cosa fa 

Il Pdnd Connector implementa la comunicazione tra Stanza Del Cittadino e i servizi erograti tramite pdnd.
Attraverso le sue API è possibile configurare un nuovo client per uno specifico tenant e fruire degli e-service erogati da ANPR

## Struttura del repository
Il repository ha come unico riferimento il branch main. \
Altri File: \
file di configurazione per la continuous integration:\ 
dockerFile, docker-compose.yml, docker-compose.ovverride.yml, gitlab-ci.yml, dockerignore, gitignore\

## Prerequisiti e dipendenze
tutte le librerie esterne utilizzate sono elencate nel file go.mod. La sola presenza di tale file e del suo attuale contenuto permette una gestione automatica delle dipendenze. \
È necessaria l'installazione di Docker e Docker Compose per il deploy automatico del servizio e di tutti gli applicativi accessori

## Monitoring
il sistema usufruisce di Sentry per il monitoring degli errori, inoltre sono esposti i seguenti endpoint per il monitoring : \
- /status : endpoint per l'healtcheck. Resituisce 200
- /metrics : espone le metriche in formato Prometheus

### Configurazione variabili d'ambiente
| Variabile               | Descrizione                  | Valore predefinito | Esempio                   |
|-------------------------|------------------------------|--------------------|---------------------------|
| ENVIRONMENT             | Ambiente di esecuzione       | -                  | local                     |
| DEBUG                   | Attiva la modalità debug    | -                  | false                      |
| HTTP_DEFAULT_INFO       | Attiva la modalità info con le info di default    | -                  | false                      |
| SENTRY_ENABLED          | Abilita l'integrazione Sentry| -                  | false                     |
| SENTRY_TOKEN            | Token Sentry per l'integrazione | -               | test                      |
| HTTP_BIND               | Indirizzo IP per il binding HTTP | 0.0.0.0         | 0.0.0.0                   |
| HTTP_PORT               | Porta HTTP del servizio      | 8000               | 8000                      |
| HTTP_EXTERNAL_BASEPATH  | Percorso base esterno HTTP   | -                  | il tuo endpoint           |
| CACHE_EXPIRATION        | Tempo di scadenza della cache| 5m                 | 5m                        |
| CACHE_EVICTION          | Tempo di evizione della cache| 10m                | 10m                       |
| STORAGE_TYPE            | Tipo di archiviazione         | local              | local                     |
| STORAGE_BUCKET          | Bucket per l'archiviazione    | -                  | testsdc                   |
| STORAGE_BASE_PATH       | Percorso base per l'archiviazione| pdnd_test       | pdnd_test                 |
| STORAGE_LOCAL_PATH      | Percorso locale dell'archiviazione | /data/pdnd      | /data/pdnd                |
| STORAGE_S3_KEY          | Chiave di accesso S3         | -                  | -                         |
| STORAGE_S3_SECRET       | Chiave segreta di accesso S3 | -                  | -                         |
| STORAGE_S3_REGION       | Regione S3                   | -                  | -                         |
| STORAGE_S3_ENDPOINT     | Endpoint personalizzato S3   | -                  | -                         |
| STORAGE_S3_SSL          | Abilita SSL per S3           | -                  | -                         |
| USER_TOKEN_VALIDATION_ENABLED          | abilita il confronto tra CF contenuto nel token e il CF contenuto nel parametro della richiesta GET per la fruizione          | true                  | -    
| VALIDATION_DATA_ENABLED          | se posto  a "false", la post per la validazione dei dati restituirà sempre "true"           | true                 | -    
| PDND_VOUCHER_ENDPOINT          |endpoint per richiedere il voucher pdnd           | https://auth.uat.interop.pagopa.it/token.oauth2                 | https://auth.uat.interop.pagopa.it/token.oauth2   
| PDND_ENV          |ambiente di test o di prod per la piattaforma interoperabilità. due valori accettati: "collaudo", "interoperabilita"(senza accento finale sulla "a")           | collaudo                 | interoperabilita  

## Come si usa
Aggiungere il file docker-compose.override.yml avente seguente struttura : 
```yaml
version: '<la tua versione>'

services:
  pdnd-connector:
    build:
      context: .
    ports:
      - "8000:8000"
    environment:
      ENVIRONMENT: local
      DEBUG: true
      SENTRY_ENABLED: false
      SENTRY_TOKEN: test
      HTTP_BIND: 0.0.0.0
      HTTP_PORT: 8000
      HTTP_EXTERNAL_BASEPATH: 'il tuo endpoint'
      CACHE_EXPIRATION: 5m
      CACHE_EVICTION: 10m
      STORAGE_TYPE: local
      STORAGE_BUCKET: testsdc 
      STORAGE_BASE_PATH: pdnd_test
      STORAGE_LOCAL_PATH: /data/pdnd
      #STORAGE_S3_KEY: 
      #STORAGE_S3_SECRET: 
      #STORAGE_S3_REGION: 
      #STORAGE_S3_ENDPOINT:
      #STORAGE_S3_SSL: 

       
    volumes:
      - ./configurations:/data/pdnd/testsdc

```

Da terminale: 
1. `git clone  git@gitlab.com:opencity-labs/area-personale/pdnd-connector.git`
2. `cd fake-registry-proxy`
3. `docker-compose build`
4. `docker compose up -d`

## Swagger
le API sono documente e consultabili all'indirizzo: 

base url + /swagger/index.html


## Testing
nella gitlab ci sono presenti test end to end
## Stadio di sviluppo

il software si trova in fase `di sviluppo`

## Autori e Riconoscimenti

* Opencity Labs

## License

AGPL 3.0 only

## Contatti

support@opencitylabs.it


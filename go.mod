module gitlab.com/opencontent/stanza-del-cittadino/pdnd-connector

go 1.23

require (
	github.com/getsentry/sentry-go v0.25.0
	github.com/go-chi/chi/v5 v5.0.10
	github.com/prometheus/client_golang v1.17.0
	github.com/rs/zerolog v1.31.0
	github.com/satori/go.uuid v1.2.0
	github.com/sethvargo/go-envconfig v0.9.0
	github.com/swaggest/openapi-go v0.2.42
	github.com/swaggest/rest v0.2.59
	github.com/swaggest/swgui v1.7.4
	github.com/swaggest/usecase v1.2.1
)

require (
	github.com/Azure/azure-sdk-for-go v32.5.0+incompatible // indirect
	github.com/Azure/go-autorest/autorest v0.9.0 // indirect
	github.com/Azure/go-autorest/autorest/adal v0.5.0 // indirect
	github.com/Azure/go-autorest/autorest/date v0.1.0 // indirect
	github.com/Azure/go-autorest/logger v0.1.0 // indirect
	github.com/Azure/go-autorest/tracing v0.5.0 // indirect
	github.com/aws/aws-sdk-go v1.23.4 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/golang/mock v1.6.0 // indirect
	github.com/jmespath/go-jmespath v0.0.0-20180206201540-c2b33e8439af // indirect
	golang.org/x/exp v0.0.0-20230315142452-642cacee5cc0 // indirect
)

require (
	github.com/beorn7/perks v1.0.1 // indirect
	github.com/cespare/xxhash/v2 v2.2.0 // indirect
	github.com/eko/gocache/lib/v4 v4.1.5
	github.com/eko/gocache/store/go_cache/v4 v4.2.1
	github.com/gofrs/uuid v4.4.0+incompatible
	github.com/golang-jwt/jwt v3.2.2+incompatible
	github.com/golang-jwt/jwt/v4 v4.5.0
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/google/uuid v1.6.0
	github.com/graymeta/stow v0.2.8
	github.com/kr/text v0.2.0 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.19 // indirect
	github.com/matttproud/golang_protobuf_extensions v1.0.4 // indirect
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/pkg/errors v0.9.1 // indirect
	github.com/prometheus/client_model v0.4.1-0.20230718164431-9a2bf3000d16 // indirect
	github.com/prometheus/common v0.44.0 // indirect
	github.com/prometheus/procfs v0.11.1 // indirect
	github.com/santhosh-tekuri/jsonschema/v3 v3.1.0 // indirect
	github.com/swaggest/form/v5 v5.1.1 // indirect
	github.com/swaggest/jsonschema-go v0.3.62 // indirect
	github.com/swaggest/refl v1.3.0 // indirect
	github.com/vearutop/statigz v1.4.0 // indirect
	golang.org/x/sys v0.12.0 // indirect
	golang.org/x/text v0.13.0 // indirect
	google.golang.org/protobuf v1.31.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)

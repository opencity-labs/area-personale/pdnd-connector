# Docs: https://hurl.dev/docs/entry.html

################################
#                              #
#              SETUP           #
#                              #
################################
POST https://servizi.comune-qa.bugliano.pi.it/lang/api/auth
```json
{
    "username": "{{admin_user_1}}",
    "password": "{{admin_psw_1}}"
}

```
#expected result 
HTTP/2 200
[Captures]
token_admin_tenant_1: jsonpath "$.token"

POST https://qa.genova.opencityitalia.it/app/api/auth
```json
{
    "username": "{{admin_user_2}}",
    "password": "{{admin_psw_2}}"
}

```
#expected result 
HTTP/2 200
[Captures]
token_admin_tenant_2: jsonpath "$.token"

POST https://servizi.comune-qa.bugliano.pi.it/lang/api/session-auth
```json
{
    "username": "{{user_user_1}}",
    "password": "{{user_psw_1}}"
}

```
#expected result 
HTTP/2 200
[Captures]
token_user_tenant_1: jsonpath "$.token"


#cleaning env
DELETE {{base_url}}/tenants/60e35f02-1509-408c-b101-3b1a28109329
Authorization: {{token_admin_tenant_1}}



POST {{base_url}}/tenants/
Authorization: {{token_admin_tenant_1}}
```json
{
    "id": "60e35f02-1509-408c-b101-3b1a28109329",
    "name": "tenant Roma",
    "ipa_code":"123456"
}

```
#expected result 
HTTP/1.1 201
[Asserts] 
jsonpath "$.id" == "60e35f02-1509-408c-b101-3b1a28109329"
jsonpath "$.name" == "tenant Roma"
jsonpath "$.ipa_code" == "123456"
# checks for the ISO 8601 date and time format including timezone offset
jsonpath "$.created_at" matches /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\+\d{2}:\d{2}$/
jsonpath "$.updated_at" matches /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\+\d{2}:\d{2}$/
[Captures]
tenant_1: jsonpath "$.id"
################################
#                              #
#          END SETUP           #
#                              #
################################


################################
#                              #
#           OPTIONS            #
#                              #
################################
#check option with parameter
OPTIONS {{base_url}}/tenants/{{tenant_1}}
Authorization: {{token_admin_tenant_1}}
#expected result 
HTTP/1.1 200

#check option without parameter
OPTIONS {{base_url}}/tenants
Authorization: {{token_admin_tenant_1}}

#expected result 
HTTP/1.1 200

################################
#              OPTIONS         #
#          token checks        #
################################


OPTIONS {{base_url}}/tenants/{{tenant_1}}

#expected result 
HTTP/1.1 401
[Asserts] 
jsonpath "$.detail" == "unauthenticated: empty token"
jsonpath "$.type" == "https://httpstatuses.io/401"
jsonpath "$.status" == 401
jsonpath "$.title" == "UNAUTHENTICATED"
jsonpath "$.instance" == "/tenants/{{tenant_1}}"

OPTIONS {{base_url}}/tenants/{{tenant_1}}
Authorization: {{token_user_tenant_1}}

#expected result 
HTTP/1.1 403
[Asserts] 
jsonpath "$.detail" == "permission denied: wrong role"
jsonpath "$.type" == "https://httpstatuses.io/403"
jsonpath "$.status" == 403
jsonpath "$.title" == "PERMISSION_DENIED"
jsonpath "$.instance" == "/tenants/{{tenant_1}}"


OPTIONS {{base_url}}/tenants/{{tenant_1}}
Authorization: eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE3MTE0NjIzNzUsImV4cCI6MTcxMjMyNjM3NSwicm9sZXMiOlsiUk9MRV9BRE1JTiJdLCJ1c2VybmFtZSI6ImFwaSIsImlkIjoiNjMyNmNjZWEtMGYxMC00NzMxLTk4MTgtMzMxNmJmOTFjYWYwIiwidGVuYW50X2lkIjoiNjBlMzVmMDItMTUwOS00MDhjLWIxMDEtM2IxYTI4MTA5MzI5In0.KkDAQWsUhS11ZTuHvJG6WRCJDo_adrArTZKUaQkflNlGEnQgyR4e7MSDjrJm3YpTQ909DZs4k3u4EirMCq77PMxJWXWhodo62ulvdDwOpVl9Hp-ri0glZ5q3VSO77vfk1PIAjnZ-tXGAvx-A-dihi-czrO_Lelnspkwg9svx_nrzO8eFXSnC2Y97lL7yrgMGglj4E3h7ZeoeBolxYgqkJ3jSKjZMLCteIohTMa9JHj2UkWKQ0g8lXwhtV73VIoLgGoqEF0mdVmtRev_EJEaO2JUcbUvYK-4xzT_lCe3X5xeh9KeK-z-4ruxbM8J8uwePvZxIFsIo0QN-UlZjFmYOyuw8hITWjEeGG6X7pHqgsAXxdM23GBUcR65ppoIYCx1JppYT6Tb7tfY_zp56vQV8EV9m1z6Nq70zSwJADjV-PujIxTcqB6sdhlsv9a7oiIsdHmxjz66PR4dxGLVYhDNks5Ct-wETg99Dger_NZhFEBuHceEFWjZTUwEsMDiK_pqRIirs9S3jpycxmKFSiKt0qPxQo5RH5W0umOKnPYRb3YQTukrZfc4scHRHlcdDay_Jj6ibcmcskAj5F332NHIpm0vsBOw-_aljgrXut19AOxW--RLKk-rCIb1pz6pGmkzwgjQVbX45yYb4ZqvG-zDLkoXjMcBGweUygnIBYH5bAN8

#expected result 
HTTP/1.1 403
[Asserts] 
jsonpath "$.detail" == "permission denied: jwt has expired"
jsonpath "$.type" == "https://httpstatuses.io/403"
jsonpath "$.status" == 403
jsonpath "$.title" == "PERMISSION_DENIED"
jsonpath "$.instance" == "/tenants/{{tenant_1}}"


################################
#                              #
#           TEARDOWN           #
#                              #
################################



DELETE {{base_url}}/tenants/{{tenant_1}}
Authorization: {{token_admin_tenant_1}}
HTTP/1.1 204
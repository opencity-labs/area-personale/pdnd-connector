# Docs: https://hurl.dev/docs/entry.html

################################
#                              #
#              SETUP           #
#                              #
################################
POST https://servizi.comune-qa.bugliano.pi.it/lang/api/auth
```json
{
    "username": "{{admin_user_1}}",
    "password": "{{admin_psw_1}}"
}

```
#expected result 
HTTP/2 200
[Captures]
token_admin_tenant_1: jsonpath "$.token"

#cleaning env
DELETE {{base_url}}/tenants/60e35f02-1509-408c-b101-3b1a28109329
Authorization: {{token_admin_tenant_1}}


POST https://qa.genova.opencityitalia.it/app/api/auth
```json
{
    "username": "{{admin_user_2}}",
    "password": "{{admin_psw_2}}"
}

```
#expected result 
HTTP/2 200
[Captures]
token_admin_tenant_2: jsonpath "$.token"

POST https://servizi.comune-qa.bugliano.pi.it/lang/api/session-auth
```json
{
    "username": "{{user_user_1}}",
    "password": "{{user_psw_1}}"
}

```
#expected result 
HTTP/2 200
[Captures]
token_user_tenant_1: jsonpath "$.token"

#cleaning env
DELETE {{base_url}}/tenants/60e35f02-1509-408c-b101-3b1a28109329
Authorization: {{token_admin_tenant_1}}


POST {{base_url}}/tenants/
Authorization: {{token_admin_tenant_1}}
```json
{
    "id": "60e35f02-1509-408c-b101-3b1a28109329",
    "name": "tenant Roma",
    "ipa_code":"123456"
}

```
#expected result 
HTTP/1.1 201
[Captures]
tenant_1: jsonpath "$.id"


#retireving new key 
GET {{base_url}}/tenants/{{tenant_1}}/keys
Authorization: {{token_admin_tenant_1}}
#expected result
HTTP/1.1 200
[Asserts] 
jsonpath "$.public_key" exists
jsonpath "$.id" exists
[Captures]
key_1: jsonpath "$.id"

#creating no-existing client
POST {{base_url}}/tenants/{{tenant_1}}/clients
Authorization: {{token_admin_tenant_1}}
```json
{
  "id": "a8f8501b-6ed6-4866-b64d-f1c3d26684ac",
  "name": "client di test",
  "env": "collaudo",
  "key_id": "KID test",
  "key_pair_id": "{{key_1}}"
}
```
#expected result 
HTTP/1.1 201
[Captures]
client_1: jsonpath "$.id"

################################
#                              #
#          END SETUP           #
#                              #
################################


################################
#                              #
# POST  no-existing configs    #
#                              #
################################

#creating no-existing configs
POST {{base_url}}/tenants/{{tenant_1}}/configs
Authorization: {{token_admin_tenant_1}}
```json
{
  "client_id": "{{client_1}}",
  "eservice_id": "a6fcd036-f1a2-4df5-b986-7410e9e0e97a",
  "purpose_id": "106388f6-323c-4d1c-80ab-3dd994370440",
  "is_active": true
}
```
#expected result 
HTTP/1.1 201
[Asserts]
jsonpath "$.id" exists
jsonpath "$.client_id" == "{{client_1}}"
jsonpath "$.eservice_id" == "a6fcd036-f1a2-4df5-b986-7410e9e0e97a"
jsonpath "$.purpose_id" == "106388f6-323c-4d1c-80ab-3dd994370440"
jsonpath "$.tenant_id" == "{{tenant_1}}"
jsonpath "$.created_at" matches /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\+\d{2}:\d{2}$/
jsonpath "$.updated_at" matches /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\+\d{2}:\d{2}$/
[Captures]
config_1: jsonpath "$.id"


################################
#                              #
#              POST            #
#   configs invalid client     #
################################

POST {{base_url}}/tenants/{{tenant_1}}/configs
Authorization: {{token_admin_tenant_1}}
```json
{
  "client_id": "b3d79dcc-773e-41b4-8984-ffe8668cd0d6",
  "eservice_id": "a6fcd036-f1a2-4df5-b986-7410e9e0e97a",
  "purpose_id": "106388f6-323c-4d1c-80ab-3dd994370440",
  "is_active": true
}
```
#expected result 
HTTP/1.1 400
[Asserts] 
jsonpath "$.detail" == "invalid argument: client does not belong to any client"
jsonpath "$.type" == "https://httpstatuses.io/400"
jsonpath "$.status" == 400
jsonpath "$.title" == "INVALID_ARGUMENT"
jsonpath "$.instance" == "/tenants/{{tenant_1}}/configs"

################################
#                              #
#              POST            #
#       invalid purpose id     #
################################

#creating a configs invalid purpose id
POST {{base_url}}/tenants/{{tenant_1}}/configs
Authorization: {{token_admin_tenant_1}}
```json
{
  "client_id": "{{client_1}}",
  "eservice_id": "a6fcd036-f1a2-4df5-b986-7410e9e0e97a",
  "purpose_id": "106388f6-323c-4d1c-80ab-3dd994370440-111",
  "is_active": true
}
```
#expected result 
HTTP/1.1 400
[Asserts] 
jsonpath "$.detail" == "invalid argument: purpose_id uuid: incorrect UUID length 40 in string \"106388f6-323c-4d1c-80ab-3dd994370440-111\""
jsonpath "$.type" == "https://httpstatuses.io/400"
jsonpath "$.status" == 400
jsonpath "$.title" == "INVALID_ARGUMENT"
jsonpath "$.instance" == "/tenants/{{tenant_1}}/configs"

################################
#              POST             #
#          token checks        #
################################


POST {{base_url}}/tenants/{{tenant_1}}/configs/

```json
{
  "client_id": "{{client_1}}",
  "eservice_id": "a6fcd036-f1a2-4df5-b986-7410e9e0e97a",
  "purpose_id": "106388f6-323c-4d1c-80ab-3dd994370440",
  "is_active": true
}
```
#expected result 
HTTP/1.1 401
[Asserts] 
jsonpath "$.detail" == "unauthenticated: empty token"
jsonpath "$.type" == "https://httpstatuses.io/401"
jsonpath "$.status" == 401
jsonpath "$.title" == "UNAUTHENTICATED"
jsonpath "$.instance" == "/tenants/{{tenant_1}}/configs/"

POST {{base_url}}/tenants/{{tenant_1}}/configs/
Authorization: {{token_user_tenant_1}}
```json
{
  "client_id": "{{client_1}}",
  "eservice_id": "a6fcd036-f1a2-4df5-b986-7410e9e0e97a",
  "purpose_id": "106388f6-323c-4d1c-80ab-3dd994370440",
  "is_active": true
}
```

#expected result 
HTTP/1.1 403
[Asserts] 
jsonpath "$.detail" == "permission denied: wrong role"
jsonpath "$.type" == "https://httpstatuses.io/403"
jsonpath "$.status" == 403
jsonpath "$.title" == "PERMISSION_DENIED"
jsonpath "$.instance" == "/tenants/{{tenant_1}}/configs/"


POST {{base_url}}/tenants/{{tenant_1}}/configs/
Authorization: eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE3MTE0NjIzNzUsImV4cCI6MTcxMjMyNjM3NSwicm9sZXMiOlsiUk9MRV9BRE1JTiJdLCJ1c2VybmFtZSI6ImFwaSIsImlkIjoiNjMyNmNjZWEtMGYxMC00NzMxLTk4MTgtMzMxNmJmOTFjYWYwIiwidGVuYW50X2lkIjoiNjBlMzVmMDItMTUwOS00MDhjLWIxMDEtM2IxYTI4MTA5MzI5In0.KkDAQWsUhS11ZTuHvJG6WRCJDo_adrArTZKUaQkflNlGEnQgyR4e7MSDjrJm3YpTQ909DZs4k3u4EirMCq77PMxJWXWhodo62ulvdDwOpVl9Hp-ri0glZ5q3VSO77vfk1PIAjnZ-tXGAvx-A-dihi-czrO_Lelnspkwg9svx_nrzO8eFXSnC2Y97lL7yrgMGglj4E3h7ZeoeBolxYgqkJ3jSKjZMLCteIohTMa9JHj2UkWKQ0g8lXwhtV73VIoLgGoqEF0mdVmtRev_EJEaO2JUcbUvYK-4xzT_lCe3X5xeh9KeK-z-4ruxbM8J8uwePvZxIFsIo0QN-UlZjFmYOyuw8hITWjEeGG6X7pHqgsAXxdM23GBUcR65ppoIYCx1JppYT6Tb7tfY_zp56vQV8EV9m1z6Nq70zSwJADjV-PujIxTcqB6sdhlsv9a7oiIsdHmxjz66PR4dxGLVYhDNks5Ct-wETg99Dger_NZhFEBuHceEFWjZTUwEsMDiK_pqRIirs9S3jpycxmKFSiKt0qPxQo5RH5W0umOKnPYRb3YQTukrZfc4scHRHlcdDay_Jj6ibcmcskAj5F332NHIpm0vsBOw-_aljgrXut19AOxW--RLKk-rCIb1pz6pGmkzwgjQVbX45yYb4ZqvG-zDLkoXjMcBGweUygnIBYH5bAN8
```json
{
  "client_id": "{{client_1}}",
  "eservice_id": "a6fcd036-f1a2-4df5-b986-7410e9e0e97a",
  "purpose_id": "106388f6-323c-4d1c-80ab-3dd994370440",
  "is_active": true
}
```

#expected result 
HTTP/1.1 403
[Asserts] 
jsonpath "$.detail" == "permission denied: jwt has expired"
jsonpath "$.type" == "https://httpstatuses.io/403"
jsonpath "$.status" == 403
jsonpath "$.title" == "PERMISSION_DENIED"
jsonpath "$.instance" == "/tenants/{{tenant_1}}/configs/"


################################
#                              #
#           TEARDOWN           #
#                              #
################################


DELETE {{base_url}}/tenants/{{tenant_1}}/keys/{{key_1}}
Authorization: {{token_admin_tenant_1}}
HTTP/1.1 204

DELETE {{base_url}}/tenants/{{tenant_1}}/configs/{{config_1}}
Authorization: {{token_admin_tenant_1}}
HTTP/1.1 204

DELETE {{base_url}}/tenants/{{tenant_1}}/clients/{{client_1}}
Authorization: {{token_admin_tenant_1}}
HTTP/1.1 204

DELETE {{base_url}}/tenants/{{tenant_1}}
Authorization: {{token_admin_tenant_1}}
HTTP/1.1 204

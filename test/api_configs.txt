# Docs: https://hurl.dev/docs/entry.html

################################
#                              #
#              SETUP           #
#                              #
################################
POST https://servizi.comune-qa.bugliano.pi.it/lang/api/auth
```json
{
    "username": "{{admin_user_1}}",
    "password": "{{admin_psw_1}}"
}

```
#expected result 
HTTP/2 200
[Captures]
token_admin_tenant_1: jsonpath "$.token"

POST https://qa.genova.opencityitalia.it/app/api/auth
```json
{
    "username": "{{admin_user_2}}",
    "password": "{{admin_psw_2}}"
}

```
#expected result 
HTTP/2 200
[Captures]
token_admin_tenant_2: jsonpath "$.token"

POST https://servizi.comune-qa.bugliano.pi.it/lang/api/session-auth
```json
{
    "username": "{{user_user_1}}",
    "password": "{{user_psw_1}}"
}

```
#expected result 
HTTP/2 200
[Captures]
token_user_tenant_1: jsonpath "$.token"

#cleaning env
DELETE {{base_url}}/tenants/60e35f02-1509-408c-b101-3b1a28109329
Authorization: {{token_admin_tenant_1}}


POST {{base_url}}/tenants/
Authorization: {{token_admin_tenant_1}}
```json
{
    "id": "60e35f02-1509-408c-b101-3b1a28109329",
    "name": "tenant Roma",
    "ipa_code":"123456"
    
}

```
#expected result 
HTTP/1.1 201
[Asserts] 
jsonpath "$.id" == "60e35f02-1509-408c-b101-3b1a28109329"
jsonpath "$.name" == "tenant Roma"
jsonpath "$.ipa_code" == "123456"
# checks for the ISO 8601 date and time format including timezone offset
jsonpath "$.created_at" matches /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\+\d{2}:\d{2}$/
jsonpath "$.updated_at" matches /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\+\d{2}:\d{2}$/
[Captures]
created_at: jsonpath "$.created_at"
tenant_1: jsonpath "$.id"

#retireving new key 
GET {{base_url}}/tenants/{{tenant_1}}/keys
Authorization: {{token_admin_tenant_1}}
#expected result
HTTP/1.1 200
[Asserts] 
jsonpath "$.public_key" exists
jsonpath "$.id" exists
[Captures]
existingKeysId: jsonpath "$.id"

#creating not-existing client
POST {{base_url}}/tenants/{{tenant_1}}/clients
Authorization: {{token_admin_tenant_1}}
```json
{
  "id": "b3d79dcc-773e-41b4-8984-ffe8668cd0d6",
  "name": "client di test",
  "env": "collaudo",
  "key_id": "KID test",
  "key_pair_id": "{{existingKeysId}}"
}
```
#expected result 
HTTP/1.1 201

#                              #
#       GET BY configs ID      #
#                              #
#getting no-existing configs
GET {{base_url}}/tenants/{{tenant_1}}/configs/fc9ba78d-073a-4f4c-a96b-ce300ec1922b
Authorization: {{token_admin_tenant_1}}

#expected result 
HTTP/1.1 404
[Asserts] 
jsonpath "$.detail" == "not found"
jsonpath "$.type" == "https://httpstatuses.io/404"
jsonpath "$.status" == 404
jsonpath "$.title" == "NOT_FOUND"
jsonpath "$.instance" == "/tenants/{{tenant_1}}/configs/fc9ba78d-073a-4f4c-a96b-ce300ec1922b"

#                              #
#       POST configs ID        #
#                              #
#creating no-existing configs
POST {{base_url}}/tenants/{{tenant_1}}/configs
Authorization: {{token_admin_tenant_1}}
```json
{
  "client_id": "b3d79dcc-773e-41b4-8984-ffe8668cd0d6",
  "eservice_id": "a6fcd036-f1a2-4df5-b986-7410e9e0e97a",
  "purpose_id": "106388f6-323c-4d1c-80ab-3dd994370440",
  "is_active": true
}
```
#expected result 
HTTP/1.1 201
[Asserts]
jsonpath "$.id" exists
jsonpath "$.client_id" == "b3d79dcc-773e-41b4-8984-ffe8668cd0d6"
jsonpath "$.eservice_id" == "a6fcd036-f1a2-4df5-b986-7410e9e0e97a"
jsonpath "$.purpose_id" == "106388f6-323c-4d1c-80ab-3dd994370440"
jsonpath "$.tenant_id" == "{{tenant_1}}"
jsonpath "$.created_at" matches /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\+\d{2}:\d{2}$/
jsonpath "$.updated_at" matches /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\+\d{2}:\d{2}$/
[Captures]
config_1: jsonpath "$.id"





#                              #
#       GET BY configs ID      #
#                              #
#getting configs
GET {{base_url}}/tenants/{{tenant_1}}/configs/{{config_1}}
Authorization: {{token_admin_tenant_1}}

#expected result 
HTTP/1.1 200
[Asserts]
jsonpath "$.id" =={{config_1}}
jsonpath "$.client_id" == "b3d79dcc-773e-41b4-8984-ffe8668cd0d6"
jsonpath "$.eservice_id" == "a6fcd036-f1a2-4df5-b986-7410e9e0e97a"
jsonpath "$.purpose_id" == "106388f6-323c-4d1c-80ab-3dd994370440"
jsonpath "$.tenant_id" == "{{tenant_1}}"
jsonpath "$.is_active" == true
jsonpath "$.created_at" matches /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\+\d{2}:\d{2}$/
jsonpath "$.updated_at" matches /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\+\d{2}:\d{2}$/

#getting configs
GET {{base_url}}/tenants/{{tenant_1}}/configs
Authorization: {{token_admin_tenant_1}}

#expected result 
HTTP/1.1 200
[Asserts]
jsonpath "$.meta.page.offset" == 0
jsonpath "$.meta.total" == 1


#creating no-existing configs
POST {{base_url}}/tenants/{{tenant_1}}/configs
Authorization: {{token_admin_tenant_1}}
```json
{
  "client_id": "b3d79dcc-773e-41b4-8984-ffe8668cd0d6",
  "eservice_id": "a6fcd036-f1a2-4df5-b986-7410e9e0e97a",
  "purpose_id": "106388f6-323c-4d1c-80ab-3dd994370440",
  "is_active": true
}
```
#expected result 
HTTP/1.1 201
[Asserts]
jsonpath "$.id" exists
jsonpath "$.client_id" == "b3d79dcc-773e-41b4-8984-ffe8668cd0d6"
jsonpath "$.eservice_id" == "a6fcd036-f1a2-4df5-b986-7410e9e0e97a"
jsonpath "$.purpose_id" == "106388f6-323c-4d1c-80ab-3dd994370440"
jsonpath "$.tenant_id" == "{{tenant_1}}"
jsonpath "$.created_at" matches /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\+\d{2}:\d{2}$/
jsonpath "$.updated_at" matches /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\+\d{2}:\d{2}$/
[Captures]
config_2: jsonpath "$.id"

#                              #
#       GET BY configs ID      #
#                              #
#getting configs
GET {{base_url}}/tenants/{{tenant_1}}/configs/{{config_2}}
Authorization: {{token_admin_tenant_1}}

#expected result 
HTTP/1.1 200
[Asserts]
jsonpath "$.id" =={{config_2}}
jsonpath "$.client_id" == "b3d79dcc-773e-41b4-8984-ffe8668cd0d6"
jsonpath "$.eservice_id" == "a6fcd036-f1a2-4df5-b986-7410e9e0e97a"
jsonpath "$.purpose_id" == "106388f6-323c-4d1c-80ab-3dd994370440"
jsonpath "$.tenant_id" == "{{tenant_1}}"
jsonpath "$.is_active" == true
jsonpath "$.created_at" matches /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\+\d{2}:\d{2}$/
jsonpath "$.updated_at" matches /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\+\d{2}:\d{2}$/

#getting configs
GET {{base_url}}/tenants/{{tenant_1}}/configs
Authorization: {{token_user_tenant_1}}

#expected result 
HTTP/1.1 200
[Asserts]
jsonpath "$.meta.page.offset" == 0
jsonpath "$.meta.total" == 2

GET {{base_url}}/tenants/{{tenant_1}}/configs/{{config_2}}
Authorization: {{token_admin_tenant_1}}

#expected result 
HTTP/1.1 200
[Asserts]
jsonpath "$.id" =={{config_2}}
jsonpath "$.client_id" == "b3d79dcc-773e-41b4-8984-ffe8668cd0d6"
jsonpath "$.eservice_id" == "a6fcd036-f1a2-4df5-b986-7410e9e0e97a"
jsonpath "$.purpose_id" == "106388f6-323c-4d1c-80ab-3dd994370440"
jsonpath "$.tenant_id" == "{{tenant_1}}"
jsonpath "$.is_active" == true
jsonpath "$.created_at" matches /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\+\d{2}:\d{2}$/
jsonpath "$.updated_at" matches /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\+\d{2}:\d{2}$/

#getting configs
GET {{base_url}}/tenants/{{tenant_1}}/configs
Authorization: {{token_admin_tenant_1}}

#expected result 
HTTP/1.1 200
[Asserts]
jsonpath "$.meta.page.offset" == 0
jsonpath "$.meta.total" == 2


#                              #
#              DELETE          #
#                              #
#deleting no-existing configs 
DELETE {{base_url}}/tenants/{{tenant_1}}/configs/fc9ba78d-073a-4f4c-a96b-ce300ec1922b
Authorization: {{token_admin_tenant_1}}

#expected result
HTTP/1.1 404
[Asserts] 
jsonpath "$.detail" == "not found"
jsonpath "$.type" == "https://httpstatuses.io/404"
jsonpath "$.status" == 404
jsonpath "$.title" == "NOT_FOUND"
jsonpath "$.instance" == "/tenants/{{tenant_1}}/configs/fc9ba78d-073a-4f4c-a96b-ce300ec1922b"



#deleting configs - invalid argument 
DELETE {{base_url}}/tenants/60e35f02-1509-408c-b101-123456789002-11111/configs/fc9ba78d-073a-4f4c-a96b-ce300ec1922a
Authorization: {{token_admin_tenant_1}}
#expected result
HTTP/1.1 403
[Asserts] 
jsonpath "$.detail" == "permission denied: wrong tenant"
jsonpath "$.type" == "https://httpstatuses.io/403"
jsonpath "$.status" == 403
jsonpath "$.title" == "PERMISSION_DENIED"
jsonpath "$.instance" == "/tenants/60e35f02-1509-408c-b101-123456789002-11111/configs/fc9ba78d-073a-4f4c-a96b-ce300ec1922a"


################################
#                              #
#              TEARDOWN        #
#                              #
################################
DELETE {{base_url}}/tenants/{{tenant_1}}/keys/{{existingKeysId}}
Authorization: {{token_admin_tenant_1}}
HTTP/1.1 204

DELETE {{base_url}}/tenants/{{tenant_1}}/clients/b3d79dcc-773e-41b4-8984-ffe8668cd0d6
Authorization: {{token_admin_tenant_1}}
HTTP/1.1 204

DELETE {{base_url}}/tenants/{{tenant_1}}
Authorization: {{token_admin_tenant_1}}
HTTP/1.1 204


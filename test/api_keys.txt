# Docs: https://hurl.dev/docs/entry.html


###############################
#                             #
#            SETUP            #
###############################

POST https://servizi.comune-qa.bugliano.pi.it/lang/api/auth
```json
{
    "username": "{{admin_user_1}}",
    "password": "{{admin_psw_1}}"
}

```
#expected result 
HTTP/2 200
[Captures]
token_admin_tenant_1: jsonpath "$.token"

POST https://qa.genova.opencityitalia.it/app/api/auth
```json
{
    "username": "{{admin_user_2}}",
    "password": "{{admin_psw_2}}"
}

```
#expected result 
HTTP/2 200
[Captures]
token_admin_tenant_2: jsonpath "$.token"

POST https://servizi.comune-qa.bugliano.pi.it/lang/api/session-auth
```json
{
    "username": "{{user_user_1}}",
    "password": "{{user_psw_1}}"
}

```
#expected result 
HTTP/2 200
[Captures]
token_user_tenant_1: jsonpath "$.token"

#cleaning env
DELETE {{base_url}}/tenants/60e35f02-1509-408c-b101-3b1a28109329
Authorization: {{token_admin_tenant_1}}


POST {{base_url}}/tenants/
Authorization: {{token_admin_tenant_1}}
```json
{
    "id": "60e35f02-1509-408c-b101-3b1a28109329",
    "name": "tenant Roma",
    "ipa_code":"123456"
}

```
#expected result 
HTTP/1.1 201
[Asserts] 
jsonpath "$.id" == "60e35f02-1509-408c-b101-3b1a28109329"
jsonpath "$.name" == "tenant Roma"
# checks for the ISO 8601 date and time format including timezone offset
jsonpath "$.created_at" matches /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\+\d{2}:\d{2}$/
jsonpath "$.updated_at" matches /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\+\d{2}:\d{2}$/
[Captures]
tenant_1: jsonpath "$.id"


POST {{base_url}}/tenants/
Authorization: {{token_admin_tenant_2}}
```json
{
    "id": "8aa682f6-f192-11ed-a05b-0242ac120003",
    "name": "tenant Roma",
    "ipa_code":"123456"
}

```
#expected result 
HTTP/1.1 201
[Asserts] 
jsonpath "$.id" == "8aa682f6-f192-11ed-a05b-0242ac120003"
jsonpath "$.name" == "tenant Roma"
# checks for the ISO 8601 date and time format including timezone offset
jsonpath "$.created_at" matches /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\+\d{2}:\d{2}$/
jsonpath "$.updated_at" matches /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\+\d{2}:\d{2}$/
[Captures]
tenant_2: jsonpath "$.id"


################################
#                              #
#              DELETE          #
#  deleting no-existing keys   #
################################ 
DELETE {{base_url}}/tenants/{{tenant_2}}/keys/b2756823-ba98-49c8-8b0e-ee7826cd1495
Authorization: {{token_admin_tenant_2}}
#expected result
HTTP/1.1 404
[Asserts] 
jsonpath "$.detail" == "not found"
jsonpath "$.type" == "https://httpstatuses.io/404"
jsonpath "$.status" == 404
jsonpath "$.title" == "NOT_FOUND"
jsonpath "$.instance" == "/tenants/{{tenant_2}}/keys/b2756823-ba98-49c8-8b0e-ee7826cd1495"


################################
#                              #
#         GET new key          #
#                              #
################################

GET {{base_url}}/tenants/{{tenant_1}}/keys
Authorization: {{token_admin_tenant_1}}

#expected result
HTTP/1.1 200
[Asserts] 
jsonpath "$.public_key" exists
jsonpath "$.id" exists
[Captures]
keysid: jsonpath "$.id"

#######################################################################
#                                                                     #
#                     GET new key                                     #
# should retrieve the previous because no associated to a client yet #
#######################################################################
GET {{base_url}}/tenants/{{tenant_1}}/keys
Authorization: {{token_admin_tenant_1}}

#expected result
HTTP/1.1 200
[Asserts] 
jsonpath "$.public_key" exists
jsonpath "$.id" == {{keysid}}

################################
#                              #
#         GET new key          #
#   for a different tenant     #
################################
GET {{base_url}}/tenants/{{tenant_2}}/keys
Authorization: {{token_admin_tenant_2}}
#expected result
HTTP/1.1 200
[Asserts] 
[Asserts] 
jsonpath "$.public_key" exists
jsonpath "$.id" != {{keysid}}
[Captures]
keysid_2: jsonpath "$.id"

#######################################################################
#                                                                     #
#                     GET new key                                     #
# should retrieve a new one because the previous has been             #
# associated to a client                                              #
#######################################################################
POST {{base_url}}/tenants/{{tenant_1}}/clients
Authorization: {{token_admin_tenant_1}}

```json
{
  "id": "084fc79c-6b95-4df1-8981-6234311ff3a9",
  "name": "client di test",
  "env": "collaudo",
  "key_id": "key pair id test 1234566",
  "key_pair_id": "{{keysid}}"
}
```
#expected result 
HTTP/1.1 201
[Asserts]
jsonpath "$.tenant_id" == "{{tenant_1}}"
jsonpath "$.name" == "client di test"
jsonpath "$.env" == "collaudo"
jsonpath "$.key_id" == "key pair id test 1234566"
jsonpath "$.key_pair_id" == "{{keysid}}"
# checks for the ISO 8601 date and time format including timezone offset
jsonpath "$.created_at" matches /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\+\d{2}:\d{2}$/
jsonpath "$.updated_at" matches /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\+\d{2}:\d{2}$/


GET {{base_url}}/tenants/{{tenant_1}}/keys
Authorization: {{token_admin_tenant_1}}

#expected result
HTTP/1.1 200
[Asserts] 
jsonpath "$.public_key" exists
jsonpath "$.id" != {{keysid}}
[Captures]
keysid_3: jsonpath "$.id"


################################
#                              #
#         TEARDOWN             #
#                              #
################################


DELETE {{base_url}}/tenants/{{tenant_1}}/clients/084fc79c-6b95-4df1-8981-6234311ff3a9
Authorization: {{token_admin_tenant_1}}
#expected result 
HTTP/1.1 204

GET {{base_url}}/tenants/{{tenant_1}}/keys
Authorization: {{token_admin_tenant_1}}

#expected result
HTTP/1.1 200
[Asserts] 
jsonpath "$.public_key" exists
jsonpath "$.id" exists
[Captures]
keysid: jsonpath "$.id"

DELETE {{base_url}}/tenants/{{tenant_1}}/keys/{{keysid}}
Authorization: {{token_admin_tenant_1}}

#expected result
HTTP/1.1 204

GET {{base_url}}/tenants/{{tenant_2}}/keys
Authorization: {{token_admin_tenant_2}}
#expected result
HTTP/1.1 200
[Asserts] 
jsonpath "$.public_key" exists
jsonpath "$.id" exists
[Captures]
keysid_2: jsonpath "$.id"

DELETE {{base_url}}/tenants/{{tenant_2}}/keys/{{keysid_2}}
Authorization: {{token_admin_tenant_2}}

#expected result
HTTP/1.1 204

DELETE {{base_url}}/tenants/{{tenant_1}}
Authorization: {{token_admin_tenant_1}}
#expected result
HTTP/1.1 204

DELETE {{base_url}}/tenants/{{tenant_2}}
Authorization: {{token_admin_tenant_2}}

#expected result
HTTP/1.1 204


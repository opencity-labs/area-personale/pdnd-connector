package server

import (
	"context"
	"errors"
	"unicode"

	"github.com/gofrs/uuid"
	"github.com/swaggest/usecase"
	"github.com/swaggest/usecase/status"
	"gitlab.com/opencontent/stanza-del-cittadino/pdnd-connector/server/models"
)

type TenantPayloadInput struct {
	Name     string `json:"name" description:"nome del tenant" example:"comune di Bugliano"`
	TenantId string `json:"id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	IPaCode  string `json:"ipa_code" description:"..." example:"123456"`
	Token    string `header:"Authorization" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
}

func CreateTenants(sctx *ServerContext) usecase.Interactor {
	tenantsCache := sctx.TenantsCache()
	servicesSync := sctx.ServicesSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input TenantPayloadInput, output *models.Tenant) error {
		servicesSync.Lock()
		defer servicesSync.Unlock()

		err := ValidateAdminToken(sctx, input.Token, input.TenantId)
		if err != nil && err.Error() == "empty token" {
			MetricsAdminTokenUnauthenticatedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			return status.Wrap(err, status.Unauthenticated)
		}
		if err != nil {
			MetricsAdminTokenPermissionDeniedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			sctx.LogHttpWarning().Str("http request POST /tenants/"+input.TenantId, "").Str("result", "403")
			return status.Wrap(err, status.PermissionDenied)
		}

		err = validateTenantPayload(input)
		if err != nil {
			return status.Wrap(err, status.InvalidArgument)

		}

		_, err = tenantsCache.Get(ctx, input.TenantId)
		if err == nil {
			return status.AlreadyExists.Status()
		}
		output.TenantID = input.TenantId
		output.Name = input.Name
		output.IPaCode = input.IPaCode
		romeTime := GetCurrentDateTime()
		output.CreatedAt = romeTime
		output.UpdatedAt = romeTime

		err = StoreTenants(sctx, output)
		if err != nil && err.Error() == "invalid data" {
			return status.InvalidArgument
		}
		if err != nil {
			return err
		}

		return nil
	})

	uc.SetTitle("Save Tenant Configuration")
	uc.SetDescription("...")
	uc.SetTags("Tenants")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}

type TenantPayloadUpdateInput struct {
	Name     string `json:"name" description:"nome del tenant" example:"comune di Bugliano"`
	TenantId string `path:"tenant_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	IPaCode  string `json:"ipa_code" description:"..." example:"123456"`
	Token    string `header:"Authorization" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
}

func UpdateTenant(sctx *ServerContext) usecase.Interactor {
	tenantsCache := sctx.TenantsCache()
	servicesSync := sctx.ServicesSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input TenantPayloadUpdateInput, output *models.Tenant) error {
		servicesSync.Lock()
		defer servicesSync.Unlock()

		err := ValidateAdminToken(sctx, input.Token, input.TenantId)
		if err != nil && err.Error() == "empty token" {
			MetricsAdminTokenUnauthenticatedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			sctx.LogHttpWarning().Str("http request PUT /tenants/"+input.TenantId, "").Str("result", "401")
			return status.Wrap(err, status.Unauthenticated)
		}
		if err != nil {
			MetricsAdminTokenPermissionDeniedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			sctx.LogHttpWarning().Str("http request PUT /tenants/"+input.TenantId, "").Str("result", "403")
			return status.Wrap(err, status.PermissionDenied)
		}
		err = validateTenantUpdatePayload(input)
		if err != nil {
			return status.Wrap(err, status.InvalidArgument)

		}
		romeTime := GetCurrentDateTime()
		tenant, err := tenantsCache.Get(ctx, input.TenantId)
		if err != nil && err.Error() == "value not found in store" {
			output.CreatedAt = romeTime
			output.TenantID = input.TenantId
		} else {
			output.CreatedAt = tenant.CreatedAt
			output.TenantID = tenant.TenantID
		}

		output.Name = input.Name
		output.IPaCode = input.IPaCode

		output.UpdatedAt = romeTime

		err = StoreTenants(sctx, output)
		if err != nil && err.Error() == "invalid data" {
			return status.InvalidArgument
		}
		if err != nil {
			return err
		}
		return nil
	})

	uc.SetTitle("Update tenant Configuration")
	uc.SetDescription("...")
	uc.SetTags("Tenants")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}

func PatchTenant(sctx *ServerContext) usecase.Interactor {
	tenantsCache := sctx.TenantsCache()
	servicesSync := sctx.ServicesSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input TenantPayloadUpdateInput, output *models.Tenant) error {
		servicesSync.Lock()
		defer servicesSync.Unlock()
		isModified := false

		err := ValidateAdminToken(sctx, input.Token, input.TenantId)
		if err != nil && err.Error() == "empty token" {
			MetricsAdminTokenUnauthenticatedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			return status.Wrap(err, status.Unauthenticated)
		}
		if err != nil {
			MetricsAdminTokenPermissionDeniedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			sctx.LogHttpWarning().Str("http request PATCH /tenants/"+input.TenantId, "").Str("result", "403")
			return status.Wrap(err, status.PermissionDenied)
		}

		tenant, err := tenantsCache.Get(ctx, input.TenantId)
		if err != nil && err.Error() == "value not found in store" {
			return status.Wrap(err, status.NotFound)

		}
		_, err = uuid.FromString(input.TenantId)
		if err != nil {
			return status.Wrap(errors.New("tenant id empty or is an invalid uuid: "+err.Error()), status.InvalidArgument)
		}
		if input.Name != "" {
			if isValidAlphanumericPhrase(input.Name) {
				output.Name = input.Name
				isModified = true
			} else {
				return status.Wrap(errors.New("tenant name contains digits"), status.InvalidArgument)
			}
		} else {
			output.Name = tenant.Name
		}

		if input.IPaCode != "" {
			if len(input.IPaCode) == 6 {
				output.IPaCode = input.IPaCode
				isModified = true
			} else {
				return status.Wrap(errors.New("ipa code not valid"), status.InvalidArgument)
			}
		} else {
			output.IPaCode = tenant.IPaCode
		}
		if isModified {
			romeTime := GetCurrentDateTime()
			output.UpdatedAt = romeTime
			output.CreatedAt = tenant.CreatedAt
			output.TenantID = input.TenantId

			err = StoreTenants(sctx, output)
			if err != nil && err.Error() == "invalid data" {
				return status.InvalidArgument
			}
			if err != nil {
				return err
			}
		}
		return nil
	})

	uc.SetTitle("Update tenant Configuration")
	uc.SetDescription("...")
	uc.SetTags("Tenants")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}

type TenantGetInput struct {
	TeantIdPath string `path:"tenant_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	Token       string `header:"Authorization" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
}

func GetTenant(sctx *ServerContext) usecase.Interactor {
	tenantsCache := sctx.TenantsCache()
	servicesSync := sctx.ServicesSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input TenantGetInput, output *models.Tenant) error {
		servicesSync.RLock()
		defer servicesSync.RUnlock()

		err := ValidateAdminToken(sctx, input.Token, input.TeantIdPath)
		if err != nil && err.Error() == "empty token" {
			MetricsAdminTokenUnauthenticatedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			sctx.LogHttpWarning().Str("http request GET /tenants/"+input.TeantIdPath, "").Str("result", "401")
			return status.Wrap(err, status.Unauthenticated)
		}
		if err != nil {
			MetricsAdminTokenPermissionDeniedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			sctx.LogHttpWarning().Str("http request GET /tenants/"+input.TeantIdPath, "").Str("result", "403")
			return status.Wrap(err, status.PermissionDenied)
		}

		_, err = uuid.FromString(input.TeantIdPath)
		if err != nil {
			return status.Wrap(err, status.InvalidArgument)

		}

		tenant, err := tenantsCache.Get(ctx, input.TeantIdPath)
		if err != nil && err.Error() == "value not found in store" {
			return status.NotFound
		}
		if err != nil {
			sctx.LogHttpError().Stack().Err(err).Msg("get tenant by id error for: " + input.TeantIdPath)
			return status.Internal
		}

		*output = *tenant

		return nil
	})

	uc.SetTitle("Get Tenant Configuration")
	uc.SetDescription("...")
	uc.SetTags("Tenants")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}

type GetTenantsListInput struct {
	Offset int    `query:"offset" description:" the starting point or the index from which the data should be retrieved" example:"6"`
	Token  string `header:"Authorization" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	Limit  int    `query:"limit" description:"The limit parameter specifies the maximum number of items to be returned in a single page or request"`
	Sort   string `query:"sort" description:"..."`
}

func GetTenants(sctx *ServerContext) usecase.Interactor {
	//servicesCache := sctx.ServicesCache()
	servicesSync := sctx.ServicesSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input GetTenantsListInput, output *models.Tenant) error {
		servicesSync.RLock()
		defer servicesSync.RUnlock()

		err := ValidateAdminTokenWithoutTenantId(sctx, input.Token)
		if err != nil && err.Error() == "empty token" {
			MetricsAdminTokenUnauthenticatedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			sctx.LogHttpWarning().Str("http request GET /tenants/", "").Str("result", "401")
			return status.Wrap(err, status.Unauthenticated)
		}
		if err != nil {
			MetricsAdminTokenPermissionDeniedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			sctx.LogHttpWarning().Str("http request GET /tenants/", "").Str("result", "403")
			return status.Wrap(err, status.PermissionDenied)
		}

		_, err = GetTenantsPage(sctx, input.Offset, input.Limit)
		if err != nil && err.Error() == "not found" {
			return status.NotFound
		}
		if err != nil {
			sctx.LogHttpError().Stack().Err(err).Msg("get tenants id errpor: ")
			return status.Internal
		}

		//*output = *tenant

		return nil
	})

	uc.SetTitle("Get Tenants Configuration list")
	uc.SetDescription("...")
	uc.SetTags("Tenants")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}

func DeleteTenant(sctx *ServerContext) usecase.Interactor {
	//servicesCache := sctx.ServicesCache()
	servicesSync := sctx.ServicesSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input TenantGetInput, output *models.Tenant) error {
		servicesSync.RLock()
		defer servicesSync.RUnlock()

		err := ValidateAdminToken(sctx, input.Token, input.TeantIdPath)
		if err != nil && err.Error() == "empty token" {
			MetricsAdminTokenUnauthenticatedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			sctx.LogHttpWarning().Str("http request DELETE /tenants/", "").Str("result", "401")
			return status.Wrap(err, status.Unauthenticated)
		}
		if err != nil {
			MetricsAdminTokenPermissionDeniedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			sctx.LogHttpWarning().Str("http request DELETE /tenants/", "").Str("result", "403")
			return status.Wrap(err, status.PermissionDenied)
		}

		_, err = uuid.FromString(input.TeantIdPath)
		if err != nil {
			return status.Wrap(err, status.InvalidArgument)

		}
		err = TrashTenant(sctx, input.TeantIdPath)
		if err != nil && (err.Error() == "no such file or directory" || err.Error() == "not found") {
			return status.NotFound
		}
		if err != nil {
			return status.Internal
		}

		return nil
	})

	uc.SetTitle("delete Tenants Configuration")
	uc.SetDescription("...")
	uc.SetTags("Tenants")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}
func OptionsTenantById(sctx *ServerContext) usecase.Interactor {

	uc := usecase.NewInteractor(func(ctx context.Context, input TenantGetInput, _ *struct{}) error {
		err := ValidateAdminToken(sctx, input.Token, input.TeantIdPath)
		if err != nil && err.Error() == "empty token" {
			MetricsAdminTokenUnauthenticatedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			sctx.LogHttpWarning().Str("http request OPTION /tenants/", "").Str("result", "401")
			return status.Wrap(err, status.Unauthenticated)
		}
		if err != nil {
			MetricsAdminTokenPermissionDeniedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			sctx.LogHttpWarning().Str("http request OPTION /tenants/", "").Str("result", "403")
			return status.Wrap(err, status.PermissionDenied)
		}
		return nil
	})

	uc.SetTitle("Options Tenant by id")
	uc.SetDescription("Get available operations for the tenant resource.")
	uc.SetTags("Tenants")

	return uc
}

type TenantOptionInput struct {
	Token string `header:"Authorization" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
}

func OptionsTenant(sctx *ServerContext) usecase.Interactor {
	uc := usecase.NewInteractor(func(ctx context.Context, input *TenantOptionInput, _ *struct{}) error {
		err := ValidateAdminTokenWithoutTenantId(sctx, input.Token)
		if err != nil && err.Error() == "empty token" {
			MetricsAdminTokenUnauthenticatedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			sctx.LogHttpWarning().Str("http request GET /tenants/", "").Str("result", "401")
			return status.Wrap(err, status.Unauthenticated)
		}
		if err != nil {
			MetricsAdminTokenPermissionDeniedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			sctx.LogHttpWarning().Str("http request GET /tenants/", "").Str("result", "403")
			return status.Wrap(err, status.PermissionDenied)
		}
		return nil
	})

	uc.SetTitle("Options Tenant")
	uc.SetDescription("Get available operations for the tenant resource.")
	uc.SetTags("Tenants")

	return uc
}

func isValidAlphanumericPhrase(s string) bool {
	if s == "" {
		return false
	}

	for _, char := range s {
		if !unicode.IsLetter(char) && !unicode.IsSpace(char) {
			return false
		}
	}

	return true
}

func validateTenantPayload(input TenantPayloadInput) error {
	_, err := uuid.FromString(input.TenantId)
	if err != nil {
		return errors.New("tenant id empty or is an invalid uuid: " + err.Error())
	}
	if !isValidAlphanumericPhrase(input.Name) {
		return errors.New("tenant name empty or contains digits")
	}
	if len(input.IPaCode) != 6 {
		return errors.New("ipa code empty or invalid")
	}
	return nil
}
func validateTenantUpdatePayload(input TenantPayloadUpdateInput) error {
	_, err := uuid.FromString(input.TenantId)
	if err != nil {
		return errors.New("tenant id empty or is an invalid uuid: " + err.Error())
	}
	if !isValidAlphanumericPhrase(input.Name) {
		return errors.New("tenant name empty or contains digits")
	}
	if len(input.IPaCode) != 6 {
		return errors.New("ipa code empty or invalid")
	}
	return nil
}

package server

import (
	"io"
	"strings"

	"github.com/graymeta/stow"
	"gitlab.com/opencontent/stanza-del-cittadino/pdnd-connector/server/models"
)

func GetPrivateKey(sctx *ServerContext, tenantId, keyId string) ([]byte, error) {
	serverConfig := sctx.ServerConfig()
	fileStorage := sctx.FileStorage()

	basePath := serverConfig.StorageBasePath + "/pdnd_keys/" + "tenant_" + tenantId + "/" + keyId + "/"

	itemPath := basePath + "privateClientKey" + ".pem"

	item, err := fileStorage.Item(itemPath)
	if err != nil {
		return nil, err
	}

	itemContent, err := item.Open()
	if err != nil {
		return nil, err
	}
	defer itemContent.Close()

	itemBytes, err := io.ReadAll(itemContent)
	if err != nil {
		return nil, err
	}
	return itemBytes, nil
}
func GetPublicKey(sctx *ServerContext, tenantId, keyId string) ([]byte, error) {
	serverConfig := sctx.ServerConfig()
	fileStorage := sctx.FileStorage()

	basePath := serverConfig.StorageBasePath + "/pdnd_keys/" + "tenant_" + tenantId + "/" + keyId + "/"

	itemPath := basePath + "publicClientKey.pem"

	item, err := fileStorage.Item(itemPath)
	if err != nil {
		return nil, err
	}

	itemContent, err := item.Open()
	if err != nil {
		return nil, err
	}
	defer func() {
		itemContent.Close()
	}()

	itemBytes, err := io.ReadAll(itemContent)
	if err != nil {
		return nil, err
	}

	return itemBytes, nil
}

func GetFreePublicKey(sctx *ServerContext, tenantId string) (string, string, error) {
	serverConfig := sctx.ServerConfig()

	var freeKeyPairId string
	var freePublicKey string
	fileServer := sctx.FileServer()
	basePath := serverConfig.StorageBasePath + "/pdnd_keys/tenant_" + tenantId + "/"
	pageSize := 10

	container, err := fileServer.Container(serverConfig.StorageBucket)
	if err != nil {
		return "", "", err
	}

	cursor := stow.CursorStart
	for {
		items, nextCursor, err := container.Items(basePath, cursor, pageSize)
		if err != nil {
			return "", "", err
		}

		for _, item := range items {
			keyPairId := extractLastUUID(item.Name())

			result, err := IsKeyPairIdUsedByAClientPdnd(sctx, keyPairId)
			if err != nil {
				return "", "", err
			}

			if !result {
				freeKeyPairId = keyPairId

				publicKey, err := GetPublicKey(sctx, tenantId, keyPairId)
				if err != nil {
					return "", "", err
				}
				freePublicKey = string(publicKey)
				break
			}
		}

		cursor = nextCursor
		if stow.IsCursorEnd(cursor) {
			break
		}
	}

	if freeKeyPairId == "" {
		return "", "", nil
	}

	return freeKeyPairId, freePublicKey, nil
}

func Storekeys(sctx *ServerContext, tenantId string, input *models.Keys) error {
	serverConfig := sctx.ServerConfig()

	basePath := serverConfig.StorageBasePath + "/pdnd_keys/" + "tenant_" + tenantId + "/" + input.KeysId + "/"

	err := StoreKeyByPath(sctx, tenantId, input, basePath)
	if err != nil {
		return err
	}
	return nil
}

func DeleteKeys(sctx *ServerContext, tenantId, keysId string) error {
	fileServer := sctx.FileServer()
	_, err := GetPublicKey(sctx, tenantId, keysId)
	if err != nil {
		return err
	}

	serverConfig := sctx.ServerConfig()
	basePath := serverConfig.StorageLocalPath + "/" + serverConfig.StorageBucket + "/" + serverConfig.StorageBasePath + "/pdnd_keys/" + "tenant_" + tenantId + "/" + keysId + "/"

	containerToDelete, err := fileServer.Container(basePath)
	if err != nil {
		return err
	}
	err = fileServer.RemoveContainer(containerToDelete.ID())
	if err != nil {
		return err
	}
	return nil
}

func extractLastUUID(input string) string {
	parts := strings.Split(input, "/")
	if len(parts) < 2 {
		return ""
	}
	uuid := parts[len(parts)-2]
	return uuid
}

func TrashKeysTenant(sctx *ServerContext, tenantId string) error {
	fileServer := sctx.FileServer()
	serverConfig := sctx.ServerConfig()
	basePath := serverConfig.StorageBucket + "/" + serverConfig.StorageBasePath + "/pdnd_keys/" + "tenant_" + tenantId + "/"

	err := CopyContainerItems(sctx, tenantId, basePath)
	if err != nil {
		return err
	}

	containerToDelete, err := fileServer.Container(basePath)
	if err != nil {
		return err
	}
	err = fileServer.RemoveContainer(containerToDelete.ID())
	if err != nil {
		return err
	}
	return nil
}

func CopyContainerItems(sctx *ServerContext, tenantId, originalLocation string) error {
	fileServer := sctx.FileServer()
	serverConfig := sctx.ServerConfig()
	pageSize := 10

	err := stow.WalkContainers(fileServer, originalLocation, pageSize, func(container stow.Container, err error) error {
		if err != nil {
			return err
		}
		keyPairId := extractLastUUID(container.Name())

		publicKey, err := GetPublicKey(sctx, tenantId, keyPairId)
		if err != nil {
			return err
		}
		privateKey, err := GetPrivateKey(sctx, tenantId, keyPairId)
		if err != nil {
			return err
		}
		newLocation := serverConfig.StorageBasePath + "/trash/" + "/pdnd_keys/" + "tenant_" + tenantId + "/" + keyPairId + "/"
		input := models.Keys{PublicKey: string(publicKey), PrivateKey: string(privateKey), KeysId: keyPairId}
		err = StoreKeyByPath(sctx, tenantId, &input, newLocation)
		if err != nil {
			return err
		}

		return nil

	})
	if err != nil {
		return err
	}

	return nil
}

func StoreKeyByPath(sctx *ServerContext, tenantId string, input *models.Keys, basePath string) error {
	fileStorage := sctx.FileStorage()

	itemBytes := []byte(input.PublicKey)
	itemName := basePath + "publicClientKey" + ".pem"
	itemContent := string(itemBytes)
	itemSize := int64(len(itemContent))

	itemReader := strings.NewReader(itemContent)

	_, err := fileStorage.Put(itemName, itemReader, itemSize, nil)
	if err != nil {
		return err
	}

	itemBytes = []byte(input.PrivateKey)
	itemName = basePath + "privateClientKey" + ".pem"
	itemContent = string(itemBytes)
	itemSize = int64(len(itemContent))

	itemReader = strings.NewReader(itemContent)

	_, err = fileStorage.Put(itemName, itemReader, itemSize, nil)
	if err != nil {
		return err
	}
	return nil
}

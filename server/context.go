package server

import (
	"context"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"github.com/eko/gocache/lib/v4/cache"
	"github.com/getsentry/sentry-go"
	"github.com/graymeta/stow"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/pkgerrors"
	"gitlab.com/opencontent/stanza-del-cittadino/pdnd-connector/server/models"
)

type ServerContext struct {
	ctx                   context.Context
	exitFn                context.CancelFunc
	wg                    *sync.WaitGroup
	serverConfig          ServerConfig
	outlog                zerolog.Logger
	errlog                zerolog.Logger
	fileServer            stow.Location
	clientsCache          *cache.Cache[*models.ClientPDND]
	tenantsCache          *cache.Cache[*models.Tenant]
	configsCache          *cache.Cache[*models.Config]
	sdcJWKSPublicKeyCache *cache.Cache[*models.SdcJWKSPublicKey]
	fileStorage           stow.Container
	servicesSync          *sync.RWMutex
	eServiceList          models.EServiceList
}

type processStarterFn func(*ServerContext)

func InitServerContext() *ServerContext {
	ctx, exitFn := context.WithCancel(context.Background())

	wg := &sync.WaitGroup{}

	zerolog.ErrorStackMarshaler = pkgerrors.MarshalStack
	errlog := zerolog.New(os.Stderr).Level(zerolog.ErrorLevel).With().Str("channel", "err").Logger()

	serverConfig, err := LoadConfig(ctx)
	if err != nil {
		errlog.Fatal().Str("source", "core").Stack().Err(err).Msg("wrong environment configuration")
	}

	var outlogLevel zerolog.Level
	if serverConfig.Debug {
		outlogLevel = zerolog.DebugLevel
	} else {
		outlogLevel = zerolog.InfoLevel
	}
	outlog := zerolog.New(os.Stdout).Level(outlogLevel).With().Str("channel", "out").Logger()

	sctx := &ServerContext{
		ctx:          ctx,
		exitFn:       exitFn,
		wg:           wg,
		serverConfig: serverConfig,
		outlog:       outlog,
		errlog:       errlog,
		fileServer:   nil,
		fileStorage:  nil,
		servicesSync: &sync.RWMutex{},
	}

	if serverConfig.SentryEnabled {
		err = sentry.Init(sentry.ClientOptions{
			Dsn:           serverConfig.SentryToken,
			EnableTracing: true,
			// Set TracesSampleRate to 1.0 to capture 100%
			// of transactions for performance monitoring.
			// We recommend adjusting this value in production,
			TracesSampleRate: 1.0,
			Release:          "v" + VERSION,
		})
		if err != nil {
			sctx.LogCoreFatal().Stack().Err(err).Msg("sentry initialization failed")
		}
	}

	fileServer, fileStorage := connectFileStorage(sctx)
	sctx.fileServer = fileServer
	sctx.fileStorage = fileStorage

	clientsCache := startClientsCache(sctx)
	sctx.clientsCache = clientsCache

	tenantsCache := startTenantsCache(sctx)
	sctx.tenantsCache = tenantsCache

	configsCache := startConfigsCache(sctx)
	sctx.configsCache = configsCache

	startSdcJWKSPublicKeyCache := startSdcJWKSPublicKeyCache(sctx)
	sctx.sdcJWKSPublicKeyCache = startSdcJWKSPublicKeyCache

	configsTenantsPreloader(sctx)
	configsClientsPreloader(sctx)
	configsCofingsPreloader(sctx)

	err = sctx.loadEserviceList()
	if err != nil {
		sctx.LogCoreFatal().Stack().Err(err).Msg("eServiceList initialization failed")
	}
	return sctx
}

func (sctx *ServerContext) StartProcess(name string, processStarterFn processStarterFn) {
	sctx.wg.Add(1)
	go func() {
		defer sctx.wg.Done()
		sctx.LogCoreDebug().Msgf("%s process started", name)
		processStarterFn(sctx)
		sctx.LogCoreDebug().Msgf("%s process done", name)
	}()
}

func (sctx *ServerContext) Daemonize() {
	termChan := make(chan os.Signal, 1)

	signal.Notify(termChan, syscall.SIGINT, syscall.SIGTERM)

	<-termChan

	sctx.LogCoreDebug().Msg("shutdown signal received")

	sctx.exitFn()
	sctx.wg.Wait()

	if sctx.serverConfig.SentryEnabled {
		sentry.Flush(2 * time.Second)
	}

	sctx.fileServer.Close()

	sctx.LogCoreDebug().Msg("all processes done, shutting down")
}

func (sctx *ServerContext) Ctx() context.Context {
	return sctx.ctx
}

func (sctx *ServerContext) ServerConfig() ServerConfig {
	return sctx.serverConfig
}

func (sctx *ServerContext) FileStorage() stow.Container {
	return sctx.fileStorage
}
func (sctx *ServerContext) FileServer() stow.Location {
	return sctx.fileServer
}

func (sctx *ServerContext) ClientsCache() cache.Cache[*models.ClientPDND] {
	return *sctx.clientsCache
}
func (sctx *ServerContext) TenantsCache() cache.Cache[*models.Tenant] {
	return *sctx.tenantsCache
}
func (sctx *ServerContext) ConfigsCache() cache.Cache[*models.Config] {
	return *sctx.configsCache
}
func (sctx *ServerContext) SdcJWKSPublicKeyCache() cache.Cache[*models.SdcJWKSPublicKey] {
	return *sctx.sdcJWKSPublicKeyCache
}

func (sctx *ServerContext) ServicesSync() *sync.RWMutex {
	return sctx.servicesSync
}

func (sctx *ServerContext) Done() <-chan struct{} {
	return sctx.ctx.Done()
}

func (sctx *ServerContext) WithTimeout(duration time.Duration) (context.Context, context.CancelFunc) {
	return context.WithTimeout(sctx.ctx, duration)
}

func (sctx *ServerContext) LogHttpDebug() *zerolog.Event {
	return sctx.outlog.Debug().Str("source", "http")
}

func (sctx *ServerContext) LogHttpInfo() *zerolog.Event {
	return sctx.outlog.Info().Str("source", "http")
}
func (sctx *ServerContext) LogHttpWarning() *zerolog.Event {
	return sctx.outlog.Warn().Str("source", "http")
}

func (sctx *ServerContext) LogHttpError() *zerolog.Event {
	return sctx.errlog.Error().Str("source", "http")
}

func (sctx *ServerContext) LogHttpFatal() *zerolog.Event {
	return sctx.errlog.Fatal().Str("source", "http")
}

func (sctx *ServerContext) LogCoreDebug() *zerolog.Event {
	return sctx.outlog.Debug().Str("source", "core")
}

func (sctx *ServerContext) LogCoreError() *zerolog.Event {
	return sctx.errlog.Error().Str("source", "core")
}

func (sctx *ServerContext) LogCoreFatal() *zerolog.Event {
	return sctx.errlog.Fatal().Str("source", "core")
}

func (sctx *ServerContext) EServiceList() *models.EServiceList {
	return &sctx.eServiceList
}

func (sctx *ServerContext) loadEserviceList() error {

	//eServiceList, err := GetEserviceListByEnv(sctx)
	eServiceList, err := GetEserviceList(sctx)
	/* 	err := json.Unmarshal([]byte(models.ESERVICELIST), &sctx.eServiceList)*/
	if err != nil {
		sctx.LogCoreError().Stack().Err(err).Msg("error loading e-services configurations")
		return err
	}
	sctx.eServiceList = eServiceList
	return nil
}

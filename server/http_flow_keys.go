package server

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"

	"github.com/google/uuid"
	"gitlab.com/opencontent/stanza-del-cittadino/pdnd-connector/server/models"
)

type HttpFlowKeys struct {
	Name       string
	Sctx       *ServerContext
	Err        error
	Msg        string
	FlowStatus bool
	Response   models.KeysResponse
	Keys       models.Keys
	Request    getKeysInput
}

func (r *HttpFlowKeys) Exec() bool {
	r.FlowStatus = true
	return true &&
		r.retrieveKey() &&
		r.buildKeyResponse()
}

func (r *HttpFlowKeys) retrieveKey() bool {

	freeKeyPairId, freePublicKeyId, err := GetFreePublicKey(r.Sctx, r.Request.TenantId)
	if err != nil {
		r.Sctx.LogHttpError().Stack().Err(err).Msg("failed to check available keys")
		r.Err = err
		r.Msg = "failed to check available keys"
		return false
	}

	if freeKeyPairId != "" && freePublicKeyId != "" {
		r.Keys.KeysId = freeKeyPairId
		r.Keys.PublicKey = freePublicKeyId
		return true
	} else {
		return r.createKeys() &&
			r.storeKeys()
	}
}

func (r *HttpFlowKeys) createKeys() bool {
	privateKey, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		r.Sctx.LogHttpError().Stack().Err(err).Msg("failed to create keys")
		r.Err = err
		r.Msg = "failed to create keys"
		return false
	}

	privateKeyPEM, err := encodePrivateKeyToPEM(privateKey)
	if err != nil {
		r.Sctx.LogHttpError().Stack().Err(err).Msg("failed to convert private key to PKCS8")
		r.Err = err
		r.Msg = "failed to convert private key to PKCS8"
		return false
	}

	publicKey := privateKey.PublicKey
	pkiXPublicKey, err := x509.MarshalPKIXPublicKey(&publicKey)
	if err != nil {
		r.Sctx.LogHttpError().Stack().Err(err).Msg("failed to convert private key to PKIX")
		r.Err = err
		r.Msg = "failed to convert private key to PKIX"
		return false
	}

	publicKeyPEM := pem.EncodeToMemory(&pem.Block{
		Type:  "PUBLIC KEY",
		Bytes: pkiXPublicKey,
	})

	keysId := uuid.New().String()

	r.Keys = models.Keys{
		PublicKey:  string(publicKeyPEM),
		PrivateKey: string(privateKeyPEM),
		KeysId:     keysId,
	}
	return true
}

func (r *HttpFlowKeys) storeKeys() bool {
	err := Storekeys(r.Sctx, r.Request.TenantId, &r.Keys)
	if err != nil {
		r.Sctx.LogHttpError().Stack().Err(err).Msg("failed to store keys")
		r.Err = err
		r.Msg = "failed to store keys"
		return false
	}
	return true
}

func (r *HttpFlowKeys) buildKeyResponse() bool {
	r.Response.PublicKey = r.Keys.PublicKey
	r.Response.KeysId = r.Keys.KeysId
	return true
}

func encodePrivateKeyToPEM(privateKey *rsa.PrivateKey) ([]byte, error) {
	privateKeyBytes, err := x509.MarshalPKCS8PrivateKey(privateKey)
	if err != nil {
		return nil, err
	}

	privateKeyPEM := pem.EncodeToMemory(&pem.Block{
		Type:  "PRIVATE KEY",
		Bytes: privateKeyBytes,
	})

	return privateKeyPEM, nil
}

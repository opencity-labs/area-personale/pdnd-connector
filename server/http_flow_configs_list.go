package server

import (
	"fmt"

	"gitlab.com/opencontent/stanza-del-cittadino/pdnd-connector/server/models"
)

type HttpFlowConfigsList struct {
	Name       string
	Sctx       *ServerContext
	Request    GetConfigsByTenantIdInput
	Err        error
	Msg        string
	FlowStatus bool
	Response   models.ConfigListResponse
}

func (r *HttpFlowConfigsList) Exec() bool {
	r.FlowStatus = true
	return r.FlowStatus &&
		r.buildPaginatedConfigsList()
}

func (r *HttpFlowConfigsList) buildPaginatedConfigsList() bool {

	limit := r.Request.Limit
	offset := r.Request.Offset
	sort := r.Request.Sort
	ConfigsList, err := GetConfigsByTenantId(r.Sctx, r.Request.TenantId)
	if err != nil && err.Error() == "not found" {
		r.Sctx.LogHttpError().Stack().Err(err).Msg("no Configs for tenant: " + r.Request.TenantId)
		r.Err = err
		r.Msg = "not found"
		return false
	}
	if err != nil {
		r.Sctx.LogHttpError().Stack().Err(err).Msg("error retrieving config for " + r.Request.TenantId)
		r.Msg = "internal error"
		r.Err = err
		return false
	}
	if ConfigsList == nil {
		newList := make([]models.Config, 0)
		ConfigsList = &newList
	}
	totalConfigsListSize := len(*ConfigsList)

	if limit == 0 {
		limit = 10
	}
	if limit < 0 {
		limit = 0
	}
	if offset < 0 {
		offset = 0
	}
	if limit > totalConfigsListSize {
		limit = totalConfigsListSize
	}
	if offset > totalConfigsListSize {
		offset = totalConfigsListSize
	}

	var selfPage, nextPage, prevPage string
	serverConfig := r.Sctx.serverConfig
	endpoint := serverConfig.HttpExternalBasePath
	sortPath := ""
	if sort != "" {
		sortPath = "&sort="
	}

	selfPage = fmt.Sprintf("/tenants/"+r.Request.TenantId+"/configs"+"?offset=%d&limit=%d"+sortPath+"%s", offset, limit, sort)
	if totalConfigsListSize >= limit && offset < totalConfigsListSize {
		nextOffset := offset + limit
		if nextOffset >= totalConfigsListSize {
			nextPage = ""
		} else {
			nextPage = fmt.Sprintf("/tenants/"+r.Request.TenantId+"/configs"+"?offset=%d&limit=%d"+sortPath+"%s", nextOffset, limit, sort)
		}
	}
	if offset > 0 {
		prevOffset := offset - limit
		if prevOffset < 0 {
			prevOffset = 0
		}
		prevPage = fmt.Sprintf("/tenants/"+r.Request.TenantId+"/configs"+"?offset=%d&limit=%d"+sortPath+"%s", prevOffset, limit, sort)
	}

	if nextPage != "" {
		tmp := endpoint + nextPage
		r.Response.Links.Next = &tmp
	}
	if prevPage != "" {
		tmp := endpoint + prevPage
		r.Response.Links.Prev = &tmp
	}
	r.Response.Links.Self = endpoint + selfPage

	r.Response.Data = r.getPaginatedList(offset, limit, *ConfigsList)
	r.Response.Meta.Total = totalConfigsListSize
	r.Response.Meta.Page.Limit = limit
	r.Response.Meta.Page.Offset = offset
	if sort != "" {
		r.Response.Meta.Page.Sort = &sort
	}

	return true
}

func (r *HttpFlowConfigsList) getPaginatedList(offset, limit int, ConfigsArray []models.Config) []models.Config {
	startIndex := offset
	endIndex := offset + limit
	if startIndex < 0 {
		startIndex = 0
	}
	if endIndex < 0 {
		endIndex = 0
	}
	if endIndex > len(ConfigsArray) {
		endIndex = len(ConfigsArray)
	}
	if startIndex > endIndex {
		startIndex = 0
	}
	/* 	sort.SliceStable(ConfigsArray, func(i, j int) bool {
		return ConfigsArray[i].CreatedAt < ConfigsArray[j].CreatedAt
	}) */

	return ConfigsArray[startIndex:endIndex]
}

package server

import (
	"context"
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"errors"
	"fmt"
	"strings"
	"time"

	"gitlab.com/opencontent/stanza-del-cittadino/pdnd-connector/server/models/anpr"
)

func StringPtr(s string) *string {
	s1 := s
	return &s1
}

func CapitalizeFirstLetter(s string) string {
	if len(s) == 0 {
		return s
	}
	return strings.ToUpper(string(s[0])) + strings.ToLower(s[1:])
}

func FormatDate(input string) (string, error) {
	parsedDate, err := time.Parse("2006-01-02", input)
	if err != nil {
		return "", err
	}

	return parsedDate.Format("1990-01-01T00:00:00+01:00"), nil
}

func obscureTaxCode(codiceFiscale string) string {
	if len(codiceFiscale) <= 4 {
		return strings.Repeat("*", len(codiceFiscale))
	}
	numAsterisks := len(codiceFiscale) - 4
	return strings.Repeat("*", numAsterisks) + codiceFiscale[numAsterisks:]
}

func ConvertDate(inputDate string) (string, error) {
	// Define the input and output date formats
	inputFormat := "2006-01-02"
	outputFormat := "02/01/2006"

	date, err := time.Parse(inputFormat, inputDate)
	if err != nil {
		return "", err
	}
	return date.Format(outputFormat), nil
}

func GetCurrentDateTime() string {
	currentTimeUTC := time.Now().UTC()
	romeLocation := time.FixedZone("Rome", 2*60*60)
	currentTimeRome := currentTimeUTC.In(romeLocation)
	iso8601Format := "2006-01-02T15:04:05+01:00"
	return currentTimeRome.Format(iso8601Format)
}

func GetUTCtimestamp() string {
	currentTimeUTC := time.Now().UTC()
	iso8601Format := "2006-01-02T15:04:05+01:00"
	return currentTimeUTC.Format(iso8601Format)
}

func GetClientIp(ctx context.Context) string {
	// Extract client IP from context
	clientIP, ok := ctx.Value("realIP").(string)
	if !ok {
		clientIP = "unknown"
	}
	return clientIP
}

// parsePublicKey parses a PEM encoded private key.
func parsePublicKey(pemBytes []byte) (Unsigner, error) {
	block, _ := pem.Decode(pemBytes)
	if block == nil {
		return nil, errors.New("ssh: no key found")
	}

	var rawkey interface{}
	switch block.Type {
	case "PUBLIC KEY":
		rsa, err := x509.ParsePKIXPublicKey(block.Bytes)
		if err != nil {
			return nil, err
		}
		rawkey = rsa
	default:
		return nil, fmt.Errorf("ssh: unsupported key type %q", block.Type)
	}

	return newUnsignerFromKey(rawkey)
}

// parsePublicKey parses a PEM encoded private key.
// PRIVATE KEY dovrebbe dire rsa private key per idenrtificare che è una chiave rsa
func parsePrivateKey(pemBytes []byte) (Signer, error) {
	block, _ := pem.Decode(pemBytes)
	if block == nil {
		return nil, errors.New("ssh: no key found")
	}

	var rawkey interface{}
	switch block.Type {
	case "PRIVATE KEY":
		rsa, err := x509.ParsePKCS8PrivateKey(block.Bytes)
		if err != nil {
			return nil, err
		}
		rawkey = rsa
	default:
		return nil, fmt.Errorf("ssh: unsupported key type %q", block.Type)
	}
	return newSignerFromKey(rawkey)
}

// A Signer is can create signatures that verify against a public key.
type Signer interface {
	// Sign returns raw signature for the given data. This method
	// will apply the hash specified for the keytype to the data.
	Sign(data []byte) ([]byte, error)
}

// A Signer is can create signatures that verify against a public key.
type Unsigner interface {
	// Sign returns raw signature for the given data. This method
	// will apply the hash specified for the keytype to the data.
	Unsign(data []byte, sig []byte) error
}

func newSignerFromKey(k interface{}) (Signer, error) {
	var sshKey Signer
	switch t := k.(type) {
	case *rsa.PrivateKey:
		sshKey = &RsaPrivateKey{t}
	default:
		return nil, fmt.Errorf("ssh: unsupported key type %T", k)
	}
	return sshKey, nil
}

func newUnsignerFromKey(k interface{}) (Unsigner, error) {
	var sshKey Unsigner
	switch t := k.(type) {
	case *rsa.PublicKey:
		sshKey = &RsaPublicKey{t}
	default:
		return nil, fmt.Errorf("ssh: unsupported key type %T", k)
	}
	return sshKey, nil
}

type RsaPublicKey struct {
	*rsa.PublicKey
}

type RsaPrivateKey struct {
	*rsa.PrivateKey
}

// Sign signs data with rsa-sha256
func (r *RsaPrivateKey) Sign(data []byte) ([]byte, error) {
	h := sha256.New()
	h.Write(data)
	d := h.Sum(nil)
	return rsa.SignPKCS1v15(rand.Reader, r.PrivateKey, crypto.SHA256, d)
}

// Unsign verifies the message using a rsa-sha256 signature
func (r *RsaPublicKey) Unsign(message []byte, sig []byte) error {
	h := sha256.New()
	h.Write(message)
	d := h.Sum(nil)
	return rsa.VerifyPKCS1v15(r.PublicKey, crypto.SHA256, d, sig)
}

func SignDigest(privateKey, toSign string) (string, error) {
	var sig string
	signer, err := parsePrivateKey([]byte(privateKey))
	if err != nil {
		return sig, err
	}

	signed, err := signer.Sign([]byte(toSign))
	if err != nil {
		return sig, err
	}
	sig = base64.StdEncoding.EncodeToString(signed)
	return sig, nil
}

func UnsignDigest(publicKey, toSign, sig string) error {
	parser, err := parsePublicKey([]byte(publicKey))
	if err != nil {
		return err
	}
	signed, err := base64.StdEncoding.DecodeString(sig)
	if err != nil {
		return err
	}
	err = parser.Unsign([]byte(toSign), signed)
	if err != nil {
		return err
	}
	return nil
}

func BuildMetaResponse(callUrl, source, data, format, privateKey string) (*anpr.Meta, error) {
	meta := anpr.Meta{}
	romeTime := GetCurrentDateTime()
	meta.CreatedAt = &romeTime
	meta.Format = &format
	signature, err := SignDigest(privateKey, data)
	if err != nil {
		return &meta, err
	}
	meta.Signature = &signature
	meta.Source = &source
	meta.CallUrl = &callUrl
	return &meta, nil
}

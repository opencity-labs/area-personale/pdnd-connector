package server

import (
	"context"

	"github.com/gofrs/uuid"
	"github.com/swaggest/usecase"
	"github.com/swaggest/usecase/status"
	"gitlab.com/opencontent/stanza-del-cittadino/pdnd-connector/server/models"
)

type getKeysInput struct {
	TenantId string `path:"tenant_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	Token    string `header:"Authorization" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
}

func GetKeys(sctx *ServerContext) usecase.Interactor {
	servicesSync := sctx.ServicesSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input *getKeysInput, output *models.KeysResponse) error {
		servicesSync.Lock()
		defer servicesSync.Unlock()

		err := ValidateAdminToken(sctx, input.Token, input.TenantId)
		if err != nil && err.Error() == "empty token" {
			MetricsAdminTokenUnauthenticatedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			sctx.LogHttpWarning().Str("http request GET /tenants/"+input.TenantId+"/keys", "").Str("result", "401")
			return status.Wrap(err, status.Unauthenticated)
		}
		if err != nil {
			MetricsAdminTokenPermissionDeniedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			sctx.LogHttpWarning().Str("http request GET /tenants/"+input.TenantId+"/keys", "").Str("result", "403")
			return status.Wrap(err, status.PermissionDenied)
		}

		err = ValidateTenantId(sctx, input.TenantId)
		if err != nil {
			return status.Wrap(err, status.InvalidArgument)
		}

		httpFlowKeys := &HttpFlowKeys{
			Sctx:    sctx,
			Request: *input,
		}

		if !httpFlowKeys.Exec() {
			if httpFlowKeys.Err != nil {
				sctx.LogHttpError().Str("http request ", "/keys").Str("result", "discarded").
					Str("flow", httpFlowKeys.Name).
					Stack().Err(httpFlowKeys.Err).
					Msg(httpFlowKeys.Msg)
			}
		}
		*output = httpFlowKeys.Response
		return nil
	})
	uc.SetTitle("Get Key")
	uc.SetDescription("Get key unused key for a specific tenant")
	uc.SetTags("Keys")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}

type KeysDeleteInput struct {
	KeysId   string `path:"keys_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	TenantId string `path:"tenant_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	Token    string `header:"Authorization" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
}

func DeleteKeyById(sctx *ServerContext) usecase.Interactor {
	servicesSync := sctx.ServicesSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input KeysDeleteInput, input2 *KeysDeleteInput) error {
		servicesSync.RLock()
		defer servicesSync.RUnlock()

		err := ValidateAdminToken(sctx, input.Token, input.TenantId)
		if err != nil && err.Error() == "empty token" {
			MetricsAdminTokenUnauthenticatedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			sctx.LogHttpWarning().Str("http request DELETE /tenants/"+input.TenantId+"/keys/"+input.KeysId, "").Str("result", "401")
			return status.Wrap(err, status.Unauthenticated)
		}
		if err != nil {
			MetricsAdminTokenPermissionDeniedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			sctx.LogHttpWarning().Str("http request DELETE /tenants/"+input.TenantId+"/keys/"+input.KeysId, "").Str("result", "403")
			return status.Wrap(err, status.PermissionDenied)
		}

		_, err = uuid.FromString(input.KeysId)
		if err != nil {
			return status.Wrap(err, status.InvalidArgument)
		}
		err = ValidateTenantId(sctx, input.TenantId)
		if err != nil {
			return status.Wrap(err, status.InvalidArgument)
		}
		err = DeleteKeys(sctx, input.TenantId, input.KeysId)
		if err != nil && (err.Error() == "container not found" || err.Error() == "not found") {
			return status.NotFound
		}
		if err != nil {
			sctx.LogHttpError().Str("http request ", "/keys").Str("result", "error").
				Str("flow", "delete keys").
				Stack().Err(err).
				Msg(err.Error())
			return status.Internal
		}

		return nil
	})

	uc.SetTitle("Delete Key")
	uc.SetDescription("Delete key used by specific tenant")
	uc.SetTags("Keys")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}

package server

import (
	"encoding/json"
	"errors"
	"strings"
	"time"

	"gitlab.com/opencontent/stanza-del-cittadino/pdnd-connector/server/models"
)

func StoreVoucher(sctx *ServerContext, voucherLog *models.VoucherLog) (string, error) {
	serverConfig := sctx.ServerConfig()

	basePath := serverConfig.StorageBasePath + "/vouchers/" + time.Now().Format("2006-01-02") + "/"
	logPath, err := StoreVoucherByPath(sctx, voucherLog, basePath)
	if err != nil {
		return logPath, err
	}

	return logPath, nil
}

func StoreVoucherByPath(sctx *ServerContext, voucherLog *models.VoucherLog, basePath string) (string, error) {
	fileStorage := sctx.FileStorage()

	itemBytes, err := json.Marshal(voucherLog)
	if err != nil {
		return "", errors.New("invalid data")
	}

	itemName := basePath + voucherLog.Tenant + "/" + voucherLog.ID + ".json"
	itemContent := string(itemBytes)
	itemSize := int64(len(itemContent))

	itemReader := strings.NewReader(itemContent)

	_, err = fileStorage.Put(itemName, itemReader, itemSize, nil)
	if err != nil {
		return "", err
	}
	return itemName, nil
}

package server

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/gofrs/uuid"
	"github.com/golang-jwt/jwt/v4"
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/opencontent/stanza-del-cittadino/pdnd-connector/server/models"
)

func GetVocuher(config ServerConfig, clientid string, clientAssertion string) (models.VoucherRes, string, error) {
	var requestURL string
	var voucher models.VoucherRes
	if config.PdndEnv == "collaudo" {
		requestURL = "https://auth.uat.interop.pagopa.it/token.oauth2"
	}
	if config.PdndEnv == "produzione" {
		requestURL = "https://auth.interop.pagopa.it/token.oauth2"
	}

	timer := prometheus.NewTimer(MetricsVoucherPdndLatency.WithLabelValues(config.Environment, APPNAME, requestURL))
	client := &http.Client{
		Timeout: time.Second * 10,
	}

	v := url.Values{}
	v.Set("client_id", clientid)
	v.Set("client_assertion", clientAssertion)
	v.Set("client_assertion_type", "urn:ietf:params:oauth:client-assertion-type:jwt-bearer")
	v.Set("grant_type", "client_credentials")
	encodedData := v.Encode()

	req, err := http.NewRequest("POST", requestURL, strings.NewReader(encodedData))
	if err != nil {
		msg := "internal error"
		return voucher, msg, errors.New("error performing voucher request: " + err.Error())
	}

	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	resp, err := client.Do(req)
	if err != nil {
		msg := "internal error"
		return voucher, msg, errors.New("error performing voucher request: " + err.Error())
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		msg := "internal error"
		return voucher, msg, errors.New("error reading voucher response request: " + err.Error())
	}
	if resp.StatusCode == http.StatusBadRequest {
		msg := "wrong input paramenter"
		return voucher, msg, errors.New("bad response retreving the voucher: " + string(body))
	}
	err = json.Unmarshal(body, &voucher)
	if err != nil {
		msg := "internal error"
		return voucher, msg, errors.New("error unmarshalling voucher response request " + err.Error())
	}
	timer.ObserveDuration()
	return voucher, "", nil
}

func DecodeVoucherJWT(jwtString string) (*models.Voucher, string, error) {
	// Parse the token without verification
	token, _, err := jwt.NewParser().ParseUnverified(jwtString, jwt.MapClaims{})
	if err != nil {
		msg := "internal error"
		return nil, msg, fmt.Errorf("error parsing the JWT: %w", err)
	}

	// Retrieve the claims
	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		msg := "internal error"
		return nil, msg, fmt.Errorf("error casting claims")
	}

	// Extract the header from the token
	header := token.Header

	// Serialize and deserialize the header into VoucherHeader
	headerBytes, err := json.Marshal(header)
	if err != nil {
		msg := "internal error"
		return nil, msg, fmt.Errorf("error serializing header: %w", err)
	}

	var voucherHeader models.VoucherHeader
	if err := json.Unmarshal(headerBytes, &voucherHeader); err != nil {
		msg := "internal error"
		return nil, msg, fmt.Errorf("error deserializing header into VoucherHeader: %w", err)
	}

	// Serialize and deserialize the claims into VoucherPayload
	payloadBytes, err := json.Marshal(claims)
	if err != nil {
		msg := "internal error"
		return nil, msg, fmt.Errorf("error serializing claims: %w", err)
	}

	var voucherPayload models.VoucherPayload
	if err := json.Unmarshal(payloadBytes, &voucherPayload); err != nil {
		msg := "internal error"
		return nil, msg, fmt.Errorf("error deserializing claims into VoucherPayload: %w", err)
	}

	// Build the Voucher object with header and payload
	voucher := &models.Voucher{
		Header:  voucherHeader,
		Payload: voucherPayload,
	}
	return voucher, "", nil
}

func BuildVoucherLog(voucher models.Voucher, tokenType, tenantId, userId, CofingId, eserviceVersion, eserviceSlug string) (models.VoucherLog, error) {
	logID, err := uuid.NewV4()
	if err != nil {
		return models.VoucherLog{}, fmt.Errorf("errore nella generazione dell'UUID: %w", err)
	}
	creationTime := time.Unix(voucher.Payload.IssuedAt, 0).UTC().Format(time.RFC3339)
	expirationTime := time.Unix(voucher.Payload.ExpiresAt, 0).UTC().Format(time.RFC3339)

	maximumRetentionTime := expirationTime

	// Construct the log entry
	logEntry := models.VoucherLog{
		ID:                        logID.String(),
		CreationTimestamp:         creationTime,
		ExpirationTimestamp:       expirationTime,
		MaximumRetentionTimestamp: maximumRetentionTime,
		TokenType:                 tokenType,
		EServiceSlug:              eserviceSlug,
		EServiceVersion:           eserviceVersion,
		Tenant:                    tenantId,
		UserID:                    userId,
		ConfigID:                  CofingId,
		Voucher:                   voucher,
	}

	return logEntry, nil
}

package server

import (
	"context"
	"time"

	uuid "github.com/satori/go.uuid"
	"github.com/swaggest/usecase"
	"github.com/swaggest/usecase/status"
)

type healthCheck struct {
	ID        string    `json:"id" required:"true" description:"..."`
	Status    string    `json:"status" required:"true" description:"..."`
	CheckedAt time.Time `json:"checked_at" required:"true" description:"..."`
}

func GetStatus(sctx *ServerContext) usecase.Interactor {

	uc := usecase.NewInteractor(func(ctx context.Context, _ struct{}, output *healthCheck) error {
		*output = healthCheck{
			ID:        uuid.NewV4().String(),
			Status:    "ok",
			CheckedAt: time.Now(),
		}

		return nil
	})

	uc.SetTitle("Status health-check")
	uc.SetDescription("...")
	uc.SetTags("Utility")
	uc.SetExpectedErrors(status.Internal)

	return uc
}

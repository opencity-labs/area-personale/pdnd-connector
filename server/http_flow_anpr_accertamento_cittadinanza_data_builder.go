package server

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"strings"
	"unicode"

	"gitlab.com/opencontent/stanza-del-cittadino/pdnd-connector/server/models/anpr"
)

const FORMAT_CITTADINANZA = "cittadinanza"

var AccertamentoCittadinanzaFormats = [1]string{FORMAT_CITTADINANZA}
var countries = map[string]string{
	"austria":         "Austria",
	"belgio":          "Belgium",
	"bulgaria":        "Bulgaria",
	"cipro":           "Cyprus",
	"repubblica ceca": "Czechia",
	"germania":        "Germany",
	"danimarca":       "Denmark",
	"estonia":         "Estonia",
	"spagna":          "Spain",
	"finlandia":       "Finland",
	"francia":         "France",
	"grecia":          "Greece",
	"croazia":         "Croatia",
	"ungheria":        "Hungary",
	"irlanda":         "Ireland",
	"italia":          "Italy",
	"lituania":        "Lithuania",
	"lussemburgo":     "Luxembourg",
	"lettonia":        "Latvia",
	"malta":           "Malta",
	"paesi bassi":     "Netherlands",
	"polonia":         "Poland",
	"portogallo":      "Portugal",
	"romania":         "Romania",
	"svezia":          "Sweden",
	"slovenia":        "Slovenia",
	"slovacchia":      "Slovakia",
}

type AccertamentoCittadinanzaDataBuilder struct {
	AccertamentoCittadinanzaResponse anpr.AccertamentoCittadinanza
}

func (r *AccertamentoCittadinanzaDataBuilder) GetFormatedData(format string) (*anpr.AccertamentoCittadinanzaFormattedData, error) {
	switch format {
	case FORMAT_CITTADINANZA:
		return r.buildDataResponseFormatRichiedente()
	}
	return nil, errors.New("invalid format for e-service accertamento Cittadinanza")
}

func (r *AccertamentoCittadinanzaDataBuilder) buildDataResponseFormatRichiedente() (*anpr.AccertamentoCittadinanzaFormattedData, error) {
	data := anpr.AccertamentoCittadinanzaFormattedData{}

	if len(r.AccertamentoCittadinanzaResponse.ListaSoggetti.DatiSoggetto) > 0 {
		aDatiSoggettoData := r.AccertamentoCittadinanzaResponse.ListaSoggetti.DatiSoggetto[0]

		for _, cittadinanza := range aDatiSoggettoData.Cittadinanza {
			citizenship := r.capitalizeFirstLetter(cittadinanza.DescrizioneStato)
			nationality, err := r.getNationality(citizenship)
			if err != nil {
				return &data, err
			}
			data.Cittadinanza = append(data.Cittadinanza, anpr.Citizenship{
				Cittadinanza: citizenship,
				Nationality:  nationality,
			})
		}
	}

	return &data, nil
}

func (r *AccertamentoCittadinanzaDataBuilder) IsCountryEuMember(country string) (bool, error) {
	country = r.capitalizeFirstLetter(country)
	countryEnglish, exists := countries[country]
	if !exists {
		return false, nil
	}

	url := fmt.Sprintf("https://api.opencityitalia.it/datasets/countries?countryName=%s&euMember=1", countryEnglish)
	resp, err := http.Get(url)
	if err != nil {
		return false, fmt.Errorf("errore nella richiesta HTTP: %v", err)
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return false, fmt.Errorf("errore nella lettura della risposta HTTP: %v", err)
	}
	var data []interface{}
	if err := json.Unmarshal(body, &data); err != nil {
		return false, fmt.Errorf("errore nel parsing della risposta JSON: %v", err)
	}

	return len(data) == 1, nil
}

func (r *AccertamentoCittadinanzaDataBuilder) getNationality(country string) (string, error) {
	country = strings.ToLower(country)
	if country == "italia" {
		return "italiana", nil
	}
	value, err := r.IsCountryEuMember(country)
	if err != nil {
		return "", err
	}
	if value {
		return "ue", nil
	}
	return "extra_ue", nil
}

func (r *AccertamentoCittadinanzaDataBuilder) capitalizeFirstLetter(s string) string {
	if len(s) == 0 {
		return s
	}
	runes := []rune(strings.ToLower(s))
	runes[0] = unicode.ToUpper(runes[0])
	return string(runes)
}

package server

import (
	"errors"

	"gitlab.com/opencontent/stanza-del-cittadino/pdnd-connector/server/models/anpr"
)

const FORMAT_STATO_FAMIGLIA_OC_FIGLI = "oc_figli"
const FORMAT_STATO_FAMIGLIA_OC_GENITORI = "oc_genitori"
const FORMAT_STATO_FAMIGLIA_OC_CONIUGE = "oc_coniuge"
const FORMAT_STATO_FAMIGLIA_OC_NUCLEO_FAMILIARE = "oc_nucleo_familiare"

var StatoFamigliaFormats = [4]string{FORMAT_STATO_FAMIGLIA_OC_FIGLI, FORMAT_STATO_FAMIGLIA_OC_GENITORI, FORMAT_STATO_FAMIGLIA_OC_CONIUGE, FORMAT_STATO_FAMIGLIA_OC_NUCLEO_FAMILIARE}

type StatoFamigliaDataBuilder struct {
	StatoFamigliaResponse anpr.StatoFamiglia

	FiscalCodeRichiedente string
}

func (r *StatoFamigliaDataBuilder) GetFormatedData(format string) (interface{}, error) {
	switch format {
	case FORMAT_STATO_FAMIGLIA_OC_FIGLI:
		return r.buildDataResponseFormatOcFigli()
	case FORMAT_STATO_FAMIGLIA_OC_GENITORI:
		return r.buildDataResponseFormatOcGenitori()
	case FORMAT_STATO_FAMIGLIA_OC_CONIUGE:
		return r.buildDataResponseFormatOcConiuge()
	case FORMAT_STATO_FAMIGLIA_OC_NUCLEO_FAMILIARE:
		return r.buildDataResponseFormatOcNucleoFamiliare()
	}
	return nil, errors.New("invalid format for e-service stato famiglia")
}

func (r *StatoFamigliaDataBuilder) buildDataResponseFormatOcFigli() (*anpr.StatoFamigliaFormattedDataOcFigli, error) {
	data := anpr.StatoFamigliaFormattedDataOcFigli{}
	data.Children = []anpr.Person{}
	if len(r.StatoFamigliaResponse.ListaSoggetti.DatiSoggetto) < 1 {
		return &data, nil
	}
	soggetti := r.StatoFamigliaResponse.ListaSoggetti.DatiSoggetto
	codiceLegameRichiedente, err := r.getCodiceLegameRichiedente(soggetti)
	if err != nil {
		return &data, err
	}
	if codiceLegameRichiedente == PARENT {
		r.buildDataFigli(soggetti, &data, CARD_HOLDER)
		return &data, nil
	}
	if r.isCardHolderOrSpouse(codiceLegameRichiedente) {
		r.buildDataFigli(soggetti, &data, CHILD)
		return &data, nil
	}
	return &data, errors.New("degree of kinship to find children unsupported")
}

func (r *StatoFamigliaDataBuilder) buildDataResponseFormatOcGenitori() (*anpr.StatoFamigliaFormattedDataOcGenitori, error) {
	data := anpr.StatoFamigliaFormattedDataOcGenitori{}
	data.Parents = []anpr.Person{}

	if len(r.StatoFamigliaResponse.ListaSoggetti.DatiSoggetto) < 1 {
		return &data, nil
	}
	soggetti := r.StatoFamigliaResponse.ListaSoggetti.DatiSoggetto
	codiceLegameRichiedente, err := r.getCodiceLegameRichiedente(soggetti)
	if err != nil {
		return &data, err
	}
	if codiceLegameRichiedente == CARD_HOLDER {
		r.buildDataGenitori(soggetti, &data, PARENT)
		return &data, nil
	}
	if codiceLegameRichiedente == SPOUSE {
		r.buildDataGenitori(soggetti, &data, PARENT_IN_LAW)
		return &data, nil
	}
	if codiceLegameRichiedente == CHILD {
		r.buildDataGenitori(soggetti, &data, CARD_HOLDER)
		r.buildDataGenitori(soggetti, &data, SPOUSE)
		return &data, nil
	}

	return &data, errors.New("degree of kinship to find children unsupported")
}

func (r *StatoFamigliaDataBuilder) buildDataResponseFormatOcConiuge() (*anpr.StatoFamigliaFormattedDataOcConiuge, error) {
	data := anpr.StatoFamigliaFormattedDataOcConiuge{}

	if len(r.StatoFamigliaResponse.ListaSoggetti.DatiSoggetto) < 1 {
		return &data, nil
	}
	soggetti := r.StatoFamigliaResponse.ListaSoggetti.DatiSoggetto
	codiceLegameRichiedente, err := r.getCodiceLegameRichiedente(soggetti)
	if err != nil {
		return &data, err
	}
	if codiceLegameRichiedente == CARD_HOLDER {
		r.buildDataConiuge(soggetti, &data, SPOUSE)
		return &data, nil
	}
	if codiceLegameRichiedente == SPOUSE {
		r.buildDataConiuge(soggetti, &data, CARD_HOLDER)
		return &data, nil
	}
	if codiceLegameRichiedente == PARENT {
		r.buildDataConiugeWhenCardHolderIsChild(soggetti, &data, PARENT)
		return &data, nil
	}
	return &data, errors.New("degree of kinship to find children unsupported")
}

func (r *StatoFamigliaDataBuilder) buildDataResponseFormatOcNucleoFamiliare() (*anpr.StatoFamigliaFormattedDataOcNucleoFamiliare, error) {
	data := anpr.StatoFamigliaFormattedDataOcNucleoFamiliare{}
	data.FamilyMember = []anpr.FamilyMember{}
	if len(r.StatoFamigliaResponse.ListaSoggetti.DatiSoggetto) < 1 {
		return &data, nil
	}
	soggetti := r.StatoFamigliaResponse.ListaSoggetti.DatiSoggetto
	for _, soggetto := range soggetti {
		birthDate, _ := ConvertDate(soggetto.Generalita.DataNascita)
		relationship, exists := anpr.FamilyMenberRelationshipMap[soggetto.LegameSoggetto.CodiceLegame]
		if !exists {
			return &data, errors.New("codice legame non mappato: " + soggetto.LegameSoggetto.CodiceLegame)
		}

		familyMember := anpr.FamilyMember{
			TaxID:      soggetto.Generalita.CodiceFiscale.CodFiscale,
			BirthDate:  birthDate,
			FamilyName: soggetto.Generalita.Cognome,
			GivenName:  soggetto.Generalita.Nome,
			RelatedTo:  relationship,
		}
		data.FamilyMember = append(data.FamilyMember, familyMember)
	}
	return &data, nil
}

func (r *StatoFamigliaDataBuilder) getCodiceLegameRichiedente(soggetti []anpr.DatiSoggetto) (string, error) {
	for _, soggetto := range soggetti {
		if soggetto.Generalita.CodiceFiscale.CodFiscale == r.FiscalCodeRichiedente {
			return soggetto.LegameSoggetto.CodiceLegame, nil
		}
	}
	return "", errors.New("richiedente non trovato")
}

func (r *StatoFamigliaDataBuilder) isCardHolderOrSpouse(codiceLegame string) bool {
	return codiceLegame == CARD_HOLDER || codiceLegame == SPOUSE
}

func (r *StatoFamigliaDataBuilder) buildDataFigli(aDatiSoggettoData []anpr.DatiSoggetto, data *anpr.StatoFamigliaFormattedDataOcFigli, CodiceLegame string) {
	var gender string
	for _, soggetto := range aDatiSoggettoData {
		if soggetto.LegameSoggetto.CodiceLegame == CodiceLegame {

			if soggetto.Generalita.Sesso == "F" {
				gender = "femmina"
			} else {
				gender = "maschio"
			}
			birthDate, _ := ConvertDate(soggetto.Generalita.DataNascita)
			child := anpr.Person{
				BirthDate:  birthDate,
				BirthPlace: soggetto.Generalita.LuogoNascita.Comune.NomeComune,
				FamilyName: soggetto.Generalita.Cognome,
				GivenName:  soggetto.Generalita.Nome,
				Gender:     gender,
				TaxID:      soggetto.Generalita.CodiceFiscale.CodFiscale,
			}
			data.Children = append(data.Children, child)
		}
	}
}

func (r *StatoFamigliaDataBuilder) buildDataGenitori(aDatiSoggettoData []anpr.DatiSoggetto, data *anpr.StatoFamigliaFormattedDataOcGenitori, CodiceLegame string) {
	var gender string
	for _, soggetto := range aDatiSoggettoData {
		if soggetto.LegameSoggetto.CodiceLegame == CodiceLegame {

			if soggetto.Generalita.Sesso == "F" {
				gender = "femmina"
			} else {
				gender = "maschio"
			}
			birthDate, _ := ConvertDate(soggetto.Generalita.DataNascita)
			parents := anpr.Person{
				BirthDate:  birthDate,
				BirthPlace: soggetto.Generalita.LuogoNascita.Comune.NomeComune,
				FamilyName: soggetto.Generalita.Cognome,
				GivenName:  soggetto.Generalita.Nome,
				Gender:     gender,
				TaxID:      soggetto.Generalita.CodiceFiscale.CodFiscale,
			}
			data.Parents = append(data.Parents, parents)
		}
	}
}

func (r *StatoFamigliaDataBuilder) buildDataConiugeWhenCardHolderIsChild(aDatiSoggettoData []anpr.DatiSoggetto, data *anpr.StatoFamigliaFormattedDataOcConiuge, CodiceLegame string) {
	for _, soggetto := range aDatiSoggettoData {
		if (soggetto.LegameSoggetto.CodiceLegame == CodiceLegame) && (soggetto.Generalita.CodiceFiscale.CodFiscale != r.FiscalCodeRichiedente) {
			var gender string
			if soggetto.Generalita.Sesso == "F" {
				gender = "femmina"
			} else {
				gender = "maschio"
			}

			BirthPlace := soggetto.Generalita.LuogoNascita.Comune.NomeComune
			FamilyName := soggetto.Generalita.Cognome
			GivenName := soggetto.Generalita.Nome
			TaxID := soggetto.Generalita.CodiceFiscale.CodFiscale

			birthDate, _ := ConvertDate(soggetto.Generalita.DataNascita)
			data.BirthDate = &birthDate
			data.BirthPlace = &BirthPlace
			data.FamilyName = &FamilyName
			data.GivenName = &GivenName
			data.Gender = &gender
			data.TaxID = &TaxID

		}
	}
}

func (r *StatoFamigliaDataBuilder) buildDataConiuge(aDatiSoggettoData []anpr.DatiSoggetto, data *anpr.StatoFamigliaFormattedDataOcConiuge, CodiceLegame string) {

	for _, soggetto := range aDatiSoggettoData {
		if soggetto.LegameSoggetto.CodiceLegame == CodiceLegame && soggetto.Generalita.CodiceFiscale.CodFiscale != r.FiscalCodeRichiedente {
			var gender string
			if soggetto.Generalita.Sesso == "F" {
				gender = "femmina"
			} else {
				gender = "maschio"
			}
			BirthPlace := soggetto.Generalita.LuogoNascita.Comune.NomeComune
			FamilyName := soggetto.Generalita.Cognome
			GivenName := soggetto.Generalita.Nome
			TaxID := soggetto.Generalita.CodiceFiscale.CodFiscale

			birthDate, _ := ConvertDate(soggetto.Generalita.DataNascita)
			data.BirthDate = &birthDate
			data.BirthPlace = &BirthPlace
			data.FamilyName = &FamilyName
			data.GivenName = &GivenName
			data.Gender = &gender
			data.TaxID = &TaxID

		}
	}
}

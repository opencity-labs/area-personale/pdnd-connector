package server

import (
	"encoding/json"
	"io"
	"os"
	"strings"

	"github.com/graymeta/stow"
	"github.com/graymeta/stow/azure"
	"github.com/graymeta/stow/local"
	"github.com/graymeta/stow/s3"
	"gitlab.com/opencontent/stanza-del-cittadino/pdnd-connector/server/models"
)

func connectFileStorage(sctx *ServerContext) (stow.Location, stow.Container) {
	serverConfig := sctx.ServerConfig()

	if serverConfig.StorageType != "s3" && serverConfig.StorageType != "azure" && serverConfig.StorageType != "local" {
		sctx.LogCoreFatal().Msg("storage type not supported")
	}

	s3DisableSsl := "false"
	if !serverConfig.StorageS3Ssl {
		s3DisableSsl = "true"
	}

	stowConfig := stow.ConfigMap{
		s3.ConfigAccessKeyID: serverConfig.StorageS3Key,
		s3.ConfigSecretKey:   serverConfig.StorageS3Secret,
		s3.ConfigRegion:      serverConfig.StorageS3Region,
		s3.ConfigEndpoint:    serverConfig.StorageS3Endpoint,
		s3.ConfigDisableSSL:  s3DisableSsl,
		azure.ConfigAccount:  serverConfig.StorageAzureAccount,
		azure.ConfigKey:      serverConfig.StorageAzureKey,
		local.ConfigKeyPath:  serverConfig.StorageLocalPath,
	}

	if serverConfig.StorageType == "local" {
		err2 := createFolderIfNotExists(serverConfig.StorageLocalPath + "/" + serverConfig.StorageBucket)
		if err2 != nil {
			sctx.LogCoreFatal().Stack().Err(err2).Msg("storage location creation failed")
		}
	}

	location, err := stow.Dial(serverConfig.StorageType, stowConfig)
	if err != nil {

		sctx.LogCoreFatal().Stack().Err(err).Msg("storage location initialization failed")
	}

	container, err := location.Container(serverConfig.StorageBucket)
	if err != nil {
		sctx.LogCoreFatal().Stack().Err(err).Msg("storage container initialization failed")
	}

	return location, container
}

func configsTenantsPreloader(sctx *ServerContext) {
	serverConfig := sctx.ServerConfig()
	fileStorage := sctx.FileStorage()
	basePath := serverConfig.StorageBasePath + "/tenants/"

	currentCursor := stow.CursorStart
	for {
		items, nextCursor, err := fileStorage.Items(basePath, currentCursor, 10)
		if err != nil {
			sctx.LogCoreFatal().Stack().Err(err).Msg("storage loader has failed")
		}

		for _, item := range items {
			name := item.Name()
			switch {
			case strings.HasSuffix(name, ".json"):
				itemReadCloser, err := item.Open()
				if err != nil {
					sctx.LogCoreError().Stack().Err(err).Msg("storage loader open error for tenants: " + item.Name())
					continue
				}
				defer itemReadCloser.Close()

				itemBytes, err := io.ReadAll(itemReadCloser)
				if err != nil {
					sctx.LogCoreError().Stack().Err(err).Msg("storage loader read error for tenants: " + item.Name())
					continue
				}

				var tenant models.Tenant
				err = json.Unmarshal(itemBytes, &tenant)
				if err != nil {
					sctx.LogCoreError().Stack().Err(err).Msg("storage loader unmarshal error for tenants: " + item.Name())
					continue
				}

				tenantCache := sctx.TenantsCache()
				tenantCache.Set(sctx.Ctx(), tenant.TenantID, &tenant)

			default:
				continue
			}
		}

		currentCursor = nextCursor
		if stow.IsCursorEnd(currentCursor) {
			break
		}
	}
}
func configsClientsPreloader(sctx *ServerContext) {
	serverConfig := sctx.ServerConfig()
	fileStorage := sctx.FileStorage()
	basePath := serverConfig.StorageBasePath + "/clients/"

	currentCursor := stow.CursorStart
	for {
		items, nextCursor, err := fileStorage.Items(basePath, currentCursor, 10)
		if err != nil {
			sctx.LogCoreFatal().Stack().Err(err).Msg("storage loader has failed")
		}

		for _, item := range items {
			name := item.Name()
			switch {
			case strings.HasSuffix(name, ".json"):
				itemReadCloser, err := item.Open()
				if err != nil {
					sctx.LogCoreError().Stack().Err(err).Msg("storage loader open error for clients: " + item.Name())
					continue
				}
				defer itemReadCloser.Close()

				itemBytes, err := io.ReadAll(itemReadCloser)
				if err != nil {
					sctx.LogCoreError().Stack().Err(err).Msg("storage loader read error for clients: " + item.Name())
					continue
				}

				var client models.ClientPDND
				err = json.Unmarshal(itemBytes, &client)
				if err != nil {
					sctx.LogCoreError().Stack().Err(err).Msg("storage loader unmarshal error for clients: " + item.Name())
					continue
				}

				clientsCache := sctx.ClientsCache()
				clientsCache.Set(sctx.Ctx(), client.ID, &client)

			default:
				continue
			}
		}

		currentCursor = nextCursor
		if stow.IsCursorEnd(currentCursor) {
			break
		}
	}
}
func configsCofingsPreloader(sctx *ServerContext) {
	serverConfig := sctx.ServerConfig()
	fileStorage := sctx.FileStorage()
	basePath := serverConfig.StorageBasePath + "/eservice_configs/"
	currentCursor := stow.CursorStart
	for {
		items, nextCursor, err := fileStorage.Items(basePath, currentCursor, 10)
		if err != nil {
			sctx.LogCoreFatal().Stack().Err(err).Msg("storage loader has failed")
		}
		for _, item := range items {
			name := item.Name()
			switch {
			case strings.HasSuffix(name, ".json"):
				itemReadCloser, err := item.Open()
				if err != nil {
					sctx.LogCoreError().Stack().Err(err).Msg("storage loader open error for configs: " + item.Name())
					continue
				}
				defer itemReadCloser.Close()

				itemBytes, err := io.ReadAll(itemReadCloser)
				if err != nil {
					sctx.LogCoreError().Stack().Err(err).Msg("storage loader read error for configs: " + item.Name())
					continue
				}

				var config models.Config
				err = json.Unmarshal(itemBytes, &config)
				if err != nil {
					sctx.LogCoreError().Stack().Err(err).Msg("storage loader unmarshal error for configs: " + item.Name())
					continue
				}

				configsCache := sctx.ConfigsCache()
				configsCache.Set(sctx.Ctx(), config.ID, &config)

			default:
				continue
			}
		}

		currentCursor = nextCursor
		if stow.IsCursorEnd(currentCursor) {
			break
		}
	}
}
func createFolderIfNotExists(folderPath string) error {
	// Check if the folder already exists
	_, err := os.Stat(folderPath)

	if os.IsNotExist(err) {
		err := os.MkdirAll(folderPath, 0775)
		if err != nil {
			return err
		}
	} else if err != nil {
		return err
	}

	err = os.Chmod(folderPath, 0775)
	if err != nil {
		return err
	}

	return nil
}

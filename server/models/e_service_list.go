package models

type EServiceList struct {
	Data []EService `json:"data"`
}

type EService struct {
	ID                 string `json:"id"`
	Version            string `json:"version"`
	Name               string `json:"name"`
	Slug               string `json:"slug"`
	AudClientAssertion string `json:"aud_client_assertion"`
	Audience           string `json:"audience"`
	Endpoint           string `json:"endpoint"`
}

type Meta struct {
	Page  Page `json:"page"`
	Total int  `json:"total"`
}

type Page struct {
	Offset int     `json:"offset"`
	Limit  int     `json:"limit"`
	Sort   *string `json:"sort,omitempty"`
}

type Links struct {
	Self string  `json:"self"`
	Prev *string `json:"prev,omitempty"`
	Next *string `json:"next,omitempty"`
}

type EServiceListResponse struct {
	Meta  Meta       `json:"meta"`
	Links Links      `json:"links"`
	Data  []EService `json:"data"`
}

const ESERVICELIST = `{
    "data": [
      {
        "id": "a152b46b-f330-494d-9845-82c460bc9fcf",
        "version": "2",
        "name": "C020 Accertamento Residenza",
        "slug": "anpr/accertamento-residenza",
        "aud_client_assertion": "auth.interop.pagopa.it/client-assertion",
        "audience": "https://modipa.anpr.interno.it/govway/rest/in/MinInternoPortaANPR/C020-servizioAccertamentoResidenza/v1",
        "endpoint":"https://modipa.anpr.interno.it/govway/rest/in/MinInternoPortaANPR-PDND/C020-servizioAccertamentoResidenza/v1/anpr-service-e002"
      },
      {
        "id": "c88d7efe-84a1-43c5-9edc-07f18a577289",
        "version": "3",
        "name": "C021 stato famiglia",
        "slug": "anpr/stato-famiglia",
        "aud_client_assertion": "auth.uat.interop.pagopa.it/client-assertion",
        "audience": "https://modipa-val.anpr.interno.it/govway/rest/in/MinInternoPortaANPR/C021-servizioAccertamentoStatoFamiglia/v1",
        "endpoint":"https://modipa-val.anpr.interno.it/govway/rest/in/MinInternoPortaANPR-PDND/C021-servizioAccertamentoStatoFamiglia/v1/anpr-service-e002"
      },
      {
        "id": "2",
        "version": "5",
        "name": "C023 test servizio",
        "aud_client_assertion": "auth.uat.interop.pagopa.it/client-assertion",
        "audience": "https://modipa-val.anpr.interno.it/govway/rest/in/MinInternoPortaANPR/C020-servizioAccertamentoResidenza/v1",
        "endpoint":"https://modipa-val.anpr.interno.it/govway/rest/in/MinInternoPortaANPR-PDND/C020-servizioAccertamentoResidenza/v1/anpr-service-e002"
      }
    ]
  }
`

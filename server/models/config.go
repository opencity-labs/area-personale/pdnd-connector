package models

type Config struct {
	ID         string `json:"id"`
	ClientID   string `json:"client_id"`
	EserviceID string `json:"eservice_id"`
	PurposeID  string `json:"purpose_id"`
	TenantID   string `json:"tenant_id"`
	CreatedAt  string `json:"created_at"`
	UpdatedAt  string `json:"updated_at"`
	CallUrl    string `json:"call_url"`
	IsActive   bool   `json:"is_active"`
}

type ConfigList struct {
	Data []Config `json:"data"`
}

type ConfigListResponse struct {
	Meta  Meta     `json:"meta"`
	Links Links    `json:"links"`
	Data  []Config `json:"data"`
}

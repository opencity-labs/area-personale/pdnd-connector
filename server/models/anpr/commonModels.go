package anpr

type Identificativi struct {
	IDANPR string `json:"idANPR"`
}

type LegameSoggetto struct {
	CodiceLegame   string `json:"codiceLegame"`
	DataDecorrenza string `json:"dataDecorrenza"`
	TipoLegame     string `json:"tipoLegame"`
}
type Comune struct {
	CodiceIstat         string `json:"codiceIstat"`
	NomeComune          string `json:"nomeComune"`
	SiglaProvinciaIstat string `json:"siglaProvinciaIstat"`
}

type LuogoDiNascita struct {
	Comune Comune `json:"comune"`
}

type CodiceFiscale struct {
	CodFiscale string `json:"codFiscale"`
	ValiditaCF string `json:"validitaCF"`
}

type Generalita struct {
	CodiceFiscale        CodiceFiscale  `json:"codiceFiscale"`
	Cognome              string         `json:"cognome"`
	DataNascita          string         `json:"dataNascita"`
	IDSchedaSoggettoANPR string         `json:"idSchedaSoggettoANPR"`
	LuogoNascita         LuogoDiNascita `json:"luogoNascita"`
	Nome                 string         `json:"nome"`
	Sesso                string         `json:"sesso"`
	SoggettoAIRE         string         `json:"soggettoAIRE"`
}
type Meta struct {
	Signature *string `json:"signature,omitempty"`
	Format    *string `json:"format,omitempty"`
	CreatedAt *string `json:"created_at,omitempty"`
	Source    *string `json:"source,omitempty"`
	CallUrl   *string `json:"call_url,omitempty"`
}

type ValidateAnpresponse struct {
	Result bool `json:"result"`
}

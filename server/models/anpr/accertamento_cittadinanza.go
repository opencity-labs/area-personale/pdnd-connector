package anpr

import "gitlab.com/opencontent/stanza-del-cittadino/pdnd-connector/server/models"

type AccertamentoCittadinanza struct {
	ListaSoggetti    ListaSoggettiCittadinanza `json:"listaSoggetti"`
	IDOperazioneANPR string                    `json:"idOperazioneANPR"`
	ListaAnomalie    []ListaAnomalie           `json:"listaAnomalie"`
}

type ListaSoggettiCittadinanza struct {
	DatiSoggetto []DatiSoggettoCittadinanza `json:"datiSoggetto"`
}

type DatiSoggettoCittadinanza struct {
	Generalita     Generalita     `json:"generalita"`
	Cittadinanza   []Cittadinanza `json:"cittadinanza"`
	Identificativi Identificativi `json:"identificativi"`
}

type LuogoNascita struct {
	Comune Comune `json:"comune"`
}

type Cittadinanza struct {
	CodiceStato      string `json:"codiceStato"`
	DataValidita     string `json:"dataValidita"`
	DescrizioneStato string `json:"descrizioneStato"`
}

type ListaAnomalie struct {
	CodiceErroreAnomalia string `json:"codiceErroreAnomalia"`
	TestoErroreAnomalia  string `json:"testoErroreAnomalia"`
	TipoErroreAnomalia   string `json:"tipoErroreAnomalia"`
}
type Anomalia struct {
	CodiceErroreAnomalia string `json:"codiceErroreAnomalia"`
	TestoErroreAnomalia  string `json:"testoErroreAnomalia"`
	TipoErroreAnomalia   string `json:"tipoErroreAnomalia"`
}

type AccertamentoCittadinanzaFormattedResponse struct {
	Data *AccertamentoCittadinanzaFormattedData `json:"data,omitempty"`
	Meta *Meta                                  `json:"meta,omitempty"`
}

type AccertamentoCittadinanzaFormattedData struct {
	Cittadinanza []Citizenship `json:"cittadinanza,omitempty"`
}

type Citizenship struct {
	Cittadinanza string `json:"country"`
	Nationality  string `json:"nationality"`
}

// Parametri per accertamento cittadinanza
type ParametersAccertamentoCittadinanza struct {
	ClientID                         string                   // ID del client
	ServiceAudience                  string                   // Audience del servizio
	JTI                              string                   // ID transazione
	KID                              string                   // Key ID
	PurposeID                        string                   // ID scopo
	ClientPDNDPrivateKey             string                   // Chiave privata PDND del client
	PayloadData                      string                   // Dati del payload
	UserId                           string                   // ID utente
	UserLocation                     string                   // Posizione dell'utente
	Loa                              string                   // Livello di autenticazione
	AuditAssertion                   string                   // Asserzione di audit
	ClientAssertion                  string                   // Asserzione del client
	AudClientAssertion               string                   // Asserzione client audit
	Signature                        string                   // Firma
	Voucher                          models.VoucherRes        // Voucher
	Digest64                         string                   // Digest in base64
	AccertamentoCittadinanzaEndpoint string                   // Endpoint accertamento cittadinanza
	EserviceSlug                     string                   // Slug del servizio
	EserviceName                     string                   // Nome del servizio
	EserviceID                       string                   // ID del servizio
	EserviceVersion                  string                   // Versione del servizio
	TenantId                         string                   // ID tenant
	PurposeId                        string                   // ID scopo
	AccertamentoCittadinanzaResponse AccertamentoCittadinanza // Risposta accertamento cittadinanza
}

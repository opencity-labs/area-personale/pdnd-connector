package anpr

import "gitlab.com/opencontent/stanza-del-cittadino/pdnd-connector/server/models"

type StatoFamiglia struct {
	ListaSoggetti ListaSoggetti `json:"listaSoggetti"`
	Signature     string        `json:"signature"`
}

type ListaSoggetti struct {
	DatiSoggetto []DatiSoggetto `json:"datiSoggetto"`
}

type DatiSoggetto struct {
	Generalita     Generalita     `json:"generalita"`
	LegameSoggetto LegameSoggetto `json:"legameSoggetto"`
	Identificativi Identificativi `json:"identificativi"`
}

type StatoFamigliaFormattedResponse struct {
	Data interface{} `json:"data,omitempty"`
	Meta *Meta       `json:"meta,omitempty"`
}

type StatoFamigliaFormattedDataOcConiuge struct {
	BirthDate  *string `json:"birth_date,omitempty"`
	BirthPlace *string `json:"birth_place,omitempty"`
	FamilyName *string `json:"family_name,omitempty"`
	Gender     *string `json:"gender,omitempty"`
	GivenName  *string `json:"given_name,omitempty"`
	TaxID      *string `json:"tax_id,omitempty"`
}

type StatoFamigliaFormattedDataOcFigli struct {
	Children []Person `json:"children"`
}
type StatoFamigliaFormattedDataOcGenitori struct {
	Parents []Person `json:"parents"`
}
type StatoFamigliaFormattedDataOcNucleoFamiliare struct {
	FamilyMember []FamilyMember `json:"family_unit"`
}

type FamilyMember struct {
	BirthDate  string `json:"birth_date"`
	FamilyName string `json:"family_name"`
	GivenName  string `json:"given_name"`
	RelatedTo  string `json:"related_to"`
	TaxID      string `json:"tax_id"`
}

type Person struct {
	BirthDate  string `json:"birth_date"`
	BirthPlace string `json:"birth_place"`
	FamilyName string `json:"family_name"`
	Gender     string `json:"gender"`
	GivenName  string `json:"given_name"`
	TaxID      string `json:"tax_id"`
}

type ParametersStatoFamiglia struct {
	ClientID              string
	ServiceAudience       string
	JTI                   string
	KID                   string
	ClientPDNDPrivateKey  string
	PayloadData           string
	UserId                string
	UserLocation          string
	Loa                   string
	AuditAssertion        string
	ClientAssertion       string
	AudClientAssertion    string
	Signature             string
	Voucher               models.VoucherRes
	Digest64              string
	StatoFamigliaEndpoint string
	EserviceSlug          string
	EserviceName          string
	EserviceID            string
	EserviceVersion       string
	TenantId              string
	PurposeID             string
	StatoFamigliaResponse StatoFamiglia
}

var FamilyMenberRelationshipMap = map[string]string{
	"26": "tutore",
	"1":  "intestatario",
	"2":  "marito/moglie",
	"3":  "figlio/figlia",
	"4":  "nipote (discendente)",
	"5":  "pronipote (discendente)",
	"6":  "padre/madre",
	"7":  "nonno/nonna",
	"8":  "bisnonno/bisnonna",
	"9":  "fratello/sorella",
	"10": "nipote (collaterale)",
	"11": "zio/zia (collaterale)",
	"12": "cugino/cugina",
	"13": "altro parente",
	"16": "genero/nuora",
	"17": "suocero/suocera",
	"18": "cognato/cognata",
	"20": "nipote (affine)",
	"21": "zio/zia (affine)",
	"22": "altro affine",
	"23": "convivente (con vincoli di adozione o affettivi)",
	"24": "responsabile della convivenza non affettiva",
	"25": "convivente in convivenza non affettiva",
	"99": "non definito/comunicato",
	"14": "figliastro/figliastra",
	"15": "patrigno/matrigna",
	"19": "fratellastro/sorellastra",
	"80": "adottato",
	"81": "nipote",
	"28": "unito civilmente",
}

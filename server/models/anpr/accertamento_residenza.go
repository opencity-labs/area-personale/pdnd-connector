package anpr

import "gitlab.com/opencontent/stanza-del-cittadino/pdnd-connector/server/models"

type AccertamentoResidenza struct {
	ListaSoggetti    ListaSoggettiAR `json:"listaSoggetti"`
	ListaAnomalie    []AnomaliaAR    `json:"listaAnomalie"`
	IDOperazioneANPR string          `json:"idOperazioneANPR"`
}

type ListaSoggettiAR struct {
	DatiSoggetto []DatiSoggettoAR `json:"datiSoggetto"`
}

type DatiSoggettoAR struct {
	Generalita       GeneralitaAR         `json:"generalita"`
	Residenza        []ResidenzaAR        `json:"residenza"`
	Identificativi   IdentificativiAR     `json:"identificativi"`
	InfoSoggettoEnte []InfoSoggettoEnteAR `json:"infoSoggettoEnte"`
}

type GeneralitaAR struct {
	CodiceFiscale          CodiceFiscaleAR  `json:"codiceFiscale"`
	Cognome                string           `json:"cognome"`
	SenzaCognome           string           `json:"senzaCognome"`
	Nome                   string           `json:"nome"`
	SenzaNome              string           `json:"senzaNome"`
	Sesso                  string           `json:"sesso"`
	DataNascita            string           `json:"dataNascita"`
	SenzaGiorno            string           `json:"senzaGiorno"`
	SenzaGiornoMese        string           `json:"senzaGiornoMese"`
	LuogoNascita           LuogoNascitaAR   `json:"luogoNascita"`
	SoggettoAIRE           string           `json:"soggettoAIRE"`
	AnnoEspatrio           string           `json:"annoEspatrio"`
	IDSchedaSoggettoComune IDSchedaComuneAR `json:"idSchedaSoggettoComune"`
	IDSchedaSoggettoANPR   string           `json:"idSchedaSoggettoANPR"`
	Note                   string           `json:"note"`
}

type CodiceFiscaleAR struct {
	CodFiscale               string `json:"codFiscale"`
	ValiditaCF               string `json:"validitaCF"`
	DataAttribuzioneValidita string `json:"dataAttribuzioneValidita"`
}
type LuogoNascitaAR struct {
	LuogoEccezionale string     `json:"luogoEccezionale"`
	Comune           ComuneAR   `json:"comune"`
	Localita         LocalitaAR `json:"localita"`
}

type ComuneAR struct {
	NomeComune          string `json:"nomeComune"`
	CodiceIstat         string `json:"codiceIstat"`
	SiglaProvinciaIstat string `json:"siglaProvinciaIstat"`
	DescrizioneLocalita string `json:"descrizioneLocalita"`
}

type LocalitaAR struct {
	DescrizioneLocalita string `json:"descrizioneLocalita"`
	DescrizioneStato    string `json:"descrizioneStato"`
	CodiceStato         string `json:"codiceStato"`
	ProvinciaContea     string `json:"provinciaContea"`
}

type IDSchedaComuneAR struct {
	IDSchedaSoggettoComuneIstat string `json:"idSchedaSoggettoComuneIstat"`
	IDSchedaSoggetto            string `json:"idSchedaSoggetto"`
}

type ResidenzaAR struct {
	TipoIndirizzo           string           `json:"tipoIndirizzo"`
	NoteIndirizzo           string           `json:"noteIndirizzo"`
	Indirizzo               IndirizzoAR      `json:"indirizzo"`
	LocalitaEstera          LocalitaEsteraAR `json:"localitaEstera"`
	Presso                  string           `json:"presso"`
	DataDecorrenzaResidenza string           `json:"dataDecorrenzaResidenza"`
}

type IndirizzoAR struct {
	Cap          string         `json:"cap"`
	Comune       ComuneAR       `json:"comune"`
	Frazione     string         `json:"frazione"`
	Toponimo     ToponimoAR     `json:"toponimo"`
	NumeroCivico NumeroCivicoAR `json:"numeroCivico"`
}

type NumeroCivicoAR struct {
	CodiceCivico  string          `json:"codiceCivico"`
	CivicoFonte   string          `json:"civicoFonte"`
	Numero        string          `json:"numero"`
	Metrico       string          `json:"metrico"`
	ProgSNC       string          `json:"progSNC"`
	Lettera       string          `json:"lettera"`
	Esponente1    string          `json:"esponente1"`
	Colore        string          `json:"colore"`
	CivicoInterno CivicoInternoAR `json:"civicoInterno"`
}

type CivicoInternoAR struct {
	Corte        string `json:"corte"`
	Scala        string `json:"scala"`
	Interno1     string `json:"interno1"`
	EspInterno1  string `json:"espInterno1"`
	Interno2     string `json:"interno2"`
	EspInterno2  string `json:"espInterno2"`
	ScalaEsterna string `json:"scalaEsterna"`
	Secondario   string `json:"secondario"`
	Piano        string `json:"piano"`
	Nui          string `json:"nui"`
	Isolato      string `json:"isolato"`
}

type ToponimoAR struct {
	CodSpecie             string `json:"codSpecie"`
	Specie                string `json:"specie"`
	SpecieFonte           string `json:"specieFonte"`
	CodToponimo           string `json:"codToponimo"`
	DenominazioneToponimo string `json:"denominazioneToponimo"`
	ToponimoFonte         string `json:"toponimoFonte"`
}

type LocalitaEsteraAR struct {
	IndirizzoEstero IndirizzoEsteroAR `json:"indirizzoEstero"`
	Consolato       ConsolatoAR       `json:"consolato"`
}

type IndirizzoEsteroAR struct {
	Cap      string     `json:"cap"`
	Localita LocalitaAR `json:"localita"`
	Toponimo struct {
		Denominazione string `json:"denominazione"`
		NumeroCivico  string `json:"numeroCivico"`
	} `json:"toponimo"`
}

type ConsolatoAR struct {
	CodiceConsolato      string `json:"codiceConsolato"`
	DescrizioneConsolato string `json:"descrizioneConsolato"`
}

type IdentificativiAR struct {
	IDANPR string `json:"idANPR"`
}

type InfoSoggettoEnteAR struct {
	ID          string `json:"id"`
	Chiave      string `json:"chiave"`
	Valore      string `json:"valore"`
	ValoreTesto string `json:"valoreTesto"`
	ValoreData  string `json:"valoreData"`
	Dettaglio   string `json:"dettaglio"`
}

type AnomaliaAR struct {
	CodiceErroreAnomalia  string `json:"codiceErroreAnomalia"`
	TipoErroreAnomalia    string `json:"tipoErroreAnomalia"`
	TestoErroreAnomalia   string `json:"testoErroreAnomalia"`
	OggettoErroreAnomalia string `json:"oggettoErroreAnomalia"`
	CampoErroreAnomalia   string `json:"campoErroreAnomalia"`
	ValoreErroreAnomalia  string `json:"valoreErroreAnomalia"`
}

type AccertamentoResidenzaFormattedResponse struct {
	Data *AccertamentoResidenzaFormattedData `json:"data,omitempty"`
	Meta *Meta                               `json:"meta,omitempty"`
}

type AccertamentoResidenzaFormattedData struct {
	Address               *string      `json:"address,omitempty"`
	HouseNumber           *string      `json:"house_number,omitempty"`
	Municipality          *string      `json:"municipality,omitempty"`
	County                *string      `json:"county,omitempty"`
	Country               *string      `json:"country,omitempty"`
	PostalCode            *string      `json:"postal_code,omitempty"`
	AddressData           *AddressData `json:"address_data,omitempty"`
	MunicipalityIstatCode *string      `json:"codice_istat_comune,omitempty"`
	LocatorWithin         *string      `json:"locator_within,omitempty"`
}

type AddressData struct {
	Province         *string `json:"pr"`
	PlaceIstat       *string `json:"luogo_istat"`
	Municipality     *string `json:"comune"`
	MunicipalityCode *int    `json:"codice_comune"`
	PostalCode       *string `json:"cap"`
}

type ParametersAccertamentoResidenza struct {
	ClientID                      string
	ServiceAudience               string
	JTI                           string
	KID                           string
	PurposeID                     string
	ClientPDNDPrivateKey          string
	PayloadData                   string
	UserId                        string
	UserLocation                  string
	Loa                           string
	AuditAssertion                string
	ClientAssertion               string
	AudClientAssertion            string
	Signature                     string
	Voucher                       models.VoucherRes
	Digest64                      string
	AccertamentoResidenzaEndpoint string
	EserviceSlug                  string
	EserviceName                  string
	EserviceID                    string
	EserviceVersion               string
	TenantId                      string
	PurposeId                     string
	AccertamentoResidenzaResponse AccertamentoResidenza
}

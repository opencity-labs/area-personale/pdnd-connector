package models

type ClientPDND struct {
	ID        string `json:"id"`
	Name      string `json:"name"`
	TenantID  string `json:"tenant_id"`
	Kid       string `json:"key_id"`
	Env       string `json:"env"`
	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
	KeyPairId string `json:"key_pair_id"`
	PublicKey string `json:"public_key"`
}

type ClientsList struct {
	Data []ClientPDND `json:"data"`
}

type ClientListResponse struct {
	Meta  Meta         `json:"meta"`
	Links Links        `json:"links"`
	Data  []ClientPDND `json:"data"`
}

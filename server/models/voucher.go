package models

type VoucherRes struct {
	AccessToken string `json:"access_token"`
	ExpiresIn   int    `json:"expires_in"`
	TokenType   string `json:"token_type"`
}

type VoucherData struct {
	ID         string  `json:"id"`
	ConfigID   string  `json:"config_id"`
	TenantID   string  `json:"tenant_id"`
	UserID     string  `json:"user_id,omitempty"`
	Voucher    Voucher `json:"voucher"`
	SavingTime string  `json:"saving_time"`
}

type Voucher struct {
	Header  VoucherHeader  `json:"header"`
	Payload VoucherPayload `json:"payload"`
}

type VoucherHeader struct {
	Type string `json:"typ"`
	Alg  string `json:"alg"`
	Use  string `json:"use"`
	Kid  string `json:"kid"`
}

type VoucherPayload struct {
	Audience  string `json:"aud"`
	Subject   string `json:"sub"`
	NotBefore int64  `json:"nbf"`
	Digest    Digest `json:"digest"`
	PurposeID string `json:"purposeId"`
	Issuer    string `json:"iss"`
	ExpiresAt int64  `json:"exp"`
	IssuedAt  int64  `json:"iat"`
	ClientID  string `json:"client_id"`
	JTI       string `json:"jti"`
}

type Digest struct {
	Algorithm string `json:"alg"`
	Value     string `json:"value"`
}

type VoucherLog struct {
	ID                        string  `json:"id"`
	CreationTimestamp         string  `json:"creation_timestamp"`
	ExpirationTimestamp       string  `json:"expiration_timestamp"`
	MaximumRetentionTimestamp string  `json:"maximum_retention_timestamp"`
	TokenType                 string  `json:"token_type"`
	EServiceSlug              string  `json:"eservice_slug"`
	EServiceVersion           string  `json:"eservice_version"`
	Tenant                    string  `json:"tenant"`
	UserID                    string  `json:"user_id"`
	ConfigID                  string  `json:"config_id"`
	Voucher                   Voucher `json:"voucher"`
}

type VoucherField struct {
	ID          string `json:"id"`
	Path        string `json:"path"`
	StorageType string `json:"storage_type"`
}

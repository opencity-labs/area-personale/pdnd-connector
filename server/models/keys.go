package models

type KeysResponse struct {
	PublicKey string `json:"public_key"`
	KeysId    string `json:"id"`
}

type Keys struct {
	PublicKey  string `json:"public_key"`
	PrivateKey string `json:"private_key"`
	KeysId     string `json:"id"`
}

package models

type Service struct {
	Data struct {
		TenantID            string `json:"tenant_id" example:"example_tenant_id"`
		ID                  string `json:"id" example:"12"`
		Typ                 string `json:"typ"`
		Alg                 string `json:"alg"`
		Kid                 string `json:"kid"`
		PurposeID           string `json:"purposeId"`
		ClientID            string `json:"clientId"`
		ClientAssertionType string `json:"clientAssertionType"`
		GrantType           string `json:"grantType"`
	} `json:"data"`
	Metadata struct{} `json:"metadata"`
}

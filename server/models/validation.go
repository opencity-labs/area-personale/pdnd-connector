package models

type Jwt struct {
	Plaintext []byte
	Payload   map[string]interface{}
	Signature []byte
}

type JwtClaims struct {
	Exp      int      `json:"exp"`
	Iat      int      `json:"iat"`
	ID       string   `json:"id"`
	Roles    []string `json:"roles"`
	TenantID string   `json:"tenant_id"`
	Username string   `json:"username"`
}

type SdcJWKSPublicKey struct {
	Keys []struct {
		Alg string `json:"alg"`
		Kid string `json:"kid"`
		Kty string `json:"kty"`
		N   string `json:"n"`
		E   string `json:"e"`
		Use string `json:"use"`
	} `json:"keys"`
}

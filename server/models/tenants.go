package models

type Tenant struct {
	TenantID  string `json:"id"`
	Name      string `json:"name"`
	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
	IPaCode   string `json:"ipa_code"`
}

package models

import "github.com/golang-jwt/jwt"

type SdcTokenClaims struct {
	Exp      int64    `json:"exp"`
	Iat      int64    `json:"iat"`
	ID       string   `json:"id"`
	Roles    []string `json:"roles"`
	TenantID string   `json:"tenant_id"`
	Username string   `json:"username"`
	jwt.StandardClaims
}

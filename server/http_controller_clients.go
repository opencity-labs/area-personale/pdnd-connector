package server

import (
	"context"
	"errors"

	"github.com/gofrs/uuid"
	"github.com/swaggest/usecase"
	"github.com/swaggest/usecase/status"
	"gitlab.com/opencontent/stanza-del-cittadino/pdnd-connector/server/models"
)

type ClientPostInput struct {
	TenantId  string `path:"tenant_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	ID        string `json:"id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	Name      string `json:"name" description:"nome del tenant" example:"comune di Bugliano"`
	Kid       string `json:"key_id" description:"key id del client" example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	Env       string `json:"env" description:"ambiente del client" example:"collaudo"`
	KeyPairId string `json:"key_pair_id" description:"id interno del materiale critografico"`
	Token     string `header:"Authorization" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
}

func CreateClient(sctx *ServerContext) usecase.Interactor {
	servicesSync := sctx.ServicesSync()
	clientsCache := sctx.ClientsCache()

	uc := usecase.NewInteractor(func(ctx context.Context, input ClientPostInput, output *models.ClientPDND) error {
		servicesSync.Lock()
		defer servicesSync.Unlock()

		err := ValidateAdminToken(sctx, input.Token, input.TenantId)
		if err != nil && err.Error() == "empty token" {
			MetricsAdminTokenUnauthenticatedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			return status.Wrap(err, status.Unauthenticated)
		}
		if err != nil {
			MetricsAdminTokenPermissionDeniedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			sctx.LogHttpWarning().Str("http request POST /tenants/"+input.TenantId+"/clients", "").Str("result", "403")
			return status.Wrap(err, status.PermissionDenied)
		}
		// Step 2: Validate required fields
		if err := validateClientPostPDND(sctx, input); err != nil {
			return status.Wrap(err, status.InvalidArgument)
		}
		publicKey, err := validateKeyPairId(sctx, input.TenantId, input.KeyPairId)
		if err != nil {
			return status.Wrap(err, status.InvalidArgument)
		}

		_, err = clientsCache.Get(ctx, input.ID)
		if err == nil {
			return status.AlreadyExists.Status()
		}
		output.ID = input.ID
		output.Name = input.Name
		output.TenantID = input.TenantId
		output.Kid = input.Kid
		output.Env = input.Env
		output.KeyPairId = input.KeyPairId
		romeTime := GetCurrentDateTime()
		output.CreatedAt = romeTime
		output.UpdatedAt = romeTime
		output.PublicKey = publicKey

		err = StoreClientPDND(sctx, output)
		if err != nil && err.Error() == "invalid data" {
			return status.InvalidArgument
		}
		if err != nil {
			return status.Wrap(err, status.NotFound)
		}

		return nil
	})

	uc.SetTitle("store  client pdnd Configuration")
	uc.SetDescription("Save client pdnd Configuration of specific tenant")
	uc.SetTags("Clients")
	uc.SetExpectedErrors(status.InvalidArgument, status.PermissionDenied)

	return uc
}

type GetClientsByTenantIdInput struct {
	TenantId string `path:"tenant_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	Limit    int    `query:"limit" description:"The limit parameter specifies the maximum number of items to be returned in a single page or request"`
	Sort     string `query:"sort" description:"..."`
	Offset   int    `query:"offset" description:" the starting point or the index from which the data should be retrieved" example:"6"`
	Token    string `header:"Authorization" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
}

func GetClientsByTenantId(sctx *ServerContext) usecase.Interactor {
	servicesSync := sctx.ServicesSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input GetClientsByTenantIdInput, output *models.ClientListResponse) error {
		servicesSync.RLock()
		defer servicesSync.RUnlock()

		err := ValidateAdminToken(sctx, input.Token, input.TenantId)
		if err != nil && err.Error() == "empty token" {
			MetricsAdminTokenUnauthenticatedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			sctx.LogHttpWarning().Str("http request GET /tenants/"+input.TenantId+"/clients", "").Str("result", "401")
			return status.Wrap(err, status.Unauthenticated)
		}
		if err != nil {
			MetricsAdminTokenPermissionDeniedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			sctx.LogHttpWarning().Str("http request GET /tenants/"+input.TenantId+"/clients", "").Str("result", "403")
			return status.Wrap(err, status.PermissionDenied)
		}
		_, err = uuid.FromString(input.TenantId)
		if err != nil {
			return status.Wrap(err, status.InvalidArgument)
		}

		httpFlowClientList := &HttpFlowClientsList{
			Request: input,
			Sctx:    sctx,
		}

		if !httpFlowClientList.Exec() {
			if httpFlowClientList.Err != nil {
				sctx.LogHttpError().Str("http request ", "/tenants/"+input.TenantId+"/clients").Str("result", "discarded").
					Str("flow", httpFlowClientList.Name).
					Stack().Err(httpFlowClientList.Err).
					Msg(httpFlowClientList.Msg)
			}
		}
		if len(httpFlowClientList.Response.Data) == 0 {
			return status.NotFound
		}

		*output = httpFlowClientList.Response
		return nil
	})

	uc.SetTitle("Get Clients list")
	uc.SetDescription("Get Clients Configuration list for specific tenant")
	uc.SetTags("Clients")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}

type ClientIdInput struct {
	ClientId string `path:"client_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	TenantId string `path:"tenant_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	Token    string `header:"Authorization" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
}

func GetClientByClientId(sctx *ServerContext) usecase.Interactor {
	servicesSync := sctx.ServicesSync()
	clientsCache := sctx.ClientsCache()

	uc := usecase.NewInteractor(func(ctx context.Context, input ClientIdInput, output *models.ClientPDND) error {
		servicesSync.RLock()
		defer servicesSync.RUnlock()

		err := ValidateAdminToken(sctx, input.Token, input.TenantId)
		if err != nil && err.Error() == "empty token" {
			MetricsAdminTokenUnauthenticatedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			sctx.LogHttpWarning().Str("http request GET /tenants/"+input.TenantId+"/clients/"+input.ClientId, "").Str("result", "401")
			return status.Wrap(err, status.Unauthenticated)
		}
		if err != nil {
			MetricsAdminTokenPermissionDeniedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			sctx.LogHttpWarning().Str("http request GET /tenants/"+input.TenantId+"/clients/"+input.ClientId, "").Str("result", "403")
			return status.Wrap(err, status.PermissionDenied)
		}

		_, err = uuid.FromString(input.ClientId)
		if err != nil {
			return status.Wrap(err, status.InvalidArgument)
		}
		err = ValidateTenantId(sctx, input.TenantId)
		if err != nil {
			return status.Wrap(err, status.InvalidArgument)
		}

		client, err := clientsCache.Get(ctx, input.ClientId)
		if err != nil && err.Error() == "value not found in store" {
			return status.Wrap(errors.New("no client found for id "+input.ClientId), status.NotFound)
		}

		if err != nil {
			sctx.LogHttpError().Stack().Err(err).Msg("get client by id error for: " + input.ClientId)
			return status.Internal
		}
		if client.TenantID != input.TenantId {
			return status.Wrap(errors.New("client does not belong to your tenant"), status.InvalidArgument)
		}
		*output = *client

		return nil
	})

	uc.SetTitle("Get client pdnd Configuration")
	uc.SetDescription("Get client pdnd Configuration of specific tenant")
	uc.SetTags("Clients")
	uc.SetExpectedErrors(status.InvalidArgument, status.PermissionDenied)

	return uc
}

type ClientUpdateInput struct {
	TenantId  string `path:"tenant_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	ClientId  string `path:"client_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	Name      string `json:"name" description:"nome del tenant" example:"comune di Bugliano"`
	Kid       string `json:"key_id" description:"key id del client" example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	Env       string `json:"env" description:"ambiente del client" example:"collaudo"`
	KeyPairId string `json:"key_pair_id" description:"id interno del materiale critografico"`
	Token     string `header:"Authorization" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
}

func UpdateClient(sctx *ServerContext) usecase.Interactor {
	clientsCache := sctx.ClientsCache()
	servicesSync := sctx.ServicesSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input ClientUpdateInput, output *models.ClientPDND) error {
		servicesSync.Lock()
		defer servicesSync.Unlock()

		err := ValidateAdminToken(sctx, input.Token, input.TenantId)
		if err != nil && err.Error() == "empty token" {
			MetricsAdminTokenUnauthenticatedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			sctx.LogHttpWarning().Str("http request UPDATE /tenants/"+input.TenantId+"/clients/"+input.ClientId, "").Str("result", "401")
			return status.Wrap(err, status.Unauthenticated)
		}
		if err != nil {
			MetricsAdminTokenPermissionDeniedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			sctx.LogHttpWarning().Str("http request UPDATE /tenants/"+input.TenantId+"/clients/"+input.ClientId, "").Str("result", "403")
			return status.Wrap(err, status.PermissionDenied)
		}
		//check input payload
		if err := validateClientUpdatePDND(sctx, input); err != nil {
			return status.Wrap(err, status.InvalidArgument)
		}

		shouldCreateNewClient := false
		//check if client already exists
		client, err := clientsCache.Get(ctx, input.ClientId)
		if err != nil && err.Error() == "value not found in store" {
			shouldCreateNewClient = true
		} else if err != nil && err.Error() != "not found" {
			sctx.LogHttpError().Stack().Err(err).Msg("get client by id error for: " + input.ClientId)
			return status.Wrap(err, status.Internal)
		}

		keyPairIdAlreadyUsed := false

		//check if key_pair_id is valid and belogs to any key
		publicKey, err := validateKeyPairId(sctx, input.TenantId, input.KeyPairId)
		if err != nil && err.Error() == "key_pair_id already used" {
			keyPairIdAlreadyUsed = true
		} else if err != nil && err.Error() != "key_pair_id already used" {

			return status.Wrap(errors.New("error validating key_pair_id: "+err.Error()), status.InvalidArgument)
		}

		romeTime := GetCurrentDateTime()
		if shouldCreateNewClient {

			output.CreatedAt = romeTime
			output.PublicKey = publicKey
			output.KeyPairId = input.KeyPairId
		} else {
			if keyPairIdAlreadyUsed && client.PublicKey != publicKey {
				return status.Wrap(errors.New("key_pair_id already used by another client"), status.InvalidArgument)
			}
			if keyPairIdAlreadyUsed && client.PublicKey == publicKey {
				output.KeyPairId = client.KeyPairId
				output.PublicKey = publicKey
			}
			//if they are different, we have a new key, and we should delete the old one
			if client.KeyPairId != input.KeyPairId {
				_ = deleteOldKey(sctx, input.TenantId, client.KeyPairId)
				output.PublicKey = publicKey
				output.KeyPairId = input.KeyPairId
			}
			output.CreatedAt = client.CreatedAt
		}
		output.ID = input.ClientId
		output.Name = input.Name
		output.TenantID = input.TenantId
		output.Kid = input.Kid
		output.Env = input.Env
		output.UpdatedAt = romeTime

		err = StoreClientPDND(sctx, output)
		if err != nil {
			return status.Wrap(errors.New("error storing client"), status.Internal)
		}
		return nil
	})

	uc.SetTitle("Update client pdnd Configuration")
	uc.SetDescription("Update client pdnd Configuration of specific tenant")
	uc.SetTags("Clients")
	uc.SetExpectedErrors(status.InvalidArgument, status.PermissionDenied)

	return uc
}

func PatchClient(sctx *ServerContext) usecase.Interactor {
	clientsCache := sctx.ClientsCache()
	servicesSync := sctx.ServicesSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input ClientUpdateInput, output *models.ClientPDND) error {
		servicesSync.Lock()
		defer servicesSync.Unlock()

		err := ValidateAdminToken(sctx, input.Token, input.TenantId)
		if err != nil && err.Error() == "empty token" {
			MetricsAdminTokenUnauthenticatedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			sctx.LogHttpWarning().Str("http request PATCH /tenants/"+input.TenantId+"/clients/"+input.ClientId, "").Str("result", "401")
			return status.Wrap(err, status.Unauthenticated)
		}
		if err != nil {
			MetricsAdminTokenPermissionDeniedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			sctx.LogHttpWarning().Str("http request PATCH /tenants/"+input.TenantId+"/clients/"+input.ClientId, "").Str("result", "403")
			return status.Wrap(err, status.PermissionDenied)
		}
		if err := validateClientPatchPDND(sctx, input); err != nil {
			return status.Wrap(err, status.InvalidArgument)
		}

		client, err := clientsCache.Get(ctx, input.ClientId)
		if err != nil && err.Error() == "value not found in store" {
			return status.Wrap(errors.New("no client found for id "+input.ClientId), status.NotFound)
		}
		if err != nil {
			sctx.LogHttpError().Stack().Err(err).Msg("get client by id error for: " + input.ClientId)
			return status.Internal
		}
		if client.TenantID != input.TenantId {
			return status.Wrap(errors.New("client does not belong to your tenant"), status.InvalidArgument)
		}

		if input.ClientId != "" {
			client.ID = input.ClientId
		}
		if input.Name != "" {
			client.Name = input.Name
		}
		if input.TenantId != "" {
			client.TenantID = input.TenantId
		}
		if input.Kid != "" {
			client.Kid = input.Kid
		}
		if input.Env != "" {
			client.Env = input.Env
		}

		shouldDeleteTheOldKey := false
		var oldKeyPairId, publicKey string
		if input.KeyPairId != "" && input.KeyPairId != client.KeyPairId {
			publicKey, err = validateKeyPairId(sctx, input.TenantId, input.KeyPairId)
			if err != nil {
				return status.Wrap(err, status.InvalidArgument)
			}
		}

		if input.KeyPairId != "" && publicKey != "" && input.KeyPairId != client.KeyPairId {
			shouldDeleteTheOldKey = true
			client.PublicKey = publicKey
			oldKeyPairId = client.KeyPairId
			client.KeyPairId = input.KeyPairId
		}

		romeTime := GetCurrentDateTime()
		client.UpdatedAt = romeTime
		*output = *client
		err = StoreClientPDND(sctx, client)
		if err != nil && err.Error() == "invalid data" {
			return status.InvalidArgument
		}
		if err != nil {
			return err
		}
		if shouldDeleteTheOldKey {
			_ = deleteOldKey(sctx, input.TenantId, oldKeyPairId)
		}
		return nil
	})

	uc.SetTitle("Update client pdnd Configuration")
	uc.SetDescription("Update client pdnd Configuration of specific tenant")
	uc.SetTags("Clients")
	uc.SetExpectedErrors(status.InvalidArgument, status.PermissionDenied)

	return uc
}

func deleteOldKey(sctx *ServerContext, tenantId, keysId string) error {
	//se la chiave esiste, altrimenti ritorni null se nn esiste

	_, err := GetPublicKey(sctx, tenantId, keysId)
	if err != nil {
		return nil
	}

	err = DeleteKeys(sctx, tenantId, keysId)
	if err != nil && (err.Error() == "container not found" || err.Error() == "not found") {

		sctx.LogHttpError().Str("http request ", "teanant/client").Str("result", "error").
			Str("flow", "delete keys").
			Stack().Err(err).
			Msg("not able to delete keys having key pair id: " + keysId + "for tenant: " + tenantId)

		return status.NotFound
	}
	if err != nil {
		sctx.LogHttpError().Str("http request ", "teanant/client").Str("result", "error").
			Str("flow", "delete keys").
			Stack().Err(err).
			Msg("not able to delete keys having key pair id: " + keysId + "for tenant: " + tenantId)
		return status.Internal
	}
	return nil
}

func DeleteClient(sctx *ServerContext) usecase.Interactor {
	servicesSync := sctx.ServicesSync()
	clientsCache := sctx.ClientsCache()

	uc := usecase.NewInteractor(func(ctx context.Context, input ClientIdInput, _ *struct{}) error {
		servicesSync.Lock()
		defer servicesSync.Unlock()

		err := ValidateAdminToken(sctx, input.Token, input.TenantId)
		if err != nil && err.Error() == "empty token" {
			MetricsAdminTokenUnauthenticatedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			sctx.LogHttpWarning().Str("http request DELETE /tenants/"+input.TenantId+"/clients/"+input.ClientId, "").Str("result", "401")
			return status.Wrap(err, status.Unauthenticated)
		}
		if err != nil {
			MetricsAdminTokenPermissionDeniedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			sctx.LogHttpWarning().Str("http request DELETE /tenants/"+input.TenantId+"/clients/"+input.ClientId, "").Str("result", "403")
			return status.Wrap(err, status.PermissionDenied)
		}
		_, err = uuid.FromString(input.ClientId)
		if err != nil {
			return status.Wrap(err, status.InvalidArgument)
		}
		_, err = uuid.FromString(input.TenantId)
		if err != nil {
			return status.Wrap(err, status.InvalidArgument)
		}

		client, err := clientsCache.Get(ctx, input.ClientId)
		if err != nil && err.Error() == "value not found in store" {
			return status.NotFound
		}
		if err != nil {
			sctx.LogHttpError().Stack().Err(err).Msg("get client by id error for: " + input.ClientId)
			return status.Internal
		}
		if client.TenantID != input.TenantId {
			return status.Wrap(errors.New("client does not belong to your tenant"), status.InvalidArgument)
		}

		err = TrashClient(sctx, input.ClientId)
		if err != nil && err.Error() == "invalid data" {
			return status.InvalidArgument
		}
		if err != nil {
			return err
		}
		return nil
	})

	uc.SetTitle("Delete client pdnd Configuration")
	uc.SetDescription("Delete client pdnd Configuration of specific tenant")
	uc.SetTags("Clients")
	uc.SetExpectedErrors(status.InvalidArgument, status.PermissionDenied)

	return uc
}

func OptionsClientsById(sctx *ServerContext) usecase.Interactor {
	clientsCache := sctx.ClientsCache()
	uc := usecase.NewInteractor(func(ctx context.Context, input ClientIdInput, _ *struct{}) error {

		err := ValidateAdminToken(sctx, input.Token, input.TenantId)
		if err != nil && err.Error() == "empty token" {
			MetricsAdminTokenUnauthenticatedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			sctx.LogHttpWarning().Str("http request OPTION /tenants/"+input.TenantId+"/clients/"+input.ClientId, "").Str("result", "401")
			return status.Wrap(err, status.Unauthenticated)
		}
		if err != nil {
			MetricsAdminTokenPermissionDeniedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			sctx.LogHttpWarning().Str("http request OPTION /tenants/"+input.TenantId+"/clients/"+input.ClientId, "").Str("result", "403")
			return status.Wrap(err, status.PermissionDenied)
		}

		err = ValidateTenantId(sctx, input.TenantId)
		if err != nil {
			return status.Wrap(err, status.InvalidArgument)
		}

		_, err = uuid.FromString(input.ClientId)
		if err != nil {
			return status.Wrap(err, status.InvalidArgument)
		}
		client, err := clientsCache.Get(ctx, input.ClientId)
		if err != nil && err.Error() == "value not found in store" {
			return status.NotFound
		}
		if err != nil {
			sctx.LogHttpError().Stack().Err(err).Msg("get client by id error for: " + input.ClientId)
			return status.Internal
		}
		if client.TenantID != input.TenantId {
			return status.Wrap(errors.New("client does not belong to your tenant"), status.InvalidArgument)
		}

		return nil
	})

	// Set the title, description, and tags for the OPTIONS operation
	uc.SetTitle("Options Clients")
	uc.SetDescription("Get available operations for the Clients resource.")
	uc.SetTags("Clients")

	// Set the Allow header to indicate the supported HTTP methods
	//uc.SetHeader("Allow", "GET, POST, PATCH, PUT, DELETE")
	return uc
}

func OptionsClients(sctx *ServerContext) usecase.Interactor {
	// Create a new Interactor to handle OPTIONS requests
	uc := usecase.NewInteractor(func(ctx context.Context, input GetClientsByTenantIdInput, _ *struct{}) error {

		err := ValidateAdminToken(sctx, input.Token, input.TenantId)
		if err != nil && err.Error() == "empty token" {
			MetricsAdminTokenUnauthenticatedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			sctx.LogHttpWarning().Str("http request OPTION /tenants/"+input.TenantId+"/clients", "").Str("result", "401")
			return status.Wrap(err, status.Unauthenticated)
		}
		if err != nil {
			MetricsAdminTokenPermissionDeniedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			sctx.LogHttpWarning().Str("http request OPTION /tenants/"+input.TenantId+"/clients", "").Str("result", "403")
			return status.Wrap(err, status.PermissionDenied)
		}

		err = ValidateTenantId(sctx, input.TenantId)
		if err != nil {
			return status.Wrap(err, status.InvalidArgument)
		}
		return nil
	})

	// Set the title, description, and tags for the OPTIONS operation
	uc.SetTitle("Options Clients")
	uc.SetDescription("Get available operations for the Clients resource.")
	uc.SetTags("Clients")

	// Set the Allow header to indicate the supported HTTP methods
	//uc.SetHeader("Allow", "GET, POST, PATCH, PUT, DELETE")
	return uc
}

func validateClientPostPDND(sctx *ServerContext, clientPDND ClientPostInput) error {
	err := ValidateTenantId(sctx, clientPDND.TenantId)
	if err != nil {
		return err
	}
	_, err = uuid.FromString(clientPDND.ID)
	if err != nil {
		return errors.New("client id empty or is an invalid uuid")
	}
	if clientPDND.Name == "" {
		return errors.New("missing Name")
	}
	if clientPDND.Kid == "" {
		return errors.New("KID can't be empty")
	}
	if clientPDND.Env != "collaudo" && clientPDND.Env != "produzione" {
		return errors.New("invalid env: it can be \"collaudo\" or \"produzione\"")
	}
	_, err = uuid.FromString(clientPDND.KeyPairId)
	if err != nil {
		return errors.New("KeyPairId id empty or is an invalid uuid")
	}
	return nil
}

func validateKeyPairId(sctx *ServerContext, tenantId, keyId string) (string, error) {

	publicKeyByte, err := GetPublicKey(sctx, tenantId, keyId)
	if err != nil {
		return string(publicKeyByte), errors.New("key_pair_id does not belong to any key pair")
	}
	result, err := IsKeyPairIdUsedByAClientPdnd(sctx, keyId)
	if err != nil {
		return string(publicKeyByte), err
	}
	if result {
		return string(publicKeyByte), errors.New("key_pair_id already used")
	}

	return string(publicKeyByte), nil
}

func validateClientUpdatePDND(sctx *ServerContext, clientPDND ClientUpdateInput) error {
	err := ValidateTenantId(sctx, clientPDND.TenantId)
	if err != nil {
		return err
	}
	_, err = uuid.FromString(clientPDND.ClientId)
	if err != nil {
		return errors.New("client id empty or is an invalid uuid")
	}
	if clientPDND.Name == "" {
		return errors.New("missing Name")
	}
	if clientPDND.Kid == "" {
		return errors.New("KID can't be empty")
	}
	if clientPDND.Env != "collaudo" && clientPDND.Env != "produzione" {
		return errors.New("invalid env: it can be \"collaudo\" or \"produzione\"")
	}
	_, err = uuid.FromString(clientPDND.KeyPairId)
	if err != nil {
		return errors.New("KeyPairId id empty or is an invalid uuid")
	}
	return nil
}

func validateClientPatchPDND(sctx *ServerContext, clientPDND ClientUpdateInput) error {
	err := ValidateTenantId(sctx, clientPDND.TenantId)
	if err != nil && clientPDND.TenantId != "" {
		return err
	}
	_, err = uuid.FromString(clientPDND.ClientId)
	if err != nil && clientPDND.ClientId != "" {
		return errors.New("client invalid uuid")
	}
	if clientPDND.Env != "collaudo" && clientPDND.Env != "produzione" && clientPDND.Env != "" {
		return errors.New("invalid env: it can be \"collaudo\" or \"produzione\"")
	}
	_, err = uuid.FromString(clientPDND.KeyPairId)
	if err != nil && clientPDND.KeyPairId != "" {
		return errors.New("KeyPairId invalid uuid")
	}
	return nil
}

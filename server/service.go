package server

import (
	"context"
	"errors"
	"net/http"
	"strconv"

	sentryhttp "github.com/getsentry/sentry-go/http"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/swaggest/openapi-go/openapi3"
	"github.com/swaggest/rest"
	"github.com/swaggest/rest/nethttp"
	"github.com/swaggest/rest/response"
	"github.com/swaggest/rest/response/gzip"
	"github.com/swaggest/rest/web"
)

func initService(sctx *ServerContext) *web.Service {
	serverConfig := sctx.ServerConfig()

	service := web.NewService(openapi3.NewReflector())

	service.Wrap(
		gzip.Middleware,
		enhancedErrorHandler(),
		optionsHandler(),
	)

	if serverConfig.SentryEnabled {

		middlewareSentry := sentryhttp.New(sentryhttp.Options{
			Repanic: true,
		})

		service.Use(
			middleware.RealIP,
			middleware.RequestID,
			middleware.CleanPath,
			middleware.StripSlashes,
			middleware.NoCache,
			middleware.RequestLogger(&httpLoggerFormatter{sctx}),
			middleware.Recoverer,
			// middleware.Timeout(60 * time.Second),
			middlewareSentry.Handle,
		)

	} else {

		service.Use(
			middleware.RealIP,
			middleware.RequestID,
			middleware.CleanPath,
			middleware.StripSlashes,
			middleware.NoCache,
			middleware.RequestLogger(&httpLoggerFormatter{sctx}),
			// middleware.Recoverer,
			// middleware.Timeout(60 * time.Second),
		)

	}
	// Custom middleware to set real IP in the context
	service.Use(func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			realIP := r.Header.Get("X-Real-IP")
			if realIP == "" {
				realIP = r.RemoteAddr
			}
			sctx.ctx = context.WithValue(r.Context(), "realIP", realIP)
			next.ServeHTTP(w, r.WithContext(sctx.ctx))
		})
	})
	return service
}

// RFC7807 # https://docs.opencityitalia.it/v/developers/standard-e-convenzioni/standard-della-piattaforma#error-handling
// {
// 	"type": "/errors/incorrect-user-pass",
// 	"title": "Incorrect username or password.",
// 	"status": 401,                                         (optional)
// 	"detail": "Authentication failed due to incorrect username or password.",
// 	"instance": "/login/log/abc123"                        (optional)
// }

type EnhancedErrResponse struct {
	error
	Detail   string `json:"detail" description:"Error detail."`
	Type     string `json:"type" description:"Error type."`
	Status   int    `json:"status" description:"Error status." format:"int32"`
	Title    string `json:"title" description:"Error title."`
	Instance string `json:"instance" description:"Error instance."`
}

func enhancedErrorHandler() func(handler http.Handler) http.Handler {
	return func(handler http.Handler) http.Handler {
		var h *nethttp.Handler

		if nethttp.HandlerAs(handler, &h) {
			h.MakeErrResp = func(ctx context.Context, err error) (int, interface{}) {
				code, errResponse := rest.Err(err)

				var enhancedErrResponse EnhancedErrResponse

				if errors.As(err, &enhancedErrResponse) {
					return http.StatusBadRequest, enhancedErrResponse
				}

				return code, EnhancedErrResponse{
					Detail:   errResponse.ErrorText,
					Type:     "https://httpstatuses.io/" + strconv.Itoa(code),
					Status:   code,
					Title:    errResponse.StatusText,
					Instance: "/",
				}
			}

			h.HandleErrResponse = func(w http.ResponseWriter, r *http.Request, err error) {
				code, errResponse := rest.Err(err)
				responseEncoder := response.Encoder{}

				var enhancedErrResponse EnhancedErrResponse

				if errors.As(err, &enhancedErrResponse) {
					responseEncoder.WriteErrResponse(w, r, http.StatusBadRequest, enhancedErrResponse)
					return
				}

				enhancedErrResponse = EnhancedErrResponse{
					Detail:   errResponse.ErrorText,
					Type:     "https://httpstatuses.io/" + strconv.Itoa(code),
					Status:   code,
					Title:    errResponse.StatusText,
					Instance: r.RequestURI,
				}

				responseEncoder.WriteErrResponse(w, r, code, enhancedErrResponse)
			}
		}

		return handler
	}
}

func optionsHandler() func(handler http.Handler) http.Handler {
	return func(handler http.Handler) http.Handler {
		var h *nethttp.Handler

		if nethttp.HandlerAs(handler, &h) {
			h.HandleErrResponse = func(w http.ResponseWriter, r *http.Request, err error) {
				if r.Method == http.MethodOptions {
					w.Header().Set("Allow", "GET, POST, PATCH, PUT, DELETE")
					w.WriteHeader(http.StatusOK)
				}
			}

		}
		return handler
	}

}

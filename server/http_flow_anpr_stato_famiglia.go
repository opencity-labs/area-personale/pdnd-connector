package server

import (
	"bytes"
	"crypto/sha256"
	"crypto/tls"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"math/rand"
	"net/http"
	"strconv"
	"time"

	"github.com/golang-jwt/jwt/v4"
	"github.com/google/uuid"
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/opencontent/stanza-del-cittadino/pdnd-connector/server/models"
	"gitlab.com/opencontent/stanza-del-cittadino/pdnd-connector/server/models/anpr"
)

type HttpFlowStatoFamiglia struct {
	Name               string
	Sctx               *ServerContext
	Request            AnprRequestInput
	Err                error
	Msg                string
	FlowStatus         bool
	SdcTokenClaims     models.JwtClaims
	Param              anpr.ParametersStatoFamiglia
	Response           anpr.StatoFamigliaFormattedResponse
	AnprResponseStatus int
	VoucherLog         []byte
}

func (r *HttpFlowStatoFamiglia) Exec() bool {
	r.Name = "HttpFlowStatoFamiglia"

	config := r.Sctx.serverConfig
	r.FlowStatus = true &&
		r.validateInputRequestParam() &&
		r.validateToken() &&
		r.loadConfig() &&
		r.getAuditAssertion() &&
		r.getClientAssertion() &&
		r.getVocuher() &&
		r.storeVoucher() &&
		r.getSignature() &&
		r.getStatoFamiglia() &&
		r.buildResponse()

	if r.FlowStatus || r.AnprResponseStatus == http.StatusNotFound {
		MetricsAnprTotalSuccesRequests.WithLabelValues(config.Environment, APPNAME).Inc()
		r.Sctx.LogHttpInfo().Str("request_type", "fruzione e-service").Str("e_service_id", r.Param.EserviceID).Str("e_service", r.Param.EserviceName).Str("e_service_version", r.Param.EserviceVersion).Str("purpose_id", r.Param.PurposeID).Str("client_id ", r.Param.ClientID).Str("config_id ", r.Request.EserviceConfigId).Str("tax_code ", obscureTaxCode(r.Request.FiscalCode)).Str("request_format ", r.Request.Format).Str("user_id ", r.SdcTokenClaims.ID).Str("tenant_id ", r.Param.TenantId).Str("client_ip:", GetClientIp(r.Sctx.ctx)).Str("timestamp", GetUTCtimestamp()).Str("flow", r.Name).Str("response_status", strconv.Itoa(r.AnprResponseStatus)).
			RawJSON("voucher", r.VoucherLog).Str("result", "ok").Msg("")
	} else if r.AnprResponseStatus < 600 && r.AnprResponseStatus >= 500 {
		MetricsErogatoreServerErrorsTotal.WithLabelValues(config.Environment, APPNAME, "ANPR").Inc()
		r.Sctx.LogHttpInfo().Str("request_type", "fruzione e-service").Str("e_service_id", r.Param.EserviceID).Str("e_service", r.Param.EserviceName).Str("e_service_version", r.Param.EserviceVersion).Str("purpose_id", r.Param.PurposeID).Str("client_id ", r.Param.ClientID).Str("config_id ", r.Request.EserviceConfigId).Str("tax_code ", obscureTaxCode(r.Request.FiscalCode)).Str("request_format ", r.Request.Format).Str("user_id ", r.SdcTokenClaims.ID).Str("tenant_id ", r.Param.TenantId).Str("client_ip:", GetClientIp(r.Sctx.ctx)).Str("timestamp", GetUTCtimestamp()).Str("flow", r.Name).Str("response_status", strconv.Itoa(r.AnprResponseStatus)).
			RawJSON("voucher", r.VoucherLog).Str("result", "ok").Msg("")
	} else {
		MetricsAnprTotalErrorRequests.WithLabelValues(config.Environment, APPNAME).Inc()
		r.Sctx.LogHttpError().Str("request_type", "fruzione e-service").Str("e_service_id", r.Param.EserviceID).Str("e_service", r.Param.EserviceName).Str("e_service_version", r.Param.EserviceVersion).Str("purpose_id", r.Param.PurposeID).Str("client_id ", r.Param.ClientID).Str("config_id ", r.Request.EserviceConfigId).Str("tax_code ", obscureTaxCode(r.Request.FiscalCode)).Str("request_format ", r.Request.Format).Str("user_id ", r.SdcTokenClaims.ID).Str("tenant_id ", r.Param.TenantId).Str("client_ip:", GetClientIp(r.Sctx.ctx)).Str("timestamp", GetUTCtimestamp()).Str("flow", r.Name).Str("response_status", strconv.Itoa(r.AnprResponseStatus)).Str("result", "ko").Str("error", r.Err.Error()).Msg("")
	}
	return r.FlowStatus
}
func (r *HttpFlowStatoFamiglia) validateToken() bool {
	if r.Request.Token == "" {
		r.Err = errors.New("missing token")
		r.Msg = "missing token"
		return false
	}

	token := r.Request.Token
	sdcTokenClaims, err := GetTokenClaims(token)
	if err != nil {
		r.Err = err
		r.Msg = "invalid token"
		return false
	}
	if r.Sctx.serverConfig.UserTokenValidationEnabled {
		err = ValidateUserToken(r.Sctx, token, r.Request.FiscalCode)
		if err != nil {
			r.Err = err
			r.Msg = "invalid token"
			return false
		}
	}

	r.SdcTokenClaims = sdcTokenClaims
	return true
}

func (r *HttpFlowStatoFamiglia) loadConfig() bool {
	config, err := GetConfigById(r.Sctx, r.Request.EserviceConfigId)
	if err != nil && err.Error() == "not found" {
		r.Sctx.LogHttpError().Stack().Err(err).Msg("config not found for config id : " + r.Request.EserviceConfigId)
		r.Err = errors.New("config not found")
		r.Msg = "not found"
		return false
	}
	eServiceGeneralConfig, err := GetEserviceGeneralConfig(r.Sctx, config.EserviceID)
	if err != nil && err.Error() == "not found" {
		r.Sctx.LogHttpError().Stack().Err(err).Msg("e-service not found in list for e-service id: " + config.EserviceID)
		r.Err = errors.New("e-service not found in list")
		r.Msg = "not found"
		return false
	}
	client, err := GetClientPdndByClientId(r.Sctx, config.ClientID)
	if err != nil && err.Error() == "not found" {
		r.Sctx.LogHttpError().Stack().Err(err).Msg("client config not found for client id : ")
		r.Err = errors.New("client config not found")
		r.Msg = "not found"
		return false
	}
	privateKey, err := GetPrivateKey(r.Sctx, client.TenantID, client.KeyPairId)
	if err != nil && err.Error() == "not found" {
		r.Sctx.LogHttpError().Stack().Err(err).Msg("privateKey config not found")
		r.Err = errors.New("privateKey not found")
		r.Msg = "not found"
	}
	if err != nil {
		r.Sctx.LogHttpError().Stack().Err(err)
		r.Err = err
		r.Msg = "internal error"
		return false
	}

	tenant, err := GetTenantById(r.Sctx, client.TenantID)
	if err != nil && err.Error() == "not found" {
		r.Sctx.LogHttpError().Stack().Err(err).Msg("tenant not found for config id : " + r.Request.EserviceConfigId)
		r.Err = errors.New("tenant not found")
		r.Msg = "tenant not found for config id : " + r.Request.EserviceConfigId
		return false
	}

	r.Param.ClientPDNDPrivateKey = string(privateKey)
	r.Param.ClientID = client.ID
	r.Param.JTI = uuid.NewString()
	r.Param.KID = client.Kid
	r.Param.PurposeID = config.PurposeID
	r.Param.ServiceAudience = eServiceGeneralConfig.Audience
	r.Param.AudClientAssertion = eServiceGeneralConfig.AudClientAssertion
	r.Param.PayloadData = r.buildPayload()
	r.Param.UserId = uuid.NewString()
	r.Param.UserLocation = tenant.IPaCode
	r.Param.Loa = LOA
	r.Param.Digest64 = r.buildDigest64()
	r.Param.StatoFamigliaEndpoint = eServiceGeneralConfig.Endpoint
	r.Param.EserviceSlug = eServiceGeneralConfig.Slug
	r.Param.EserviceName = eServiceGeneralConfig.Name
	r.Param.EserviceVersion = eServiceGeneralConfig.Version
	r.Param.TenantId = client.TenantID

	return true
}

func (r *HttpFlowStatoFamiglia) validateInputRequestParam() bool {
	err := ValidateAnprRequestInput(r.Request, isValidStatoFamigliaFormat)
	if err != nil {
		r.Err = err
		r.Msg = "bad request"
		return false
	}

	return true
}

func (r *HttpFlowStatoFamiglia) buildPayload() string {
	currentDate := time.Now().Format("2006-01-02")
	idOperazioneClient := r.Param.UserLocation + "_" + currentDate
	return "{\"idOperazioneClient\":\"" + idOperazioneClient + "\",\"criteriRicerca\":{\"codiceFiscale\":\"" + r.Request.FiscalCode + "\"},\"datiRichiesta\":{\"dataRiferimentoRichiesta\":\"" + currentDate + "\",\"motivoRichiesta\":\"" + idOperazioneClient + "\",\"casoUso\":\"C021\"}}"
}
func (r *HttpFlowStatoFamiglia) buildDigest64() string {
	digest32Bytes := sha256.Sum256([]byte(r.Param.PayloadData))
	digestBytes := digest32Bytes[:]
	digest64 := base64.StdEncoding.EncodeToString(digestBytes)
	return digest64
}
func (r *HttpFlowStatoFamiglia) getAuditAssertion() bool {
	trackToken := jwt.NewWithClaims(jwt.SigningMethodRS256, jwt.MapClaims{
		"iat":          jwt.NewNumericDate(time.Now()),
		"exp":          jwt.NewNumericDate(time.Now().Add(time.Minute * 60)),
		"nbf":          jwt.NewNumericDate(time.Now()),
		"aud":          r.Param.ServiceAudience,
		"purposeId":    r.Param.PurposeID,
		"iss":          r.Param.ClientID,
		"sub":          r.Param.ClientID,
		"jti":          r.Param.JTI,
		"dnonce":       rand.Int63n(9999999999999-1000000000000) + 1000000000000,
		"userID":       r.Param.UserId,
		"userLocation": r.Param.UserLocation,
		"LoA":          r.Param.Loa,
	})

	trackToken.Header["kid"] = r.Param.KID
	signKey, err := jwt.ParseRSAPrivateKeyFromPEM([]byte(r.Param.ClientPDNDPrivateKey))
	if err != nil {
		r.Err = err
		r.Msg = "unable to parse private pdnd client key"
		return false
	}
	tokenString, err := trackToken.SignedString(signKey)
	if err != nil {
		r.Err = err
		r.Msg = "file to sign audit assertion"
		return false
	}
	r.Param.AuditAssertion = tokenString

	return true
}
func (r *HttpFlowStatoFamiglia) getClientAssertion() bool {

	digestAud := sha256.Sum256([]byte(r.Param.AuditAssertion))

	issuedAt := time.Now()
	expireAt := issuedAt.Add(time.Hour * 24)

	clientToken := jwt.NewWithClaims(jwt.SigningMethodRS256, jwt.MapClaims{
		"iss":       r.Param.ClientID,
		"aud":       r.Param.AudClientAssertion,
		"jti":       r.Param.JTI,
		"iat":       issuedAt.Unix(),
		"nbf":       issuedAt.Unix(),
		"exp":       expireAt.Unix(),
		"purposeId": r.Param.PurposeID,
		"sub":       r.Param.ClientID,
		"digest": map[string]interface{}{
			"alg":   "SHA256",
			"value": fmt.Sprintf("%x", digestAud),
		},
	})

	clientToken.Header["kid"] = r.Param.KID
	signKey, err := jwt.ParseRSAPrivateKeyFromPEM([]byte(r.Param.ClientPDNDPrivateKey))
	if err != nil {
		r.Err = err
		r.Msg = "unable to parse private pdnd client key"
		return false
	}
	tokenString, err := clientToken.SignedString(signKey)
	if err != nil {
		r.Err = err
		r.Msg = "file to sign audit assertion"
		return false
	}
	r.Param.ClientAssertion = tokenString

	return true
}
func (r *HttpFlowStatoFamiglia) getSignature() bool {
	signToken := jwt.NewWithClaims(jwt.SigningMethodRS256, jwt.MapClaims{
		"aud":       r.Param.ServiceAudience,
		"iat":       jwt.NewNumericDate(time.Now()),
		"nbf":       jwt.NewNumericDate(time.Now()),
		"exp":       jwt.NewNumericDate(time.Now().Add(time.Minute * 60)),
		"purposeId": r.Param.PurposeID,
		"iss":       r.Param.ClientID,
		"sub":       r.Param.ClientID,
		"jti":       r.Param.JTI,
		"signed_headers": []map[string]string{
			{"Digest": "SHA-256=" + r.Param.Digest64},
			{"Content-Type": "application/json"},
			{"Content-Encoding": "UTF-8"},
		},
	})

	signToken.Header["kid"] = r.Param.KID

	signKey, err := jwt.ParseRSAPrivateKeyFromPEM([]byte(r.Param.ClientPDNDPrivateKey))
	if err != nil {
		r.Err = err
		r.Msg = "unable to parse private pdnd client key"
		return false
	}
	tokenString, err := signToken.SignedString(signKey)
	if err != nil {
		r.Err = err
		r.Msg = "unable to parse private pdnd client key"
		return false
	}
	r.Param.Signature = tokenString
	return true
}

func (r *HttpFlowStatoFamiglia) getVocuher() bool {
	voucher, msg, err := GetVocuher(r.Sctx.serverConfig, r.Param.ClientID, r.Param.ClientAssertion)
	if err != nil {
		r.Err = err
		r.Msg = msg
		return false
	}
	r.Param.Voucher = voucher
	return true
}
func (r *HttpFlowStatoFamiglia) storeVoucher() bool {
	param := r.Param
	voucherStrcut, msg, err := DecodeVoucherJWT(r.Param.Voucher.AccessToken)
	if err != nil {
		r.Err = err
		r.Msg = msg
		return false
	}

	voucherLog, err := BuildVoucherLog(*voucherStrcut, param.Voucher.TokenType, param.TenantId, r.SdcTokenClaims.ID, r.Request.EserviceConfigId, param.EserviceVersion, param.EserviceSlug)
	if err != nil {
		r.Err = err
		r.Msg = msg
		return false
	}
	logPath, err := StoreVoucher(r.Sctx, &voucherLog)
	if err != nil {
		r.Err = errors.New("error storing voucher for accertamentoResidenza: " + err.Error())
		r.Msg = "internal error"
		return false
	}

	voucherField := models.VoucherField{
		ID:          voucherLog.ID,
		Path:        logPath,
		StorageType: r.Sctx.serverConfig.StorageType,
	}
	voucherFieldJSON, err := json.Marshal(voucherField)
	if err != nil {
		r.Err = errors.New("error marshalling voucher for accertamentoResidenza: " + err.Error())
		r.Msg = "internal error"
		return false
	}
	r.VoucherLog = voucherFieldJSON
	return true
}

func (r *HttpFlowStatoFamiglia) getStatoFamiglia() bool {

	config := r.Sctx.serverConfig
	timer := prometheus.NewTimer(MetricsAnprLatency.WithLabelValues(config.Environment, APPNAME, r.Param.StatoFamigliaEndpoint))
	req, err := http.NewRequest("POST", r.Param.StatoFamigliaEndpoint, bytes.NewBuffer([]byte(r.Param.PayloadData)))
	if err != nil {
		r.Err = err
		r.Msg = "error creating statoFamiglia request"
		return false
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Content-Encoding", "UTF-8")
	req.Header.Set("Authorization", "Bearer "+r.Param.Voucher.AccessToken)
	req.Header.Set("Agid-JWT-TrackingEvidence", r.Param.AuditAssertion)
	req.Header.Set("Digest", "SHA-256="+r.Param.Digest64)
	req.Header.Set("Agid-JWT-Signature", r.Param.Signature)
	req.Header.Set("Accept", "*/*")

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: true,
		},
		Proxy: http.ProxyFromEnvironment,
	}

	client := &http.Client{Transport: tr}

	resp, err := client.Do(req)

	if err != nil {
		r.Err = errors.New("error performing stato famiglia request: " + err.Error())
		r.Msg = "internal error"
		return false
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		r.Err = errors.New("error reading stato famiglia request: " + err.Error())
		r.Msg = "internal error"
		return false
	}
	r.AnprResponseStatus = resp.StatusCode
	switch {
	case resp.StatusCode == http.StatusNotFound:
		r.Err = errors.New("Not Found: " + string(body))
		r.Msg = "not found"
		return false

	case resp.StatusCode >= 400 && resp.StatusCode < 500:
		r.Err = errors.New("bad request: " + string(body))
		r.Msg = "bad request"
		return false

	case resp.StatusCode >= 500:
		r.Err = errors.New("server error treated as not found: " + string(body))
		r.Msg = "not found"
		return false

	case resp.StatusCode != http.StatusOK:
		r.Err = errors.New("bad response get accertamento-reidenza: " + string(body))
		r.Msg = "internal error"
		return false
	}
	err = json.Unmarshal(body, &r.Param.StatoFamigliaResponse)
	if err != nil {
		r.Err = err
		r.Msg = "error unmarshalling response for statoFamiglia request"
		return false
	}
	timer.ObserveDuration()
	return true
}

func (r *HttpFlowStatoFamiglia) buildResponse() bool {
	StatoFamigliaDataBuilder := StatoFamigliaDataBuilder{r.Param.StatoFamigliaResponse, r.Request.FiscalCode}
	var err error

	r.Response.Data, err = StatoFamigliaDataBuilder.GetFormatedData(r.Request.Format)
	if err != nil && err.Error() == "degree of kinship to find children unsupported" {
		r.Err = errors.New("degree of kinship to find children unsupported")
		r.Msg = "not found"
		return false
	}
	if err != nil {
		r.Err = errors.New("format not implemented")
		r.Msg = "bad request"
		return false
	}

	JsonData, err := json.Marshal(r.Response.Data)
	if err != nil {
		r.Err = errors.New("error marshalling response jsonData: " + err.Error())
		r.Msg = "internal error"
		return false
	}
	callUrl := r.buildCallUrl()
	meta, err := BuildMetaResponse(callUrl, ANPR, string(JsonData), r.Request.Format, r.Param.ClientPDNDPrivateKey)
	if err != nil {
		r.Err = errors.New("error building meta: " + err.Error())
		r.Msg = "internal error"
		return false
	}
	r.Response.Meta = meta
	return true
}

func (r *HttpFlowStatoFamiglia) buildCallUrl() string {
	callUrl := r.Sctx.serverConfig.HttpExternalBasePath + "/" + r.Param.EserviceSlug + "?config_id=" + r.Request.EserviceConfigId + "&fiscal_code=" + r.Request.FiscalCode + "&format=" + r.Request.Format
	return callUrl
}

package server

import (
	"errors"
	"strings"

	"github.com/gofrs/uuid"
	"gitlab.com/opencontent/stanza-del-cittadino/pdnd-connector/server/models/anpr"
)

func checkValidateAccertamentoResidenzaInput(input ValidateAccertamentoResidenzaInput) error {
	var basicParam AnprRequestInput
	basicParam.EserviceConfigId = input.ConfigId
	basicParam.FiscalCode = input.FiscalCode
	basicParam.Format = input.Format

	err := ValidateAnprRequestInput(basicParam, isValidAccertamentoResidenzaFormat)
	if err != nil {

		return err
	}

	err = ValidateMetaPayload(input.Meta)
	if err != nil {
		return err
	}

	return nil
}

func checkValidateStatoFamigliaInput(input ValidateStatoFamigliaInput) error {
	var basicParam AnprRequestInput
	basicParam.EserviceConfigId = input.ConfigId
	basicParam.FiscalCode = input.FiscalCode
	basicParam.Format = input.Format

	err := ValidateAnprRequestInput(basicParam, isValidStatoFamigliaFormat)
	if err != nil {
		return err
	}
	/* 	err = validateStatoFamigliaDataFormatPayload(input.Format, input.Data)
	   	if err != nil {
	   		return err
	   	} */
	err = ValidateMetaPayload(input.Meta)
	if err != nil {
		return err
	}

	return nil
}

func checkValidateAccertamentoCittadinanzaInput(input ValidateAccertamentoCittadinanzaInput) error {
	var basicParam AnprRequestInput
	basicParam.EserviceConfigId = input.ConfigId
	basicParam.FiscalCode = input.FiscalCode
	basicParam.Format = input.Format

	err := ValidateAnprRequestInput(basicParam, isValidAccertamentoCittadinanzaFormat)
	if err != nil {

		return err
	}

	err = ValidateMetaPayload(input.Meta)
	if err != nil {
		return err
	}

	return nil
}

type formatCheck func(string) bool

func isValidAccertamentoResidenzaFormat(inputFormat string) bool {
	for _, format := range AccertamentoResidenzaFormats {
		if inputFormat == format {
			return true
		}
	}
	return false
}
func isValidStatoFamigliaFormat(inputFormat string) bool {
	for _, format := range StatoFamigliaFormats {
		if inputFormat == format {
			return true
		}
	}
	return false
}
func isValidAccertamentoCittadinanzaFormat(inputFormat string) bool {
	for _, format := range AccertamentoCittadinanzaFormats {
		if inputFormat == format {
			return true
		}
	}
	return false
}

func ValidateMetaPayload(meta anpr.Meta) error {
	var err error

	if meta.Signature == nil || *meta.Signature == "" {
		return errors.New("signature is required and cannot be null")
	}

	if meta.Format == nil || *meta.Format == "" {
		return errors.New("format is required and cannot be null")
	}

	if meta.CreatedAt == nil || *meta.CreatedAt == "" {
		return errors.New("created_at is required and cannot be null")
	}

	return err
}
func ValidateAnprRequestInput(a AnprRequestInput, fn formatCheck) error {

	_, err := uuid.FromString(a.EserviceConfigId)
	if err != nil {
		return errors.New("config_id " + err.Error())
	}
	if len(a.FiscalCode) != 16 {
		return errors.New("query parameter fiscal_code empty or not valid")
	}
	if !fn(a.Format) {
		return errors.New("query parameter format not valid")
	}
	a.FiscalCode = strings.ToUpper(a.FiscalCode)
	return nil
}

package server

import (
	"bytes"
	"context"
	"crypto/rsa"
	"crypto/x509"
	"encoding/base64"
	"encoding/json"
	"encoding/pem"
	"errors"
	"fmt"
	"io"
	"math/big"
	"net/http"
	"strings"
	"time"

	"github.com/eko/gocache/lib/v4/cache"
	"github.com/golang-jwt/jwt"
	"gitlab.com/opencontent/stanza-del-cittadino/pdnd-connector/server/models"
)

func retrieveSdcJWKSPublicKey(sdcJWKSPublicKeyCache cache.Cache[*models.SdcJWKSPublicKey], ctx context.Context, sdcJwtkEndpoint string) (models.SdcJWKSPublicKey, error) {
	cachedData, err := sdcJWKSPublicKeyCache.Get(ctx, "sdcJWKSPublicKey")
	if err == nil && cachedData != nil {
		return *cachedData, nil
	}

	sdcPublicKeyResponse := models.SdcJWKSPublicKey{}
	apiEndpoint := sdcJwtkEndpoint
	req, err := http.NewRequest("GET", apiEndpoint, nil)
	if err != nil {
		return sdcPublicKeyResponse, err
	}
	client := &http.Client{}

	res, err := client.Do(req)
	if err != nil {
		return sdcPublicKeyResponse, err
	}
	defer res.Body.Close()
	resBody, err := io.ReadAll(res.Body)
	if err != nil {
		return sdcPublicKeyResponse, err
	}
	if res.StatusCode != http.StatusOK {
		return sdcPublicKeyResponse, err
	}
	err = json.Unmarshal(resBody, &sdcPublicKeyResponse)
	if res.StatusCode != http.StatusOK {
		return sdcPublicKeyResponse, err
	}

	err = sdcJWKSPublicKeyCache.Set(ctx, "sdcJWKSPublicKey", &sdcPublicKeyResponse)
	if err != nil {
		return sdcPublicKeyResponse, err
	}

	return sdcPublicKeyResponse, nil
}

func GetTokenClaims(tokenString string) (models.JwtClaims, error) {
	tokenClaims := models.JwtClaims{}
	token, err := extractJwt(tokenString)
	if err != nil {
		return tokenClaims, err
	}
	tokenClaims, err = extractJwtClaims(token)
	if err != nil {
		return tokenClaims, err
	}
	return tokenClaims, nil
}
func extractJwt(tokenString string) (*models.Jwt, error) {

	parts := strings.Split(tokenString, ".")
	if len(parts) != 3 {
		return nil, fmt.Errorf("invalid token format")
	}
	payload, err := base64.RawURLEncoding.DecodeString(parts[1])
	if err != nil {
		return nil, err
	}
	signature, err := base64.RawURLEncoding.DecodeString(parts[2])
	if err != nil {
		return nil, err
	}

	token := models.Jwt{
		Plaintext: []byte(tokenString[0 : len(parts[0])+len(parts[1])+1]),
		Signature: signature,
	}
	d := json.NewDecoder(bytes.NewBuffer(payload))
	d.UseNumber()
	err = d.Decode(&token.Payload)
	if err != nil {
		return nil, err
	}
	return &token, nil
}

/*
takes a models.Jwt.Payload input, and return the parsed claims
*/
func extractJwtClaims(token *models.Jwt) (models.JwtClaims, error) {
	jwtClaims := models.JwtClaims{}
	payloadJSON, err := json.MarshalIndent(token.Payload, "", "  ")
	if err != nil {
		return jwtClaims, err
	}

	err = json.Unmarshal(payloadJSON, &jwtClaims)
	if err != nil {
		return jwtClaims, err
	}
	return jwtClaims, err
}

func ValidateUserClaims(claims models.JwtClaims, fiscalCode string) error {
	if claims.Username != fiscalCode {
		return errors.New("wrong fiscal code")
	}
	exp := time.Unix(int64(claims.Exp), 0)
	if exp.Before(time.Now()) {
		return errors.New("jwt has expired")
	}
	for _, role := range claims.Roles {
		if role == USER_ROLE {
			return nil
		} else {
			return errors.New("wrong role")
		}
	}
	return errors.New("invalid token")
}
func ValidateUserClaimsWithoutFiscalCode(claims models.JwtClaims, tenantId string) error {
	if claims.TenantID != tenantId {
		return errors.New("wrong tenant id")
	}
	exp := time.Unix(int64(claims.Exp), 0)
	if exp.Before(time.Now()) {
		return errors.New("jwt has expired")
	}
	for _, role := range claims.Roles {
		if role == USER_ROLE {
			return nil
		} else {
			return errors.New("wrong role")
		}
	}
	return errors.New("invalid token")
}

func validateAdminClaims(claims models.JwtClaims, tenantid string) error {
	if claims.TenantID != tenantid {
		return errors.New("wrong tenant")
	}
	exp := time.Unix(int64(claims.Exp), 0)
	if exp.Before(time.Now()) {
		return errors.New("jwt has expired")
	}

	for _, role := range claims.Roles {
		if role == ADMIN_ROLE {
			return nil
		} else {
			return errors.New("wrong role")
		}

	}
	return errors.New("invalid token")
}

func validateAdminClaimsWithoutTenantid(claims models.JwtClaims) error {
	exp := time.Unix(int64(claims.Exp), 0)
	if exp.Before(time.Now()) {
		return errors.New("jwt has expired")
	}

	for _, role := range claims.Roles {
		if role == ADMIN_ROLE {
			return nil
		} else {
			return errors.New("wrong role")
		}

	}
	return errors.New("invalid token")
}

func isJwtSignValid(tokenString string, sdcKey models.SdcJWKSPublicKey) (bool, error) {
	rsaPubKey, err := jwtk2RsaKey(sdcKey)
	if err != nil {
		return false, fmt.Errorf("failed to parse RSA public key: %v", err)
	}

	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		// Don't forget to validate the alg is what you expect:
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}

		return rsaPubKey, nil
	})
	if err != nil {
		return false, err
	}

	if !token.Valid {
		return false, fmt.Errorf("invalid token")
	}

	return true, nil
}
func JWTK2PEM(key models.SdcJWKSPublicKey) string {
	// Base64 decode modulus and exponent
	modulus, _ := base64.RawURLEncoding.DecodeString(key.Keys[0].N)
	exponent, _ := base64.RawURLEncoding.DecodeString(key.Keys[0].E)

	// Create a DER structure for RSA public key
	var publicKey rsa.PublicKey
	publicKey.N = new(big.Int).SetBytes(modulus)
	publicKey.E = int(new(big.Int).SetBytes(exponent).Int64())

	// Marshal the public key into PKIX format
	derBytes, _ := x509.MarshalPKIXPublicKey(&publicKey)

	// Create a PEM block for the public key
	pemBlock := &pem.Block{
		Type:  "PUBLIC KEY",
		Bytes: derBytes,
	}

	// Encode the PEM block to string
	pemString := string(pem.EncodeToMemory(pemBlock))

	return pemString
}
func jwtk2RsaKey(resp models.SdcJWKSPublicKey) (*rsa.PublicKey, error) {

	pubPEM := []byte(JWTK2PEM(resp))
	// Parse PEM block
	block, _ := pem.Decode(pubPEM)
	if block == nil || block.Type != "PUBLIC KEY" {
		return nil, errors.New("failed to decode PEM block containing public key")
	}

	// Parse RSA public key
	pub, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		return nil, err
	}
	rsaPublicKey, ok := pub.(*rsa.PublicKey)
	if !ok {
		return nil, err
	}

	return rsaPublicKey, nil
}

func isTokenValid(sctx *ServerContext, tokenString string) (bool, error) {

	sdcJWKSPublicKey, err := retrieveSdcJWKSPublicKey(sctx.SdcJWKSPublicKeyCache(), sctx.ctx, sctx.serverConfig.SdcJWKSPublicKeyEnpoint)
	if err != nil {
		return false, err
	}
	result, err := isJwtSignValid(tokenString, sdcJWKSPublicKey)
	if !result || err != nil {
		return false, err
	}

	return true, nil
}

func ValidateAdminToken(sctx *ServerContext, token, tenantId string) error {
	token = strings.TrimPrefix(token, "Bearer ")
	if token == "" {
		return errors.New("empty token")
	}

	sdcTokenClaims, err := GetTokenClaims(token)
	if err != nil {
		return err
	}
	err = validateAdminClaims(sdcTokenClaims, tenantId)
	if err != nil {
		return err
	}
	_, err = isTokenValid(sctx, token)
	if err != nil {
		return err
	}
	return nil
}

func ValidateAdminTokenWithoutTenantId(sctx *ServerContext, token string) error {
	token = strings.TrimPrefix(token, "Bearer ")
	if token == "" {
		return errors.New("empty token")
	}

	sdcTokenClaims, err := GetTokenClaims(token)
	if err != nil {
		return err
	}
	err = validateAdminClaimsWithoutTenantid(sdcTokenClaims)
	if err != nil {
		return err
	}
	_, err = isTokenValid(sctx, token)
	if err != nil {
		return err
	}
	return nil
}

func ValidateUserToken(sctx *ServerContext, token, fiscalCode string) error {
	token = strings.TrimPrefix(token, "Bearer ")
	if token == "" {
		return errors.New("empty token")
	}

	sdcTokenClaims, err := GetTokenClaims(token)
	if err != nil {
		return err
	}
	err = ValidateUserClaims(sdcTokenClaims, fiscalCode)
	if err != nil {
		return err
	}
	_, err = isTokenValid(sctx, token)
	if err != nil {
		return err
	}
	return nil
}

func ValidateUserTokenWithoutFiscalCode(sctx *ServerContext, token, tenantId string) error {
	token = strings.TrimPrefix(token, "Bearer ")
	if token == "" {
		return errors.New("empty token")
	}

	sdcTokenClaims, err := GetTokenClaims(token)
	if err != nil {
		return err
	}
	err = ValidateUserClaimsWithoutFiscalCode(sdcTokenClaims, tenantId)
	if err != nil {
		return err
	}
	_, err = isTokenValid(sctx, token)
	if err != nil {
		return err
	}
	return nil
}

func ValidateAdminUserToken(sctx *ServerContext, token, tenantId string) error {
	token = strings.TrimPrefix(token, "Bearer ")
	if token == "" {
		return errors.New("empty token")
	}

	sdcTokenClaims, err := GetTokenClaims(token)
	if err != nil {
		return err
	}

	for _, role := range sdcTokenClaims.Roles {
		if role == USER_ROLE {
			return ValidateUserTokenWithoutFiscalCode(sctx, token, tenantId)
		}
		if role == ADMIN_ROLE {
			return ValidateAdminToken(sctx, token, tenantId)
		}

	}
	return errors.New("invalid token")
}

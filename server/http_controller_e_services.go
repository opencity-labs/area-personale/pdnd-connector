package server

import (
	"context"

	"github.com/swaggest/usecase"
	"github.com/swaggest/usecase/status"
	"gitlab.com/opencontent/stanza-del-cittadino/pdnd-connector/server/models"
)

type EServiceRequestInput struct {
	Offset int    `query:"offset" description:" the starting point or the index from which the data should be retrieved" example:"6"`
	Token  string `header:"Authorization" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	Limit  int    `query:"limit" description:"The limit parameter specifies the maximum number of items to be returned in a single page or request"`
	Sort   string `query:"sort" description:"..."`
}

func GetEServicesList(sctx *ServerContext) usecase.Interactor {
	servicesSync := sctx.ServicesSync()
	uc := usecase.NewInteractor(func(ctx context.Context, input EServiceRequestInput, output *models.EServiceListResponse) error {
		servicesSync.RLock()
		defer servicesSync.RUnlock()

		/* 		err := ValidateAdminTokenWithoutTenantId(sctx, input.Token)
		   		if err != nil && err.Error() == "empty token" {
		   			MetricsAdminTokenUnauthenticatedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
		   			sctx.LogHttpWarning().Str("http request GET e-services", "").Str("result", "401")
		   			return status.Wrap(err, status.Unauthenticated)
		   		}
		   		if err != nil {
		   			MetricsAdminTokenPermissionDeniedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
		   			sctx.LogHttpWarning().Str("http request GET e-services", "").Str("result", "403")
		   			return status.Wrap(err, status.PermissionDenied)
		   		} */

		httpFlowEservicesList := &HttpFlowEservicesList{
			Request: input,
			Sctx:    sctx,
		}

		if !httpFlowEservicesList.Exec() {
			if httpFlowEservicesList.Err != nil {
				sctx.LogHttpError().Str("http request ", "/e-services").Str("result", "discarded").
					Str("flow", httpFlowEservicesList.Name).
					Stack().Err(httpFlowEservicesList.Err).
					Msg(httpFlowEservicesList.Msg)
			}
		}
		*output = httpFlowEservicesList.Response
		return nil
	})
	uc.SetTitle("Get e-services")
	uc.SetDescription("Retrieve the list of e-services available through the service")
	uc.SetTags("e-services")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}

type EServiceOptionRequestInput struct {
	Token string `header:"Authorization" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
}

func OptionsEServicesList(sctx *ServerContext) usecase.Interactor {
	// Create a new Interactor to handle OPTIONS requests
	uc := usecase.NewInteractor(func(ctx context.Context, input EServiceOptionRequestInput, _ *struct{}) error {

		err := ValidateAdminTokenWithoutTenantId(sctx, input.Token)
		if err != nil && err.Error() == "empty token" {
			MetricsAdminTokenUnauthenticatedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			sctx.LogHttpWarning().Str("http request OPTION e-services", "").Str("result", "401")
			return status.Wrap(err, status.Unauthenticated)
		}
		if err != nil {
			MetricsAdminTokenPermissionDeniedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			sctx.LogHttpWarning().Str("http request OPTION e-services", "").Str("result", "403")
			return status.Wrap(err, status.PermissionDenied)
		}
		return nil
	})

	// Set the title, description, and tags for the OPTIONS operation
	uc.SetTitle("Options e-services")
	uc.SetDescription("Get available operations for the e-services resource.")
	uc.SetTags("e-services")

	return uc
}

package server

import (
	"encoding/json"
	"errors"
	"io"
	"strings"

	"github.com/graymeta/stow"
	"gitlab.com/opencontent/stanza-del-cittadino/pdnd-connector/server/models"
)

func StoreClientPDND(sctx *ServerContext, client *models.ClientPDND) error {
	serverConfig := sctx.ServerConfig()
	clientsCache := sctx.ClientsCache()

	basePath := serverConfig.StorageBasePath + "/clients/"
	err := StoreClientPDNDByPath(sctx, client, basePath)
	if err != nil {
		return err
	}

	err = clientsCache.Set(sctx.Ctx(), client.ID, client)
	if err != nil {
		return err
	}

	return nil
}

func GetClientsPdndByTenantId(sctx *ServerContext, tenantId string) (*[]models.ClientPDND, error) {
	serverConfig := sctx.ServerConfig()
	fileStorage := sctx.FileStorage()
	basePath := serverConfig.StorageBasePath + "/clients/"
	var clientList []models.ClientPDND
	pageSize := 10
	err := stow.Walk(fileStorage, basePath, pageSize, func(item stow.Item, err error) error {
		if err != nil {
			return err
		}
		// Verify if the item is a JSON file
		if !strings.HasSuffix(item.Name(), ".json") {
			return nil
		}

		itemContent, err := item.Open()
		if err != nil {
			return err
		}

		itemBytes, err := io.ReadAll(itemContent)
		if err != nil {
			return err
		}

		var client models.ClientPDND
		if err := json.Unmarshal(itemBytes, &client); err != nil {
			return err
		}
		if client.TenantID == tenantId {
			clientList = append(clientList, client)
		}

		return nil
	})
	if err != nil {
		return nil, err
	}

	return &clientList, nil
}

func IsKeyPairIdUsedByAClientPdnd(sctx *ServerContext, keyPairId string) (bool, error) {
	serverConfig := sctx.ServerConfig()
	fileStorage := sctx.FileStorage()
	basePath := serverConfig.StorageBasePath + "/clients/"
	result := false
	pageSize := 10
	err := stow.Walk(fileStorage, basePath, pageSize, func(item stow.Item, err error) error {
		if err != nil {
			return err
		}
		// Verify if the item is a JSON file
		if !strings.HasSuffix(item.Name(), ".json") {
			return nil
		}

		itemContent, err := item.Open()
		if err != nil {
			return err
		}

		itemBytes, err := io.ReadAll(itemContent)
		if err != nil {
			return err
		}

		var client models.ClientPDND
		if err := json.Unmarshal(itemBytes, &client); err != nil {
			return err
		}
		if client.KeyPairId == keyPairId {
			result = true
			return nil
		}

		return nil
	})
	if err != nil {
		return result, err
	}

	return result, nil
}

func GetClientPdndByClientId(sctx *ServerContext, clientId string) (*models.ClientPDND, error) {
	serverConfig := sctx.ServerConfig()
	fileStorage := sctx.FileStorage()
	basePath := serverConfig.StorageBasePath + "/clients/"

	var clientPdnd *models.ClientPDND

	itemPath := basePath + clientId + ".json"

	item, err := fileStorage.Item(itemPath)
	if err != nil {
		return clientPdnd, err
	}

	itemContent, err := item.Open()
	if err != nil {
		return clientPdnd, err
	}
	defer itemContent.Close()

	itemBytes, err := io.ReadAll(itemContent)
	if err != nil {
		return clientPdnd, err
	}

	if err := json.Unmarshal(itemBytes, &clientPdnd); err != nil {
		return clientPdnd, err
	}

	return clientPdnd, nil
}

func DeleteClientPDND(sctx *ServerContext, clientId string) error {
	serverConfig := sctx.ServerConfig()
	fileStorage := sctx.FileStorage()
	clientsCache := sctx.ClientsCache()

	basePath := serverConfig.StorageBasePath + "/clients/"

	itemPath := basePath + clientId + ".json"

	item, err := fileStorage.Item(itemPath)
	if err != nil {
		return err
	}

	err = fileStorage.RemoveItem(item.ID())
	if err != nil {
		return err
	}
	err = clientsCache.Delete(sctx.Ctx(), clientId)
	if err != nil {
		return err
	}

	return nil
}

func TrashClient(sctx *ServerContext, clientId string) error {
	serverConfig := sctx.ServerConfig()
	clientToDelete, err := GetClientPdndByClientId(sctx, clientId)
	if err != nil {
		return err
	}

	basePath := serverConfig.StorageBasePath + "/trash/" + "/clients/"
	err = StoreClientPDNDByPath(sctx, clientToDelete, basePath)
	if err != nil {
		return err
	}
	err = DeleteClientPDND(sctx, clientId)
	if err != nil {
		return err
	}

	err = TrashConfigsClient(sctx, clientId)
	if err != nil {
		return err
	}
	return nil
}

func StoreClientPDNDByPath(sctx *ServerContext, client *models.ClientPDND, basePath string) error {
	fileStorage := sctx.FileStorage()

	itemBytes, err := json.Marshal(client)
	if err != nil {
		return errors.New("invalid data")
	}

	itemName := basePath + client.ID + ".json"
	itemContent := string(itemBytes)
	itemSize := int64(len(itemContent))

	itemReader := strings.NewReader(itemContent)

	_, err = fileStorage.Put(itemName, itemReader, itemSize, nil)
	if err != nil {
		return err
	}
	return nil
}

func TrashClientsTenants(sctx *ServerContext, tenantId string) error {
	serverConfig := sctx.ServerConfig()
	fileStorage := sctx.FileStorage()
	basePath := serverConfig.StorageBasePath + "/clients/"
	pageSize := 10
	err := stow.Walk(fileStorage, basePath, pageSize, func(item stow.Item, err error) error {
		if err != nil {
			return err
		}
		if !strings.HasSuffix(item.Name(), ".json") {
			return nil
		}

		itemContent, err := item.Open()
		if err != nil {
			return err
		}

		itemBytes, err := io.ReadAll(itemContent)
		if err != nil {
			return err
		}

		var client models.ClientPDND
		if err := json.Unmarshal(itemBytes, &client); err != nil {
			return err
		}
		if client.TenantID == tenantId {
			err = TrashClient(sctx, client.ID)
			if err != nil {
				return err
			}
		}

		return nil
	})
	if err != nil {
		return err
	}

	return nil
}

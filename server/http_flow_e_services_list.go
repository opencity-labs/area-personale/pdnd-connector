package server

import (
	"fmt"

	"gitlab.com/opencontent/stanza-del-cittadino/pdnd-connector/server/models"
)

type HttpFlowEservicesList struct {
	Name       string
	Sctx       *ServerContext
	Request    EServiceRequestInput
	Err        error
	Msg        string
	FlowStatus bool
	Response   models.EServiceListResponse
}

func (r *HttpFlowEservicesList) Exec() bool {
	r.FlowStatus = true
	return r.FlowStatus &&
		r.buildPaginatedEserviceList()
}

func (r *HttpFlowEservicesList) buildPaginatedEserviceList() bool {

	limit := r.Request.Limit
	offset := r.Request.Offset
	sort := r.Request.Sort
	eserviceList := r.Sctx.eServiceList
	totalEservicesListSize := len(eserviceList.Data)

	if limit == 0 {
		limit = 10
	}
	if limit < 0 {
		limit = 0
	}
	if offset < 0 {
		offset = 0
	}
	if limit > totalEservicesListSize {
		limit = totalEservicesListSize
	}
	if offset > totalEservicesListSize {
		offset = totalEservicesListSize
	}

	var selfPage, nextPage, prevPage string
	serverConfig := r.Sctx.serverConfig
	endpoint := serverConfig.HttpExternalBasePath
	sortPath := ""
	if sort != "" {
		sortPath = "&sort="
	}

	selfPage = fmt.Sprintf("/e-services?offset=%d&limit=%d"+sortPath+"%s", offset, limit, sort)
	if totalEservicesListSize >= limit && offset < totalEservicesListSize {
		nextOffset := offset + limit
		if nextOffset >= totalEservicesListSize {
			nextPage = ""
		} else {
			nextPage = fmt.Sprintf("/e-services?offset=%d&limit=%d"+sortPath+"%s", nextOffset, limit, sort)
		}
	}
	if offset > 0 {
		prevOffset := offset - limit
		if prevOffset < 0 {
			prevOffset = 0
		}
		prevPage = fmt.Sprintf("/e-services?offset=%d&limit=%d"+sortPath+"%s", prevOffset, limit, sort)
	}

	if nextPage != "" {
		tmp := endpoint + nextPage
		r.Response.Links.Next = &tmp
	}
	if prevPage != "" {
		tmp := endpoint + prevPage
		r.Response.Links.Prev = &tmp
	}
	r.Response.Links.Self = endpoint + selfPage

	r.Response.Data = r.getPaginatedList(offset, limit, eserviceList.Data)
	r.Response.Meta.Total = totalEservicesListSize
	r.Response.Meta.Page.Limit = limit
	r.Response.Meta.Page.Offset = offset
	if sort != "" {
		r.Response.Meta.Page.Sort = &sort
	}

	return true
}

func (r *HttpFlowEservicesList) getPaginatedList(offset, limit int, eserviceArray []models.EService) []models.EService {
	startIndex := offset
	endIndex := offset + limit
	if startIndex < 0 {
		startIndex = 0
	}
	if endIndex < 0 {
		endIndex = 0
	}
	if endIndex > len(eserviceArray) {
		endIndex = len(eserviceArray)
	}
	if startIndex > endIndex {
		startIndex = 0
	}
	/* 	sort.SliceStable(eserviceArray, func(i, j int) bool {
		return eserviceArray[i].CreatedAt < eserviceArray[j].CreatedAt
	}) */

	return eserviceArray[startIndex:endIndex]
}

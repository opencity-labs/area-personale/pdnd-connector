package server

import (
	"net/http"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/swaggest/openapi-go/openapi3"
	"github.com/swaggest/rest/nethttp"
	"github.com/swaggest/rest/web"
	"github.com/swaggest/swgui/v5emb"
)

func registerRoutes(sctx *ServerContext, service *web.Service) {
	setInfos(sctx, service)
	service.OpenAPISchema().SetTitle("PDND Connector API")
	service.OpenAPISchema().SetDescription("configures and uses PDND e-services")
	service.OpenAPISchema().SetVersion(VERSION)

	service.Get("/anpr/accertamento-residenza", GetAccertamentoResidenza(sctx), nethttp.SuccessStatus(http.StatusOK))
	service.Post("/anpr/accertamento-residenza", ValidateAccertamentoResidenza(sctx), nethttp.SuccessStatus(http.StatusOK))
	service.Get("/anpr/stato-famiglia", GetStatoFamiglia(sctx), nethttp.SuccessStatus(http.StatusOK))
	service.Post("/anpr/stato-famiglia", ValidateStatoFamiglia(sctx), nethttp.SuccessStatus(http.StatusOK))
	service.Get("/anpr/accertamento-cittadinanza", GetAccertamentoCittadinanza(sctx), nethttp.SuccessStatus(http.StatusOK))
	service.Post("/anpr/accertamento-cittadinanza", ValidateAccertamentoCittadinanza(sctx), nethttp.SuccessStatus(http.StatusOK))

	service.Get("/e-services", GetEServicesList(sctx), nethttp.SuccessStatus(http.StatusOK))
	service.Options("/e-services", OptionsEServicesList(sctx), nethttp.SuccessStatus(http.StatusOK))

	service.Post("/tenants/{tenant_id}/configs", CreateConfig(sctx), nethttp.SuccessStatus(http.StatusCreated))
	service.Get("/tenants/{tenant_id}/configs/{config_id}", GetconfigByID(sctx), nethttp.SuccessStatus(http.StatusOK))
	service.Get("/tenants/{tenant_id}/configs", GetConfigsListByTenantId(sctx), nethttp.SuccessStatus(http.StatusOK))
	service.Put("/tenants/{tenant_id}/configs/{config_id}", UpdateConfig(sctx), nethttp.SuccessStatus(http.StatusOK))
	service.Patch("/tenants/{tenant_id}/configs/{config_id}", PatchConfig(sctx), nethttp.SuccessStatus(http.StatusOK))
	service.Delete("/tenants/{tenant_id}/configs/{config_id}", DeleteConfig(sctx), nethttp.SuccessStatus(http.StatusNoContent))
	service.Options("/tenants/{tenant_id}/configs", OptionsConfig(sctx), nethttp.SuccessStatus(http.StatusCreated))
	service.Options("/tenants/{tenant_id}/configs/{config_id}", OptionsConfigById(sctx), nethttp.SuccessStatus(http.StatusOK))

	service.Post("/tenants/{tenant_id}/clients", CreateClient(sctx), nethttp.SuccessStatus(http.StatusCreated))
	service.Get("/tenants/{tenant_id}/clients", GetClientsByTenantId(sctx), nethttp.SuccessStatus(http.StatusOK))
	service.Get("/tenants/{tenant_id}/clients/{client_id}", GetClientByClientId(sctx), nethttp.SuccessStatus(http.StatusOK))
	service.Put("/tenants/{tenant_id}/clients/{client_id}", UpdateClient(sctx), nethttp.SuccessStatus(http.StatusOK))
	service.Patch("/tenants/{tenant_id}/clients/{client_id}", PatchClient(sctx), nethttp.SuccessStatus(http.StatusOK))
	service.Delete("/tenants/{tenant_id}/clients/{client_id}", DeleteClient(sctx), nethttp.SuccessStatus(http.StatusNoContent))
	service.Options("/tenants/{tenant_id}/clients", OptionsClients(sctx), nethttp.SuccessStatus(http.StatusOK))
	service.Options("/tenants/{tenant_id}/clients/{client_id}", OptionsClientsById(sctx), nethttp.SuccessStatus(http.StatusOK))

	service.Get("/tenants/{tenant_id}/keys", GetKeys(sctx), nethttp.SuccessStatus(http.StatusOK))
	service.Delete("/tenants/{tenant_id}/keys/{keys_id}", DeleteKeyById(sctx), nethttp.SuccessStatus(http.StatusNoContent))

	//service.Get("/tenants", GetTenants(sctx), nethttp.SuccessStatus(http.StatusCreated))
	service.Post("/tenants", CreateTenants(sctx), nethttp.SuccessStatus(http.StatusCreated))
	service.Get("/tenants/{tenant_id}", GetTenant(sctx), nethttp.SuccessStatus(http.StatusOK))
	service.Patch("/tenants/{tenant_id}", PatchTenant(sctx), nethttp.SuccessStatus(http.StatusOK))
	service.Put("/tenants/{tenant_id}", UpdateTenant(sctx), nethttp.SuccessStatus(http.StatusOK))
	service.Delete("/tenants/{tenant_id}", DeleteTenant(sctx), nethttp.SuccessStatus(http.StatusNoContent))
	service.Options("/tenants", OptionsTenant(sctx), nethttp.SuccessStatus(http.StatusOK))
	service.Options("/tenants/{tenant_id}", OptionsTenantById(sctx), nethttp.SuccessStatus(http.StatusOK))

	service.Get("/status", GetStatus(sctx))
	service.Method(http.MethodGet, "/metrics", promhttp.Handler())
	service.Docs("/docs", v5emb.New)
}

func setInfos(sctx *ServerContext, service *web.Service) {
	appName := APPNAME_VERSION

	service.OpenAPISchema().SetTitle("Pdnd Connector API")
	service.OpenAPISchema().SetDescription("Questo servizio si occupa di fare da intermediario tra l'area personale e la pdnd e i suoi erogatori ")
	service.OpenAPISchema().SetVersion(VERSION)

	openapiSchema := service.OpenAPICollector.Reflector().Spec

	contactName := "Support"
	contactUrl := "https://www.opencitylabs.it"
	contactEmail := "support@opencitylabs.it"
	openapiSchema.Info.WithContact(openapi3.Contact{
		Name:  &contactName,
		URL:   &contactUrl,
		Email: &contactEmail,
	})
	openapiSchema.Info.WithTermsOfService("https://opencitylabs.it/")
	openapiSchema.Info.WithMapOfAnything(map[string]interface{}{
		"x-api-id":  appName,
		"x-summary": "Questo servizio si occupa di fare da intermediario tra l'area personale e la pdnd e i suoi erogatori",
	})
	serverDescription := appName
	openapiSchema.WithServers(openapi3.Server{
		Description: &serverDescription,
	})
}

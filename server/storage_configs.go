package server

import (
	"encoding/json"
	"errors"
	"io"
	"strings"

	"github.com/graymeta/stow"
	"gitlab.com/opencontent/stanza-del-cittadino/pdnd-connector/server/models"
)

func StoreConfig(sctx *ServerContext, config *models.Config) error {
	serverConfig := sctx.ServerConfig()
	configsCache := sctx.ConfigsCache()

	basePath := serverConfig.StorageBasePath + "/eservice_configs/"
	err := StoreCofigByPath(sctx, config, basePath)
	if err != nil {
		return err
	}

	err = configsCache.Set(sctx.Ctx(), config.ID, config)
	if err != nil {
		return err
	}

	return nil

}

func GetConfigById(sctx *ServerContext, configId string) (*models.Config, error) {
	serverConfig := sctx.ServerConfig()
	fileStorage := sctx.FileStorage()
	basePath := serverConfig.StorageBasePath + "/eservice_configs/"

	var eserviceConfig *models.Config

	itemPath := basePath + configId + ".json"

	item, err := fileStorage.Item(itemPath)
	if err != nil {
		return eserviceConfig, err
	}

	itemContent, err := item.Open()
	if err != nil {
		return eserviceConfig, err
	}
	defer itemContent.Close()

	itemBytes, err := io.ReadAll(itemContent)
	if err != nil {
		return eserviceConfig, err
	}

	if err := json.Unmarshal(itemBytes, &eserviceConfig); err != nil {
		return eserviceConfig, err
	}

	return eserviceConfig, nil
}

func GetConfigsByTenantId(sctx *ServerContext, teantId string) (*[]models.Config, error) {
	serverConfig := sctx.ServerConfig()
	fileStorage := sctx.FileStorage()
	basePath := serverConfig.StorageBasePath + "/eservice_configs/"
	var configsList []models.Config
	pageSize := 10
	err := stow.Walk(fileStorage, basePath, pageSize, func(item stow.Item, err error) error {
		if err != nil {
			return err
		}
		// Verify if the item is a JSON file
		if !strings.HasSuffix(item.Name(), ".json") {
			return nil
		}

		itemContent, err := item.Open()
		if err != nil {
			return err
		}

		itemBytes, err := io.ReadAll(itemContent)
		if err != nil {
			return err
		}

		var config models.Config
		if err := json.Unmarshal(itemBytes, &config); err != nil {
			return err
		}

		if config.TenantID == teantId {
			configsList = append(configsList, config)
		}

		return nil
	})
	if err != nil {

		return nil, err
	}

	return &configsList, nil
}

func TrashConfig(sctx *ServerContext, configId string) error {
	serverConfig := sctx.ServerConfig()
	configToDelete, err := GetConfigById(sctx, configId)
	if err != nil {
		return err
	}

	basePath := serverConfig.StorageBasePath + "/trash/" + "/eservice_configs/"
	err = StoreCofigByPath(sctx, configToDelete, basePath)
	if err != nil {
		return err
	}
	err = DeleteConfigiguration(sctx, configId)
	if err != nil {
		return err
	}
	return nil
}

func StoreCofigByPath(sctx *ServerContext, eServiceConfig *models.Config, basePath string) error {
	fileStorage := sctx.FileStorage()

	itemBytes, err := json.Marshal(eServiceConfig)
	if err != nil {
		return errors.New("invalid data")
	}

	itemName := basePath + eServiceConfig.ID + ".json"
	itemContent := string(itemBytes)
	itemSize := int64(len(itemContent))

	itemReader := strings.NewReader(itemContent)

	_, err = fileStorage.Put(itemName, itemReader, itemSize, nil)
	if err != nil {
		return err
	}
	return nil
}

func DeleteConfigiguration(sctx *ServerContext, configId string) error {
	serverConfig := sctx.ServerConfig()
	fileStorage := sctx.FileStorage()
	configsCache := sctx.ConfigsCache()

	basePath := serverConfig.StorageBasePath + "/eservice_configs/"

	itemPath := basePath + configId + ".json"

	item, err := fileStorage.Item(itemPath)
	if err != nil {
		return err
	}

	err = fileStorage.RemoveItem(item.ID())
	if err != nil {
		return err
	}

	err = configsCache.Delete(sctx.Ctx(), configId)
	if err != nil {
		return err
	}

	return nil
}

func TrashConfigsClient(sctx *ServerContext, clientId string) error {
	serverConfig := sctx.ServerConfig()
	fileStorage := sctx.FileStorage()
	basePath := serverConfig.StorageBasePath + "/eservice_configs/"
	pageSize := 10
	err := stow.Walk(fileStorage, basePath, pageSize, func(item stow.Item, err error) error {
		if err != nil {
			return err
		}
		if !strings.HasSuffix(item.Name(), ".json") {
			return nil
		}

		itemContent, err := item.Open()
		if err != nil {
			return err
		}

		itemBytes, err := io.ReadAll(itemContent)
		if err != nil {
			return err
		}

		var config models.Config
		if err := json.Unmarshal(itemBytes, &config); err != nil {
			return err
		}
		if config.ClientID == clientId {
			err = TrashConfig(sctx, config.ID)
			if err != nil {
				return err
			}
		}

		return nil
	})
	if err != nil {
		return err
	}

	return nil
}

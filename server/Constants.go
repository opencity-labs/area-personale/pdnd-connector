package server

const APPNAME_VERSION string = "Pdnd-Connector-" + VERSION
const APPNAME string = "Pdnd-Connector"
const USER_ROLE string = "ROLE_CPS_USER"
const ADMIN_ROLE string = "ROLE_ADMIN"
const LOA string = "SpidL2"
const ANPR string = "ANPR"
const CARD_HOLDER string = "1"
const SPOUSE string = "2"
const CHILD string = "3"
const DESCENDANT_GRANDSON string = "4"
const DESCENDANT_GREAT_GRANDSON = "5"
const PARENT string = "6"
const STEPSON string = "14"
const PARENT_IN_LAW string = "17"

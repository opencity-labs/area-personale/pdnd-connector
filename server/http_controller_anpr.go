package server

import (
	"context"

	"github.com/swaggest/usecase"
	"github.com/swaggest/usecase/status"
	"gitlab.com/opencontent/stanza-del-cittadino/pdnd-connector/server/models/anpr"
)

type AnprRequestInput struct {
	FiscalCode       string `query:"fiscal_code" description:"italian fiscal code" example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	Token            string `header:"Authorization" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	EserviceConfigId string `query:"config_id" description:"id that identify a specific configuration to call an e-service" example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	Format           string `query:"format" description:"identify the output payload structure"`
}

func GetAccertamentoResidenza(sctx *ServerContext) usecase.Interactor {
	servicesSync := sctx.ServicesSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input AnprRequestInput, output *anpr.AccertamentoResidenzaFormattedResponse) error {
		servicesSync.RLock()
		defer servicesSync.RUnlock()

		httpFlowAccertamentoResidenza := &HttpFlowAccertamentoResidenza{
			Sctx:    sctx,
			Request: input,
		}

		httpFlowAccertamentoResidenza.Exec()

		if httpFlowAccertamentoResidenza.Err != nil && httpFlowAccertamentoResidenza.Msg == "internal error" {
			return status.Wrap(httpFlowAccertamentoResidenza.Err, status.Internal)
		}
		if httpFlowAccertamentoResidenza.Err != nil && httpFlowAccertamentoResidenza.Msg == "invalid token" {
			MetricsUserTokenPermissionDeniedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			return status.Wrap(httpFlowAccertamentoResidenza.Err, status.PermissionDenied)
		}
		if httpFlowAccertamentoResidenza.Err != nil && httpFlowAccertamentoResidenza.Msg == "missing token" {
			MetricsUserTokenUnauthenticatedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			return status.Wrap(httpFlowAccertamentoResidenza.Err, status.Unauthenticated)
		}
		if httpFlowAccertamentoResidenza.Err != nil && httpFlowAccertamentoResidenza.Msg == "bad request" {
			return status.Wrap(httpFlowAccertamentoResidenza.Err, status.InvalidArgument)
		}
		if httpFlowAccertamentoResidenza.Err != nil && httpFlowAccertamentoResidenza.Msg == "wrong input paramenter" {
			return status.Wrap(httpFlowAccertamentoResidenza.Err, status.InvalidArgument)
		}
		if httpFlowAccertamentoResidenza.Err != nil && httpFlowAccertamentoResidenza.Msg == "not found" {
			return status.Wrap(httpFlowAccertamentoResidenza.Err, status.NotFound)
		}
		if httpFlowAccertamentoResidenza.Err != nil {
			return status.Wrap(httpFlowAccertamentoResidenza.Err, status.Internal)
		}
		*output = httpFlowAccertamentoResidenza.Response
		return nil
	})

	uc.SetTitle("Accertamento Residenza")
	uc.SetDescription("fruizione e-service Accertamento Residenza")
	uc.SetTags("ANPR")
	uc.SetExpectedErrors(status.NotFound)

	return uc
}

type ValidateAccertamentoResidenzaInput struct {
	FiscalCode string                                  `query:"fiscal_code" description:"italian fiscal code" example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	Token      string                                  `header:"Authorization" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	ConfigId   string                                  `query:"config_id" description:"id that identify a specific configuration to call an e-service" example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	Format     string                                  `query:"format" description:"identify the output payload structure"`
	Data       anpr.AccertamentoResidenzaFormattedData `json:"Data" description:"data to validate. Can be different depending by the format param" example:"..."`
	Meta       anpr.Meta                               `json:"Meta" description:"contains the signed data to validate" example:"..."`
}

func ValidateAccertamentoResidenza(sctx *ServerContext) usecase.Interactor {
	servicesSync := sctx.ServicesSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input ValidateAccertamentoResidenzaInput, output *anpr.ValidateAnpresponse) error {
		servicesSync.RLock()
		defer servicesSync.RUnlock()

		httpFlowValidateAccertamentoResidenza := &HttpFlowValidateAccertamentoResidenza{
			Sctx:    sctx,
			Request: input,
		}

		httpFlowValidateAccertamentoResidenza.Exec()

		if httpFlowValidateAccertamentoResidenza.Err != nil && httpFlowValidateAccertamentoResidenza.Msg == "internal error" {
			return status.Wrap(httpFlowValidateAccertamentoResidenza.Err, status.Internal)
		}
		if httpFlowValidateAccertamentoResidenza.Err != nil && httpFlowValidateAccertamentoResidenza.Msg == "invalid token" {
			MetricsUserTokenPermissionDeniedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			return status.PermissionDenied
		}
		if httpFlowValidateAccertamentoResidenza.Err != nil && httpFlowValidateAccertamentoResidenza.Msg == "missing token" {
			MetricsUserTokenUnauthenticatedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			return status.Wrap(httpFlowValidateAccertamentoResidenza.Err, status.Unauthenticated)
		}

		if httpFlowValidateAccertamentoResidenza.Err != nil && httpFlowValidateAccertamentoResidenza.Msg == "bad request" {
			return status.InvalidArgument
		}
		if httpFlowValidateAccertamentoResidenza.Err != nil {

			return status.Wrap(httpFlowValidateAccertamentoResidenza.Err, status.Internal)

		}
		*output = httpFlowValidateAccertamentoResidenza.Response
		return nil
	})

	uc.SetTitle("Accertamento Residenza")
	uc.SetDescription("Validate Accertamento Residenza payload")
	uc.SetTags("ANPR")
	uc.SetExpectedErrors(status.NotFound)

	return uc
}

func GetStatoFamiglia(sctx *ServerContext) usecase.Interactor {
	servicesSync := sctx.ServicesSync()
	uc := usecase.NewInteractor(func(ctx context.Context, input AnprRequestInput, output *anpr.StatoFamigliaFormattedResponse) error {
		servicesSync.RLock()
		defer servicesSync.RUnlock()

		httpFlowStatusFamiglia := &HttpFlowStatoFamiglia{
			Sctx:    sctx,
			Request: input,
		}
		httpFlowStatusFamiglia.Exec()

		if httpFlowStatusFamiglia.Err != nil && httpFlowStatusFamiglia.Msg == "internal error" {
			return status.Wrap(httpFlowStatusFamiglia.Err, status.Internal)
		}
		if httpFlowStatusFamiglia.Err != nil && httpFlowStatusFamiglia.Msg == "invalid token" {
			MetricsUserTokenPermissionDeniedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			return status.Wrap(httpFlowStatusFamiglia.Err, status.PermissionDenied)
		}
		if httpFlowStatusFamiglia.Err != nil && httpFlowStatusFamiglia.Msg == "missing token" {
			MetricsUserTokenUnauthenticatedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			return status.Wrap(httpFlowStatusFamiglia.Err, status.Unauthenticated)
		}
		if httpFlowStatusFamiglia.Err != nil && httpFlowStatusFamiglia.Msg == "bad request" {
			return status.Wrap(httpFlowStatusFamiglia.Err, status.InvalidArgument)
		}
		if httpFlowStatusFamiglia.Err != nil && httpFlowStatusFamiglia.Msg == "wrong input paramenter" {
			return status.Wrap(httpFlowStatusFamiglia.Err, status.InvalidArgument)
		}
		if httpFlowStatusFamiglia.Err != nil && httpFlowStatusFamiglia.Msg == "not found" {
			return status.Wrap(httpFlowStatusFamiglia.Err, status.NotFound)
		}
		if httpFlowStatusFamiglia.Err != nil {
			return status.Wrap(httpFlowStatusFamiglia.Err, status.Internal)
		}
		*output = httpFlowStatusFamiglia.Response
		return nil
	})
	uc.SetTitle("Stato Famiglia")
	uc.SetDescription("Retrieve Stato Famiglia for a specific fiscal code with specific configuration")
	uc.SetTags("ANPR")
	uc.SetExpectedErrors(status.NotFound)

	return uc
}

type ValidateStatoFamigliaInput struct {
	FiscalCode string      `query:"fiscal_code" description:"italian fiscal code" example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	Token      string      `header:"Authorization" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	ConfigId   string      `query:"config_id" description:"id that identify a specific configuration to call an e-service" example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	Format     string      `query:"format" description:"identify the output payload structure"`
	Data       interface{} `json:"Data" description:"data to validate. Can be different depending by the format param" example:"..."`
	Meta       anpr.Meta   `json:"Meta" description:"contains the signed data to validate" example:"..."`
}

func ValidateStatoFamiglia(sctx *ServerContext) usecase.Interactor {
	servicesSync := sctx.ServicesSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input ValidateStatoFamigliaInput, output *anpr.ValidateAnpresponse) error {
		servicesSync.RLock()
		defer servicesSync.RUnlock()
		err := checkValidateStatoFamigliaInput(input)
		if err != nil {
			return status.Wrap(err, status.InvalidArgument)
		}
		HttpFlowValidateStatoFamiglia := &HttpFlowValidateStatoFamiglia{
			Sctx:    sctx,
			Request: input,
		}

		HttpFlowValidateStatoFamiglia.Exec()

		if HttpFlowValidateStatoFamiglia.Err != nil && HttpFlowValidateStatoFamiglia.Msg == "internal error" {
			return status.Wrap(HttpFlowValidateStatoFamiglia.Err, status.Internal)
		}
		if HttpFlowValidateStatoFamiglia.Err != nil && HttpFlowValidateStatoFamiglia.Msg == "invalid token" {
			MetricsUserTokenPermissionDeniedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			return status.PermissionDenied
		}
		if HttpFlowValidateStatoFamiglia.Err != nil && HttpFlowValidateStatoFamiglia.Msg == "missing token" {
			MetricsUserTokenUnauthenticatedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			return status.Wrap(HttpFlowValidateStatoFamiglia.Err, status.Unauthenticated)
		}
		if HttpFlowValidateStatoFamiglia.Err != nil && HttpFlowValidateStatoFamiglia.Msg == "bad request" {
			return status.Wrap(HttpFlowValidateStatoFamiglia.Err, status.InvalidArgument)
		}
		if HttpFlowValidateStatoFamiglia.Err != nil && HttpFlowValidateStatoFamiglia.Msg == "wrong input paramenter" {
			return status.Wrap(HttpFlowValidateStatoFamiglia.Err, status.InvalidArgument)
		}
		if HttpFlowValidateStatoFamiglia.Err != nil {

			return status.Wrap(HttpFlowValidateStatoFamiglia.Err, status.Internal)

		}
		*output = HttpFlowValidateStatoFamiglia.Response
		return nil
	})

	uc.SetTitle("Stato famiglia")
	uc.SetDescription("Validate Stato Famiglia payload")
	uc.SetTags("ANPR")
	uc.SetExpectedErrors(status.NotFound)

	return uc
}

func GetAccertamentoCittadinanza(sctx *ServerContext) usecase.Interactor {
	servicesSync := sctx.ServicesSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input AnprRequestInput, output *anpr.AccertamentoCittadinanzaFormattedResponse) error {
		servicesSync.RLock()
		defer servicesSync.RUnlock()

		HttpFlowAccertamentoCittadinanza := &HttpFlowAccertamentoCittadinanza{
			Sctx:    sctx,
			Request: input,
		}

		HttpFlowAccertamentoCittadinanza.Exec()

		if HttpFlowAccertamentoCittadinanza.Err != nil && HttpFlowAccertamentoCittadinanza.Msg == "internal error" {
			return status.Wrap(HttpFlowAccertamentoCittadinanza.Err, status.Internal)
		}
		if HttpFlowAccertamentoCittadinanza.Err != nil && HttpFlowAccertamentoCittadinanza.Msg == "invalid token" {
			MetricsUserTokenPermissionDeniedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			return status.Wrap(HttpFlowAccertamentoCittadinanza.Err, status.PermissionDenied)
		}
		if HttpFlowAccertamentoCittadinanza.Err != nil && HttpFlowAccertamentoCittadinanza.Msg == "missing token" {
			MetricsUserTokenUnauthenticatedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			return status.Wrap(HttpFlowAccertamentoCittadinanza.Err, status.Unauthenticated)
		}
		if HttpFlowAccertamentoCittadinanza.Err != nil && HttpFlowAccertamentoCittadinanza.Msg == "bad request" {
			return status.Wrap(HttpFlowAccertamentoCittadinanza.Err, status.InvalidArgument)
		}
		if HttpFlowAccertamentoCittadinanza.Err != nil && HttpFlowAccertamentoCittadinanza.Msg == "not found" {
			return status.Wrap(HttpFlowAccertamentoCittadinanza.Err, status.NotFound)
		}
		if HttpFlowAccertamentoCittadinanza.Err != nil {
			return status.Wrap(HttpFlowAccertamentoCittadinanza.Err, status.Internal)
		}
		*output = HttpFlowAccertamentoCittadinanza.Response
		return nil
	})

	uc.SetTitle("Accertamento Cittadinanza")
	uc.SetDescription("fruizione e-service Accertamento Cittadinanza")
	uc.SetTags("ANPR")
	uc.SetExpectedErrors(status.NotFound)

	return uc
}

type ValidateAccertamentoCittadinanzaInput struct {
	FiscalCode string                                     `query:"fiscal_code" description:"italian fiscal code" example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	Token      string                                     `header:"Authorization" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	ConfigId   string                                     `query:"config_id" description:"id that identify a specific configuration to call an e-service" example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	Format     string                                     `query:"format" description:"identify the output payload structure"`
	Data       anpr.AccertamentoCittadinanzaFormattedData `json:"Data" description:"data to validate. Can be different depending by the format param" example:"..."`
	Meta       anpr.Meta                                  `json:"Meta" description:"contains the signed data to validate" example:"..."`
}

func ValidateAccertamentoCittadinanza(sctx *ServerContext) usecase.Interactor {
	servicesSync := sctx.ServicesSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input ValidateAccertamentoCittadinanzaInput, output *anpr.ValidateAnpresponse) error {
		servicesSync.RLock()
		defer servicesSync.RUnlock()

		httpFlowValidateAccertamentoCittadinanza := &HttpFlowValidateAccertamentoCittadinanza{
			Sctx:    sctx,
			Request: input,
		}

		httpFlowValidateAccertamentoCittadinanza.Exec()

		if httpFlowValidateAccertamentoCittadinanza.Err != nil && httpFlowValidateAccertamentoCittadinanza.Msg == "internal error" {
			return status.Wrap(httpFlowValidateAccertamentoCittadinanza.Err, status.Internal)
		}
		if httpFlowValidateAccertamentoCittadinanza.Err != nil && httpFlowValidateAccertamentoCittadinanza.Msg == "invalid token" {
			MetricsUserTokenPermissionDeniedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			return status.PermissionDenied
		}
		if httpFlowValidateAccertamentoCittadinanza.Err != nil && httpFlowValidateAccertamentoCittadinanza.Msg == "missing token" {
			MetricsUserTokenUnauthenticatedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			return status.Wrap(httpFlowValidateAccertamentoCittadinanza.Err, status.Unauthenticated)
		}

		if httpFlowValidateAccertamentoCittadinanza.Err != nil && httpFlowValidateAccertamentoCittadinanza.Msg == "bad request" {
			return status.InvalidArgument
		}
		if httpFlowValidateAccertamentoCittadinanza.Err != nil {

			return status.Wrap(httpFlowValidateAccertamentoCittadinanza.Err, status.Internal)

		}
		*output = httpFlowValidateAccertamentoCittadinanza.Response
		return nil
	})

	uc.SetTitle("Accertamento Cittadinanza")
	uc.SetDescription("Validate Accertamento Cittadinanza payload")
	uc.SetTags("ANPR")
	uc.SetExpectedErrors(status.NotFound)

	return uc
}

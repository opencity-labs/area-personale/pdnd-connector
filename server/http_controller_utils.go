package server

import (
	"errors"

	"github.com/gofrs/uuid"
	"github.com/swaggest/usecase/status"
)

func ValidateTenantId(sctx *ServerContext, id string) error {
	_, err := uuid.FromString(id)
	if err != nil {
		return errors.New("tenant id is an invalid uuid")
	}
	_, err = GetTenantById(sctx, id)
	if err != nil && err.Error() == "not found" {
		return errors.New("tenant id does not belog to any tenant")
	}
	if err != nil {
		sctx.LogHttpError().Stack().Err(err).Msg("error checking if tenant exists. id: " + id)
		return status.Internal
	}

	return nil
}

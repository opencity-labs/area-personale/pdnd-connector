package server

import (
	"encoding/json"
	"errors"
	"io"
	"sort"
	"strings"

	"github.com/graymeta/stow"
	"gitlab.com/opencontent/stanza-del-cittadino/pdnd-connector/server/models"
)

func StoreTenants(sctx *ServerContext, tenant *models.Tenant) error {
	serverConfig := sctx.ServerConfig()
	tenantsCache := sctx.TenantsCache()

	basePath := serverConfig.StorageBasePath + "/tenants/"
	err := StoreTenantByPath(sctx, tenant, basePath)
	if err != nil {
		return err
	}
	err = tenantsCache.Set(sctx.Ctx(), tenant.TenantID, tenant)
	if err != nil {
		return err
	}

	return nil
}

func GetTenantById(sctx *ServerContext, tenantId string) (*models.Tenant, error) {
	serverConfig := sctx.ServerConfig()
	fileStorage := sctx.FileStorage()
	basePath := serverConfig.StorageBasePath + "/tenants/"

	var tenant *models.Tenant

	// Construct the item path for the given service ID
	itemPath := basePath + tenantId + ".json"
	// Retrieve the item from storage
	item, err := fileStorage.Item(itemPath)
	if err != nil {
		return tenant, err
	}

	// Open the item and read its content
	itemContent, err := item.Open()
	if err != nil {
		return tenant, err
	}
	defer itemContent.Close()

	itemBytes, err := io.ReadAll(itemContent)
	if err != nil {
		return tenant, err
	}

	// Unmarshal JSON content into the Service struct
	if err := json.Unmarshal(itemBytes, &tenant); err != nil {
		return tenant, err
	}

	return tenant, nil
}

func DeleteTenantById(sctx *ServerContext, tenantId string) error {
	serverConfig := sctx.ServerConfig()
	fileStorage := sctx.FileStorage()
	tenantsCache := sctx.TenantsCache()

	basePath := serverConfig.StorageBasePath + "/tenants/"

	itemPath := basePath + tenantId + ".json"

	item, err := fileStorage.Item(itemPath)
	if err != nil {
		return err
	}

	err = fileStorage.RemoveItem(item.ID())
	if err != nil {
		return err
	}

	err = tenantsCache.Delete(sctx.Ctx(), tenantId)
	if err != nil {
		return err
	}

	return nil
}

func GetTenantsPage(sctx *ServerContext, offset, limit int) ([]models.Tenant, error) {
	serverConfig := sctx.ServerConfig()
	fileStorage := sctx.FileStorage()
	basePath := serverConfig.StorageBasePath + "tenant/"

	var allTenants []models.Tenant

	err := stow.Walk(fileStorage, stow.CursorStart, offset+limit, func(item stow.Item, err error) error {
		if err != nil {
			return err
		}

		// Verify if the item is a JSON file
		if !strings.HasSuffix(item.Name(), ".json") {
			return nil
		}

		itemPath := item.Name()
		tenantID := strings.TrimPrefix(strings.TrimSuffix(itemPath, ".json"), basePath)

		itemContent, err := item.Open()
		if err != nil {
			return err
		}

		itemBytes, err := io.ReadAll(itemContent)
		if err != nil {
			return err
		}

		var tenant models.Tenant
		if err := json.Unmarshal(itemBytes, &tenant); err != nil {
			return err
		}
		tenant.TenantID = tenantID
		allTenants = append(allTenants, tenant)

		return nil
	})

	if err != nil {
		return nil, err
	}

	// Apply pagination to the result
	startIndex := offset
	endIndex := offset + limit
	if startIndex < 0 {
		startIndex = 0
	}
	if endIndex > len(allTenants) {
		endIndex = len(allTenants)
	}
	sort.SliceStable(allTenants, func(i, j int) bool {
		return allTenants[i].CreatedAt < allTenants[j].CreatedAt
	})

	return allTenants[startIndex:endIndex], nil
}

func TrashTenant(sctx *ServerContext, tenantId string) error {
	serverConfig := sctx.ServerConfig()
	tenantToDelete, err := GetTenantById(sctx, tenantId)
	if err != nil {
		return err
	}

	basePath := serverConfig.StorageBasePath + "/trash/" + "/tenants/"
	err = StoreTenantByPath(sctx, tenantToDelete, basePath)
	if err != nil {
		return err
	}

	err = TrashClientsTenants(sctx, tenantId)
	if err != nil && err.Error() != "not found" {
		return err
	}
	err = TrashKeysTenant(sctx, tenantId)
	if err != nil && err.Error() != "not found" {
		return err
	}
	err = DeleteTenantById(sctx, tenantId)
	if err != nil && err.Error() != "not found" {
		return nil
	}

	return nil
}

func StoreTenantByPath(sctx *ServerContext, tenant *models.Tenant, basePath string) error {
	fileStorage := sctx.FileStorage()

	itemBytes, err := json.Marshal(tenant)
	if err != nil {
		return errors.New("invalid data")
	}

	itemName := basePath + tenant.TenantID + ".json"
	itemContent := string(itemBytes)
	itemSize := int64(len(itemContent))

	itemReader := strings.NewReader(itemContent)

	_, err = fileStorage.Put(itemName, itemReader, itemSize, nil)
	if err != nil {
		return err
	}
	return nil
}

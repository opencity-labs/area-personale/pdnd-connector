package server

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	MetricsErrorAuthTotalRequests = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "oc_pdnd_api_request_auth_errors_total",
		Help: "The total number of requests with 403 or 401 response",
	}, []string{"env", "app_name"})
	MetricsAnprTotalSuccesRequests = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "oc_api_anpr_success_requests_total",
		Help: "The total number of success requests to anpr",
	}, []string{"env", "app_name"})

	MetricsAnprTotalErrorRequests = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "oc_api_anpr_error_requests_total",
		Help: "The total number of error requests to anpr",
	}, []string{"env", "app_name"})

	MetricsUserTokenPermissionDeniedRequests = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "oc_api_user_token_permission_denied_requests_total",
		Help: "The total number of request with user invalid token",
	}, []string{"env", "app_name"})

	MetricsUserTokenUnauthenticatedRequests = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "oc_api_user_token_unauthenticated_requests_total",
		Help: "The total number of request with no user token",
	}, []string{"env", "app_name"})

	MetricsAdminTokenPermissionDeniedRequests = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "oc_api_admin_token_permission_denied_requests_total",
		Help: "The total number of request with admin invalid token",
	}, []string{"env", "app_name"})

	MetricsAdminTokenUnauthenticatedRequests = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "oc_api_admin_token_unauthenticated_requests_total",
		Help: "The total number of request with no  admintoken",
	}, []string{"env", "app_name"})

	MetricsVoucherPdndLatency = promauto.NewHistogramVec(prometheus.HistogramOpts{
		Name: "oc_voucher_pdnd_latency",
		Help: "Duration of provider requests",
	}, []string{"env", "app_name", "path"})

	MetricsAnprLatency = promauto.NewHistogramVec(prometheus.HistogramOpts{
		Name: "oc_anpr_eservice_latency",
		Help: "Duration of provider requests",
	}, []string{"env", "app_name", "path"})

	MetricsErogatoreServerErrorsTotal = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "oc_pdnd_api_request_erogatore_5xx_total",
		Help: "The total number of requests to an erogatore that resulted in a 5xx response",
	}, []string{"env", "app_name", "erogatore"})
)

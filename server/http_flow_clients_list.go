package server

import (
	"errors"
	"fmt"

	"gitlab.com/opencontent/stanza-del-cittadino/pdnd-connector/server/models"
)

type HttpFlowClientsList struct {
	Name       string
	Sctx       *ServerContext
	Request    GetClientsByTenantIdInput
	Err        error
	Msg        string
	FlowStatus bool
	Response   models.ClientListResponse
}

func (r *HttpFlowClientsList) Exec() bool {
	r.FlowStatus = true
	return r.FlowStatus &&
		r.buildPaginatedClientList() &&
		r.setPublicKey()
}

func (r *HttpFlowClientsList) buildPaginatedClientList() bool {

	limit := r.Request.Limit
	offset := r.Request.Offset
	sort := r.Request.Sort

	ClientList, err := GetClientsPdndByTenantId(r.Sctx, r.Request.TenantId)
	if err != nil && err.Error() == "not found" {
		r.Sctx.LogHttpError().Stack().Err(err).Msg("no clients for tenant: " + r.Request.TenantId)
		r.Err = errors.New("clients not found")
		r.Msg = "clients not found"
		return false
	}

	totalClientsListSize := len(*ClientList)

	if limit == 0 {
		limit = 10
	}
	if limit < 0 {
		limit = 0
	}
	if offset < 0 {
		offset = 0
	}
	if limit > totalClientsListSize {
		limit = totalClientsListSize
	}
	if offset > totalClientsListSize {
		offset = totalClientsListSize
	}

	var selfPage, nextPage, prevPage string
	serverConfig := r.Sctx.serverConfig
	endpoint := serverConfig.HttpExternalBasePath
	sortPath := ""
	if sort != "" {
		sortPath = "&sort="
	}

	selfPage = fmt.Sprintf("/tenants/"+r.Request.TenantId+"/clients"+"?offset=%d&limit=%d"+sortPath+"%s", offset, limit, sort)
	if totalClientsListSize >= limit && offset < totalClientsListSize {
		nextOffset := offset + limit
		if nextOffset >= totalClientsListSize {
			nextPage = ""
		} else {
			nextPage = fmt.Sprintf("/tenants/"+r.Request.TenantId+"/clients"+"?offset=%d&limit=%d"+sortPath+"%s", nextOffset, limit, sort)
		}
	}
	if offset > 0 {
		prevOffset := offset - limit
		if prevOffset < 0 {
			prevOffset = 0
		}
		prevPage = fmt.Sprintf("/tenants/"+r.Request.TenantId+"/clients"+"?offset=%d&limit=%d"+sortPath+"%s", prevOffset, limit, sort)
	}

	if nextPage != "" {
		tmp := endpoint + nextPage
		r.Response.Links.Next = &tmp
	}
	if prevPage != "" {
		tmp := endpoint + prevPage
		r.Response.Links.Prev = &tmp
	}
	r.Response.Links.Self = endpoint + selfPage

	r.Response.Data = r.getPaginatedList(offset, limit, *ClientList)
	r.Response.Meta.Total = totalClientsListSize
	r.Response.Meta.Page.Limit = limit
	r.Response.Meta.Page.Offset = offset
	if sort != "" {
		r.Response.Meta.Page.Sort = &sort
	}

	return true
}

func (r *HttpFlowClientsList) setPublicKey() bool {
	for index, client := range r.Response.Data {
		publicKeyByte, err := GetPublicKey(r.Sctx, r.Request.TenantId, client.KeyPairId)
		if err != nil {
			r.Sctx.LogHttpError().Stack().Err(err).Msg("error retrieving public key for client:  : " + client.ID)
			r.Err = err
			r.Msg = "error retrieving public key for client:  : " + client.ID
			return false
		}
		r.Response.Data[index].PublicKey = string(publicKeyByte)
	}
	return true
}

func (r *HttpFlowClientsList) getPaginatedList(offset, limit int, ClientArray []models.ClientPDND) []models.ClientPDND {
	startIndex := offset
	endIndex := offset + limit
	if startIndex < 0 {
		startIndex = 0
	}
	if endIndex < 0 {
		endIndex = 0
	}
	if endIndex > len(ClientArray) {
		endIndex = len(ClientArray)
	}
	if startIndex > endIndex {
		startIndex = 0
	}
	/* 	sort.SliceStable(ClientArray, func(i, j int) bool {
		return ClientArray[i].CreatedAt < ClientArray[j].CreatedAt
	}) */

	return ClientArray[startIndex:endIndex]
}

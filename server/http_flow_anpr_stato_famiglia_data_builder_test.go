package server

import (
	"encoding/json"
	"reflect"
	"testing"

	"gitlab.com/opencontent/stanza-del-cittadino/pdnd-connector/server/models/anpr"
)

// should find no children
func TestBuildDataResponseFormatOcFigliTwo(t *testing.T) {
	jsonData := `{
  "listaSoggetti": {
    "datiSoggetto": [
      {
        "generalita": {
          "codiceFiscale": {
            "codFiscale": "QRTSGT90A01H501B",
            "validitaCF": "1"
          },
          "cognome": "QUARTO",
          "dataNascita": "1990-01-01",
          "idSchedaSoggettoANPR": "2760295",
          "luogoNascita": {
            "comune": {
              "codiceIstat": "058091",
              "nomeComune": "ROMA",
              "siglaProvinciaIstat": "RM"
            }
          },
          "nome": "SOGGETTO",
          "sesso": "M",
          "soggettoAIRE": ""
        },
        "legameSoggetto": {
          "codiceLegame": "1",
          "dataDecorrenza": "2017-02-23",
          "tipoLegame": "3"
        },
        "identificativi": {
          "idANPR": "ZR13449T3"
        }
      },
      {
        "generalita": {
          "codiceFiscale": {
            "codFiscale": "SGGNNA90R50F839T",
            "validitaCF": "1"
          },
          "cognome": "SOGGETTA",
          "dataNascita": "1990-10-10",
          "idSchedaSoggettoANPR": "87938835",
          "luogoNascita": {
            "comune": {
              "codiceIstat": "063049",
              "nomeComune": "NAPOLI",
              "siglaProvinciaIstat": "NA"
            }
          },
          "nome": "ANNA",
          "sesso": "F",
          "soggettoAIRE": ""
        },
        "legameSoggetto": {
          "codiceLegame": "2",
          "dataDecorrenza": "2021-11-15",
          "tipoLegame": "3"
        },
        "identificativi": {
          "idANPR": "EV62727VC"
        }
      }
    ]
  }
}`

	var statoFamiglia anpr.StatoFamiglia
	err := json.Unmarshal([]byte(jsonData), &statoFamiglia)
	if err != nil {
		t.Fatalf("Errore durante la decodifica del JSON: %v", err)
	}

	builder := &StatoFamigliaDataBuilder{
		StatoFamigliaResponse: statoFamiglia,
		FiscalCodeRichiedente: "QRTSGT90A01H501B",
	}

	result, _ := builder.buildDataResponseFormatOcFigli()
	expectedLen := 0

	// Verifica dei risultati
	if len(result.Children) != expectedLen {
		t.Errorf("Expected %v, got %v", expectedLen, len(result.Children))
	}
}

// Richiedente con grado di parentela non valido
func TestBuildDataResponseFormatOcFigliThree(t *testing.T) {
	jsonData := `{
    "listaSoggetti": {
        "datiSoggetto": [
            {
                "generalita": {
                    "codiceFiscale": {
                        "codFiscale": "CODICE FISCALE RICHIEDENTE",
                        "validitaCF": "1"
                    },
                    "cognome": "COGNOME RICHIEDENTE",
                    "dataNascita": "1976-09-01",
                    "idSchedaSoggettoANPR": "111111",
                    "luogoNascita": {
                        "comune": {
                            "codiceIstat": "111111",
                            "nomeComune": "COMUNE RICHIEDENTE",
                            "siglaProvinciaIstat": "PI"
                        }
                    },
                    "nome": "NOME RICHIEDENTE",
                    "sesso": "M"
                },
                "legameSoggetto": {
                    "codiceLegame": "4",
                    "dataDecorrenza": "2006-10-07",
                    "tipoLegame": "3"
                },
                "identificativi": {
                    "idANPR": "ID ANPR RICHIEDENTE"
                }
            },
            {
                "generalita": {
                    "codiceFiscale": {
                        "codFiscale": "FIGLIO RICHIEDENTE",
                        "validitaCF": "1"
                    },
                    "cognome": "COGNOME FIGLIO",
                    "dataNascita": "2009-07-30",
                    "idSchedaSoggettoANPR": "11111",
                    "idSchedaSoggettoComune": {
                        "idSchedaSoggetto": "1111",
                        "idSchedaSoggettoComuneIstat": "111"
                    },
                    "luogoNascita": {
                        "comune": {
                            "codiceIstat": "11",
                            "nomeComune": "COMUNE FIGLIO",
                            "siglaProvinciaIstat": "PI"
                        }
                    },
                    "nome": "NOME FIGLIO",
                    "sesso": "M"
                },
                "legameSoggetto": {
                    "codiceLegame": "3",
                    "dataDecorrenza": "2009-07-30",
                    "tipoLegame": "3"
                },
                "identificativi": {
                    "idANPR": "111111"
                }
            },
            {
                "generalita": {
                    "codiceFiscale": {
                        "codFiscale": "CODICE FISCALE CONIUGE",
                        "validitaCF": "1"
                    },
                    "cognome": "COGNOME CONIUGE",
                    "dataNascita": "1976-11-18",
                    "idSchedaSoggettoANPR": "111111",
                    "idSchedaSoggettoComune": {
                        "idSchedaSoggetto": "1111",
                        "idSchedaSoggettoComuneIstat": "1111"
                    },
                    "luogoNascita": {
                        "comune": {
                            "codiceIstat": "1111",
                            "nomeComune": "COMUNE CONIUGE",
                            "siglaProvinciaIstat": "PI"
                        }
                    },
                    "nome": "NOME CONIUGE",
                    "sesso": "F"
                },
                "legameSoggetto": {
                    "codiceLegame": "1",
                    "dataDecorrenza": "2004-08-28",
                    "tipoLegame": "3"
                },
                "identificativi": {
                    "idANPR": "11111"
                }
            },
            {
                "generalita": {
                    "codiceFiscale": {
                        "codFiscale": "FIGLIA RICHIEDENTE",
                        "validitaCF": "1"
                    },
                    "cognome": "COGNOME FIGLIA",
                    "dataNascita": "2014-08-05",
                    "idSchedaSoggettoANPR": "11111",
                    "idSchedaSoggettoComune": {
                        "idSchedaSoggetto": "11111",
                        "idSchedaSoggettoComuneIstat": "11111"
                    },
                    "luogoNascita": {
                        "comune": {
                            "codiceIstat": "1111",
                            "nomeComune": "COMUNE FIGLIA",
                            "siglaProvinciaIstat": "PI"
                        }
                    },
                    "nome": "NOME FIGLIA",
                    "sesso": "F"
                },
                "legameSoggetto": {
                    "codiceLegame": "3",
                    "dataDecorrenza": "2014-08-05",
                    "tipoLegame": "3"
                },
                "identificativi": {
                    "idANPR": "1111111"
                }
            }
        ]
    },
    "idOperazioneANPR": "1111111",
    "listaAnomalie": [
        {
            "codiceErroreAnomalia": "EN148",
            "testoErroreAnomalia": "Il D.M. Ministero dell'Interno 3 marzo 2023 (art.3 comma 3) prevede l'accesso ad ANPR esclusivamente con identificativo unico nazionale (ID ANPR), si  rappresenta pertanto l'urgenza di provvedere all'integrazione dei propri sistemi.",
            "tipoErroreAnomalia": "W"
        }
    ]
}`

	var statoFamiglia anpr.StatoFamiglia
	err := json.Unmarshal([]byte(jsonData), &statoFamiglia)
	if err != nil {
		t.Fatalf("Errore durante la decodifica del JSON: %v", err)
	}

	builder := &StatoFamigliaDataBuilder{
		StatoFamigliaResponse: statoFamiglia,
		FiscalCodeRichiedente: "CODICE FISCALE RICHIEDENTE",
	}

	_, err = builder.buildDataResponseFormatOcFigli()

	if err == nil {
		t.Errorf("Expected error but got")
	}
	if err.Error() != "degree of kinship to find children unsupported" {
		t.Errorf("Expected error to be -  degree of kinship to find children unsupported, but got:" + err.Error())
	}
}

// il richiedente della scheda è lo sposo/sposa dell'intestatario della scheda
func TestBuildDataResponseFormatOcFigliFour(t *testing.T) {
	jsonData := `{
    "listaSoggetti": {
        "datiSoggetto": [
            {
                "generalita": {
                    "codiceFiscale": {
                        "codFiscale": "CODICE FISCALE RICHIEDENTE",
                        "validitaCF": "1"
                    },
                    "cognome": "COGNOME RICHIEDENTE",
                    "dataNascita": "1976-09-01",
                    "idSchedaSoggettoANPR": "111111",
                    "luogoNascita": {
                        "comune": {
                            "codiceIstat": "111111",
                            "nomeComune": "COMUNE RICHIEDENTE",
                            "siglaProvinciaIstat": "PI"
                        }
                    },
                    "nome": "NOME RICHIEDENTE",
                    "sesso": "M"
                },
                "legameSoggetto": {
                    "codiceLegame": "2",
                    "dataDecorrenza": "2006-10-07",
                    "tipoLegame": "3"
                },
                "identificativi": {
                    "idANPR": "ID ANPR RICHIEDENTE"
                }
            },
            {
                "generalita": {
                    "codiceFiscale": {
                        "codFiscale": "FIGLIO RICHIEDENTE",
                        "validitaCF": "1"
                    },
                    "cognome": "COGNOME FIGLIO",
                    "dataNascita": "2009-07-30",
                    "idSchedaSoggettoANPR": "11111",
                    "idSchedaSoggettoComune": {
                        "idSchedaSoggetto": "1111",
                        "idSchedaSoggettoComuneIstat": "111"
                    },
                    "luogoNascita": {
                        "comune": {
                            "codiceIstat": "11",
                            "nomeComune": "COMUNE FIGLIO",
                            "siglaProvinciaIstat": "PI"
                        }
                    },
                    "nome": "NOME FIGLIO",
                    "sesso": "M"
                },
                "legameSoggetto": {
                    "codiceLegame": "3",
                    "dataDecorrenza": "2009-07-30",
                    "tipoLegame": "3"
                },
                "identificativi": {
                    "idANPR": "111111"
                }
            },
            {
                "generalita": {
                    "codiceFiscale": {
                        "codFiscale": "CODICE FISCALE CONIUGE",
                        "validitaCF": "1"
                    },
                    "cognome": "COGNOME CONIUGE",
                    "dataNascita": "1976-11-18",
                    "idSchedaSoggettoANPR": "111111",
                    "idSchedaSoggettoComune": {
                        "idSchedaSoggetto": "1111",
                        "idSchedaSoggettoComuneIstat": "1111"
                    },
                    "luogoNascita": {
                        "comune": {
                            "codiceIstat": "1111",
                            "nomeComune": "COMUNE CONIUGE",
                            "siglaProvinciaIstat": "PI"
                        }
                    },
                    "nome": "NOME CONIUGE",
                    "sesso": "F"
                },
                "legameSoggetto": {
                    "codiceLegame": "1",
                    "dataDecorrenza": "2004-08-28",
                    "tipoLegame": "3"
                },
                "identificativi": {
                    "idANPR": "11111"
                }
            },
            {
                "generalita": {
                    "codiceFiscale": {
                        "codFiscale": "FIGLIA RICHIEDENTE",
                        "validitaCF": "1"
                    },
                    "cognome": "COGNOME FIGLIA",
                    "dataNascita": "2014-08-05",
                    "idSchedaSoggettoANPR": "11111",
                    "idSchedaSoggettoComune": {
                        "idSchedaSoggetto": "11111",
                        "idSchedaSoggettoComuneIstat": "11111"
                    },
                    "luogoNascita": {
                        "comune": {
                            "codiceIstat": "1111",
                            "nomeComune": "COMUNE FIGLIA",
                            "siglaProvinciaIstat": "PI"
                        }
                    },
                    "nome": "NOME FIGLIA",
                    "sesso": "F"
                },
                "legameSoggetto": {
                    "codiceLegame": "3",
                    "dataDecorrenza": "2014-08-05",
                    "tipoLegame": "3"
                },
                "identificativi": {
                    "idANPR": "1111111"
                }
            }
        ]
    },
    "idOperazioneANPR": "1111111",
    "listaAnomalie": [
        {
            "codiceErroreAnomalia": "EN148",
            "testoErroreAnomalia": "Il D.M. Ministero dell'Interno 3 marzo 2023 (art.3 comma 3) prevede l'accesso ad ANPR esclusivamente con identificativo unico nazionale (ID ANPR), si  rappresenta pertanto l'urgenza di provvedere all'integrazione dei propri sistemi.",
            "tipoErroreAnomalia": "W"
        }
    ]
}`

	var statoFamiglia anpr.StatoFamiglia
	err := json.Unmarshal([]byte(jsonData), &statoFamiglia)
	if err != nil {
		t.Fatalf("Errore durante la decodifica del JSON: %v", err)
	}

	builder := &StatoFamigliaDataBuilder{
		StatoFamigliaResponse: statoFamiglia,
		FiscalCodeRichiedente: "CODICE FISCALE RICHIEDENTE",
	}

	result, _ := builder.buildDataResponseFormatOcFigli()

	expectedChildren := []anpr.Person{
		{
			BirthDate:  "30/07/2009",
			BirthPlace: "COMUNE FIGLIO",
			FamilyName: "COGNOME FIGLIO",
			GivenName:  "NOME FIGLIO",
			Gender:     "maschio",
			TaxID:      "FIGLIO RICHIEDENTE",
		},
		{
			BirthDate:  "05/08/2014",
			BirthPlace: "COMUNE FIGLIA",
			FamilyName: "COGNOME FIGLIA",
			GivenName:  "NOME FIGLIA",
			Gender:     "femmina",
			TaxID:      "FIGLIA RICHIEDENTE",
		},
	}

	if !reflect.DeepEqual(result.Children, expectedChildren) {
		t.Errorf("Expected %v, got %v", expectedChildren, result.Children)
	}
}

// il richiedente della scheda è anche l'intestatario della scheda
func TestBuildDataResponseFormatOcFigliFive(t *testing.T) {
	jsonData := `{
    "listaSoggetti": {
        "datiSoggetto": [
            {
                "generalita": {
                    "codiceFiscale": {
                        "codFiscale": "CODICE FISCALE RICHIEDENTE",
                        "validitaCF": "1"
                    },
                    "cognome": "COGNOME RICHIEDENTE",
                    "dataNascita": "1976-09-01",
                    "idSchedaSoggettoANPR": "111111",
                    "luogoNascita": {
                        "comune": {
                            "codiceIstat": "111111",
                            "nomeComune": "COMUNE RICHIEDENTE",
                            "siglaProvinciaIstat": "PI"
                        }
                    },
                    "nome": "NOME RICHIEDENTE",
                    "sesso": "M"
                },
                "legameSoggetto": {
                    "codiceLegame": "1",
                    "dataDecorrenza": "2006-10-07",
                    "tipoLegame": "3"
                },
                "identificativi": {
                    "idANPR": "ID ANPR RICHIEDENTE"
                }
            },
            {
                "generalita": {
                    "codiceFiscale": {
                        "codFiscale": "FIGLIO RICHIEDENTE",
                        "validitaCF": "1"
                    },
                    "cognome": "COGNOME FIGLIO",
                    "dataNascita": "2009-07-30",
                    "idSchedaSoggettoANPR": "11111",
                    "idSchedaSoggettoComune": {
                        "idSchedaSoggetto": "1111",
                        "idSchedaSoggettoComuneIstat": "111"
                    },
                    "luogoNascita": {
                        "comune": {
                            "codiceIstat": "11",
                            "nomeComune": "COMUNE FIGLIO",
                            "siglaProvinciaIstat": "PI"
                        }
                    },
                    "nome": "NOME FIGLIO",
                    "sesso": "M"
                },
                "legameSoggetto": {
                    "codiceLegame": "3",
                    "dataDecorrenza": "2009-07-30",
                    "tipoLegame": "3"
                },
                "identificativi": {
                    "idANPR": "111111"
                }
            },
            {
                "generalita": {
                    "codiceFiscale": {
                        "codFiscale": "CODICE FISCALE CONIUGE",
                        "validitaCF": "1"
                    },
                    "cognome": "COGNOME CONIUGE",
                    "dataNascita": "1976-11-18",
                    "idSchedaSoggettoANPR": "111111",
                    "idSchedaSoggettoComune": {
                        "idSchedaSoggetto": "1111",
                        "idSchedaSoggettoComuneIstat": "1111"
                    },
                    "luogoNascita": {
                        "comune": {
                            "codiceIstat": "1111",
                            "nomeComune": "COMUNE CONIUGE",
                            "siglaProvinciaIstat": "PI"
                        }
                    },
                    "nome": "NOME CONIUGE",
                    "sesso": "F"
                },
                "legameSoggetto": {
                    "codiceLegame": "2",
                    "dataDecorrenza": "2004-08-28",
                    "tipoLegame": "3"
                },
                "identificativi": {
                    "idANPR": "11111"
                }
            },
            {
                "generalita": {
                    "codiceFiscale": {
                        "codFiscale": "FIGLIA RICHIEDENTE",
                        "validitaCF": "1"
                    },
                    "cognome": "COGNOME FIGLIA",
                    "dataNascita": "2014-08-05",
                    "idSchedaSoggettoANPR": "11111",
                    "idSchedaSoggettoComune": {
                        "idSchedaSoggetto": "11111",
                        "idSchedaSoggettoComuneIstat": "11111"
                    },
                    "luogoNascita": {
                        "comune": {
                            "codiceIstat": "1111",
                            "nomeComune": "COMUNE FIGLIA",
                            "siglaProvinciaIstat": "PI"
                        }
                    },
                    "nome": "NOME FIGLIA",
                    "sesso": "F"
                },
                "legameSoggetto": {
                    "codiceLegame": "3",
                    "dataDecorrenza": "2014-08-05",
                    "tipoLegame": "3"
                },
                "identificativi": {
                    "idANPR": "1111111"
                }
            }
        ]
    },
    "idOperazioneANPR": "1111111",
    "listaAnomalie": [
        {
            "codiceErroreAnomalia": "EN148",
            "testoErroreAnomalia": "Il D.M. Ministero dell'Interno 3 marzo 2023 (art.3 comma 3) prevede l'accesso ad ANPR esclusivamente con identificativo unico nazionale (ID ANPR), si  rappresenta pertanto l'urgenza di provvedere all'integrazione dei propri sistemi.",
            "tipoErroreAnomalia": "W"
        }
    ]
}`

	var statoFamiglia anpr.StatoFamiglia
	err := json.Unmarshal([]byte(jsonData), &statoFamiglia)
	if err != nil {
		t.Fatalf("Errore durante la decodifica del JSON: %v", err)
	}

	builder := &StatoFamigliaDataBuilder{
		StatoFamigliaResponse: statoFamiglia,
		FiscalCodeRichiedente: "CODICE FISCALE RICHIEDENTE",
	}

	result, _ := builder.buildDataResponseFormatOcFigli()

	expectedChildren := []anpr.Person{
		{
			BirthDate:  "30/07/2009",
			BirthPlace: "COMUNE FIGLIO",
			FamilyName: "COGNOME FIGLIO",
			GivenName:  "NOME FIGLIO",
			Gender:     "maschio",
			TaxID:      "FIGLIO RICHIEDENTE",
		},
		{
			BirthDate:  "05/08/2014",
			BirthPlace: "COMUNE FIGLIA",
			FamilyName: "COGNOME FIGLIA",
			GivenName:  "NOME FIGLIA",
			Gender:     "femmina",
			TaxID:      "FIGLIA RICHIEDENTE",
		},
	}

	if !reflect.DeepEqual(result.Children, expectedChildren) {
		t.Errorf("Expected %v, got %v", expectedChildren, result.Children)
	}
}

// il richiedente è anche l'intestatario ma non ha figli
func TestBuildDataResponseFormatOcFigliSix(t *testing.T) {
	jsonData := `{
    "listaSoggetti": {
        "datiSoggetto": [
            {
                "generalita": {
                    "codiceFiscale": {
                        "codFiscale": "CODICE FISCALE RICHIEDENTE",
                        "validitaCF": "1"
                    },
                    "cognome": "COGNOME RICHIEDENTE",
                    "dataNascita": "1976-09-01",
                    "idSchedaSoggettoANPR": "111111",
                    "luogoNascita": {
                        "comune": {
                            "codiceIstat": "111111",
                            "nomeComune": "COMUNE RICHIEDENTE",
                            "siglaProvinciaIstat": "PI"
                        }
                    },
                    "nome": "NOME RICHIEDENTE",
                    "sesso": "M"
                },
                "legameSoggetto": {
                    "codiceLegame": "1",
                    "dataDecorrenza": "2006-10-07",
                    "tipoLegame": "3"
                },
                "identificativi": {
                    "idANPR": "ID ANPR RICHIEDENTE"
                }
            },
            {
                "generalita": {
                    "codiceFiscale": {
                        "codFiscale": "CODICE FISCALE CONIUGE",
                        "validitaCF": "1"
                    },
                    "cognome": "COGNOME CONIUGE",
                    "dataNascita": "1976-11-18",
                    "idSchedaSoggettoANPR": "111111",
                    "idSchedaSoggettoComune": {
                        "idSchedaSoggetto": "1111",
                        "idSchedaSoggettoComuneIstat": "1111"
                    },
                    "luogoNascita": {
                        "comune": {
                            "codiceIstat": "1111",
                            "nomeComune": "COMUNE CONIUGE",
                            "siglaProvinciaIstat": "PI"
                        }
                    },
                    "nome": "NOME CONIUGE",
                    "sesso": "F"
                },
                "legameSoggetto": {
                    "codiceLegame": "2",
                    "dataDecorrenza": "2004-08-28",
                    "tipoLegame": "3"
                },
                "identificativi": {
                    "idANPR": "11111"
                }
            }
        ]
    },
    "idOperazioneANPR": "1111111",
    "listaAnomalie": [
        {
            "codiceErroreAnomalia": "EN148",
            "testoErroreAnomalia": "Il D.M. Ministero dell'Interno 3 marzo 2023 (art.3 comma 3) prevede l'accesso ad ANPR esclusivamente con identificativo unico nazionale (ID ANPR), si  rappresenta pertanto l'urgenza di provvedere all'integrazione dei propri sistemi.",
            "tipoErroreAnomalia": "W"
        }
    ]
}`

	var statoFamiglia anpr.StatoFamiglia
	err := json.Unmarshal([]byte(jsonData), &statoFamiglia)
	if err != nil {
		t.Fatalf("Errore durante la decodifica del JSON: %v", err)
	}

	builder := &StatoFamigliaDataBuilder{
		StatoFamigliaResponse: statoFamiglia,
		FiscalCodeRichiedente: "CODICE FISCALE RICHIEDENTE",
	}

	result, err := builder.buildDataResponseFormatOcFigli()

	expectedChildren := []anpr.Person{}

	if len(result.Children) != 0 {
		t.Errorf("Expected %v, got %v", expectedChildren, result.Children)
	}
	if err != nil {
		t.Errorf("Expected no error but got" + err.Error())
	}
}

func TestBuildDataResponseFormatOcFigli_NoBrothersOrSisters(t *testing.T) {
	jsonData := `{
		"listaSoggetti": {
			"datiSoggetto": [
				{
					"generalita": {
						"codiceFiscale": {
							"codFiscale": "CODICE FISCALE RICHIEDENTE",
							"validitaCF": "1"
						},
						"cognome": "COGNOME RICHIEDENTE",
						"dataNascita": "1976-09-01",
						"idSchedaSoggettoANPR": "111111",
						"luogoNascita": {
							"comune": {
								"codiceIstat": "111111",
								"nomeComune": "COMUNE RICHIEDENTE",
								"siglaProvinciaIstat": "PI"
							}
						},
						"nome": "NOME RICHIEDENTE",
						"sesso": "M"
					},
					"legameSoggetto": {
						"codiceLegame": "3",
						"dataDecorrenza": "2006-10-07",
						"tipoLegame": "Figlio"
					},
					"identificativi": {
						"idANPR": "ID ANPR RICHIEDENTE"
					}
				},
				{
					"generalita": {
						"codiceFiscale": {
							"codFiscale": "CODICE FISCALE INTESTATARIO",
							"validitaCF": "1"
						},
						"cognome": "COGNOME INTESTATARIO",
						"dataNascita": "1950-01-01",
						"idSchedaSoggettoANPR": "222222",
						"luogoNascita": {
							"comune": {
								"codiceIstat": "222222",
								"nomeComune": "COMUNE INTESTATARIO",
								"siglaProvinciaIstat": "PI"
							}
						},
						"nome": "NOME INTESTATARIO",
						"sesso": "M"
					},
					"legameSoggetto": {
						"codiceLegame": "1",
						"dataDecorrenza": "2000-01-01",
						"tipoLegame": "Intestatario"
					},
					"identificativi": {
						"idANPR": "ID ANPR INTESTATARIO"
					}
				},
				{
					"generalita": {
						"codiceFiscale": {
							"codFiscale": "CODICE FISCALE NIPOTE 1",
							"validitaCF": "1"
						},
						"cognome": "COGNOME NIPOTE 1",
						"dataNascita": "2009-07-30",
						"idSchedaSoggettoANPR": "333333",
						"luogoNascita": {
							"comune": {
								"codiceIstat": "333333",
								"nomeComune": "COMUNE NIPOTE",
								"siglaProvinciaIstat": "PI"
							}
						},
						"nome": "NOME NIPOTE 1",
						"sesso": "M"
					},
					"legameSoggetto": {
						"codiceLegame": "4",
						"dataDecorrenza": "2009-07-30",
						"tipoLegame": "Nipote"
					},
					"identificativi": {
						"idANPR": "ID ANPR NIPOTE 1"
					}
				},
				{
					"generalita": {
						"codiceFiscale": {
							"codFiscale": "CODICE FISCALE NIPOTE 2",
							"validitaCF": "1"
						},
						"cognome": "COGNOME NIPOTE 2",
						"dataNascita": "2014-08-05",
						"idSchedaSoggettoANPR": "444444",
						"luogoNascita": {
							"comune": {
								"codiceIstat": "444444",
								"nomeComune": "COMUNE NIPOTE",
								"siglaProvinciaIstat": "PI"
							}
						},
						"nome": "NOME NIPOTE 2",
						"sesso": "F"
					},
					"legameSoggetto": {
						"codiceLegame": "4",
						"dataDecorrenza": "2014-08-05",
						"tipoLegame": "Nipote"
					},
					"identificativi": {
						"idANPR": "ID ANPR NIPOTE 2"
					}
				}
			]
		},
		"idOperazioneANPR": "1111111"
	}`

	var statoFamiglia anpr.StatoFamiglia
	err := json.Unmarshal([]byte(jsonData), &statoFamiglia)
	if err != nil {
		t.Fatalf("Errore durante la decodifica del JSON: %v", err)
	}

	builder := &StatoFamigliaDataBuilder{
		StatoFamigliaResponse: statoFamiglia,
		FiscalCodeRichiedente: "CODICE FISCALE RICHIEDENTE",
	}

	_, err = builder.buildDataResponseFormatOcFigli()

	if err == nil {
		t.Errorf("Expected error but got")
	}
	if err.Error() != "degree of kinship to find children unsupported" {
		t.Errorf("Expected error to be -  degree of kinship to find children unsupported, but got:" + err.Error())
	}
}

func TestBuildDataResponseFormatOcFigli_WithBrothersOrSisters(t *testing.T) {
	jsonData := `{
		"listaSoggetti": {
			"datiSoggetto": [
				{
					"generalita": {
						"codiceFiscale": {
							"codFiscale": "CODICE FISCALE RICHIEDENTE",
							"validitaCF": "1"
						},
						"cognome": "COGNOME RICHIEDENTE",
						"dataNascita": "1976-09-01",
						"idSchedaSoggettoANPR": "111111",
						"luogoNascita": {
							"comune": {
								"codiceIstat": "111111",
								"nomeComune": "COMUNE RICHIEDENTE",
								"siglaProvinciaIstat": "PI"
							}
						},
						"nome": "NOME RICHIEDENTE",
						"sesso": "M"
					},
					"legameSoggetto": {
						"codiceLegame": "3",
						"dataDecorrenza": "2006-10-07",
						"tipoLegame": "Figlio"
					},
					"identificativi": {
						"idANPR": "ID ANPR RICHIEDENTE"
					}
				},
                {
					"generalita": {
						"codiceFiscale": {
							"codFiscale": "CODICE FISCALE FRATELLO RICHIEDENTE",
							"validitaCF": "1"
						},
						"cognome": "COGNOME FRATELLO RICHIEDENTE",
						"dataNascita": "1976-09-01",
						"idSchedaSoggettoANPR": "111111",
						"luogoNascita": {
							"comune": {
								"codiceIstat": "111111",
								"nomeComune": "COMUNE FRATELLO RICHIEDENTE",
								"siglaProvinciaIstat": "PI"
							}
						},
						"nome": "NOME FRATELLO RICHIEDENTE",
						"sesso": "M"
					},
					"legameSoggetto": {
						"codiceLegame": "3",
						"dataDecorrenza": "2006-10-07",
						"tipoLegame": "Figlio"
					},
					"identificativi": {
						"idANPR": "ID ANPR FRATELLO RICHIEDENTE"
					}
				},
				{
					"generalita": {
						"codiceFiscale": {
							"codFiscale": "CODICE FISCALE INTESTATARIO",
							"validitaCF": "1"
						},
						"cognome": "COGNOME INTESTATARIO",
						"dataNascita": "1950-01-01",
						"idSchedaSoggettoANPR": "222222",
						"luogoNascita": {
							"comune": {
								"codiceIstat": "222222",
								"nomeComune": "COMUNE INTESTATARIO",
								"siglaProvinciaIstat": "PI"
							}
						},
						"nome": "NOME INTESTATARIO",
						"sesso": "M"
					},
					"legameSoggetto": {
						"codiceLegame": "1",
						"dataDecorrenza": "2000-01-01",
						"tipoLegame": "Intestatario"
					},
					"identificativi": {
						"idANPR": "ID ANPR INTESTATARIO"
					}
				},
				{
					"generalita": {
						"codiceFiscale": {
							"codFiscale": "CODICE FISCALE NIPOTE 1",
							"validitaCF": "1"
						},
						"cognome": "COGNOME NIPOTE 1",
						"dataNascita": "2009-07-30",
						"idSchedaSoggettoANPR": "333333",
						"luogoNascita": {
							"comune": {
								"codiceIstat": "333333",
								"nomeComune": "COMUNE NIPOTE",
								"siglaProvinciaIstat": "PI"
							}
						},
						"nome": "NOME NIPOTE 1",
						"sesso": "M"
					},
					"legameSoggetto": {
						"codiceLegame": "4",
						"dataDecorrenza": "2009-07-30",
						"tipoLegame": "Nipote"
					},
					"identificativi": {
						"idANPR": "ID ANPR NIPOTE 1"
					}
				},
				{
					"generalita": {
						"codiceFiscale": {
							"codFiscale": "CODICE FISCALE NIPOTE 2",
							"validitaCF": "1"
						},
						"cognome": "COGNOME NIPOTE 2",
						"dataNascita": "2014-08-05",
						"idSchedaSoggettoANPR": "444444",
						"luogoNascita": {
							"comune": {
								"codiceIstat": "444444",
								"nomeComune": "COMUNE NIPOTE",
								"siglaProvinciaIstat": "PI"
							}
						},
						"nome": "NOME NIPOTE 2",
						"sesso": "F"
					},
					"legameSoggetto": {
						"codiceLegame": "4",
						"dataDecorrenza": "2014-08-05",
						"tipoLegame": "Nipote"
					},
					"identificativi": {
						"idANPR": "ID ANPR NIPOTE 2"
					}
				}
			]
		},
		"idOperazioneANPR": "1111111"
	}`

	var statoFamiglia anpr.StatoFamiglia
	err := json.Unmarshal([]byte(jsonData), &statoFamiglia)
	if err != nil {
		t.Fatalf("Errore durante la decodifica del JSON: %v", err)
	}

	builder := &StatoFamigliaDataBuilder{
		StatoFamigliaResponse: statoFamiglia,
		FiscalCodeRichiedente: "CODICE FISCALE RICHIEDENTE",
	}

	_, err = builder.buildDataResponseFormatOcFigli()

	if err == nil {
		t.Errorf("Expected error but got")
	}
	if err.Error() != "degree of kinship to find children unsupported" {
		t.Errorf("Expected error to be -  degree of kinship to find children unsupported, but got:" + err.Error())
	}
}

func TestBuildDataResponseFormatOcFigli_RichiedenteIsFatherOfCardHolder(t *testing.T) {
	jsonData := `{
		"listaSoggetti": {
			"datiSoggetto": [
				{
					"generalita": {
						"codiceFiscale": {
							"codFiscale": "CODICE FISCALE RICHIEDENTE",
							"validitaCF": "1"
						},
						"cognome": "COGNOME RICHIEDENTE",
						"dataNascita": "1950-01-01",
						"idSchedaSoggettoANPR": "111111",
						"luogoNascita": {
							"comune": {
								"codiceIstat": "111111",
								"nomeComune": "COMUNE RICHIEDENTE",
								"siglaProvinciaIstat": "PI"
							}
						},
						"nome": "NOME RICHIEDENTE",
						"sesso": "M"
					},
					"legameSoggetto": {
						"codiceLegame": "6",
						"dataDecorrenza": "1980-01-01",
						"tipoLegame": "Padre"
					},
					"identificativi": {
						"idANPR": "ID ANPR RICHIEDENTE"
					}
				},
				{
					"generalita": {
						"codiceFiscale": {
							"codFiscale": "CODICE FISCALE CARDHOLDER",
							"validitaCF": "1"
						},
						"cognome": "COGNOME CARDHOLDER",
						"dataNascita": "1976-09-01",
						"idSchedaSoggettoANPR": "222222",
						"luogoNascita": {
							"comune": {
								"codiceIstat": "222222",
								"nomeComune": "COMUNE CARDHOLDER",
								"siglaProvinciaIstat": "PI"
							}
						},
						"nome": "NOME CARDHOLDER",
						"sesso": "M"
					},
					"legameSoggetto": {
						"codiceLegame": "1",
						"dataDecorrenza": "2006-10-07",
						"tipoLegame": "Figlio"
					},
					"identificativi": {
						"idANPR": "ID ANPR CARDHOLDER"
					}
				}
			]
		},
		"idOperazioneANPR": "1111111"
	}`

	var statoFamiglia anpr.StatoFamiglia
	err := json.Unmarshal([]byte(jsonData), &statoFamiglia)
	if err != nil {
		t.Fatalf("Errore durante la decodifica del JSON: %v", err)
	}

	builder := &StatoFamigliaDataBuilder{
		StatoFamigliaResponse: statoFamiglia,
		FiscalCodeRichiedente: "CODICE FISCALE RICHIEDENTE",
	}

	result, err := builder.buildDataResponseFormatOcFigli()

	expectedChildren := []anpr.Person{
		{
			BirthDate:  "01/09/1976",
			BirthPlace: "COMUNE CARDHOLDER",
			FamilyName: "COGNOME CARDHOLDER",
			GivenName:  "NOME CARDHOLDER",
			Gender:     "maschio",
			TaxID:      "CODICE FISCALE CARDHOLDER",
		},
	}

	if !reflect.DeepEqual(result.Children, expectedChildren) {
		t.Errorf("Expected %v, got %v", expectedChildren, result.Children)
	}

	if err != nil {
		t.Errorf("Expected no error but got: %v", err)
	}
}

func TestBuildDataResponseFormatOcGenitori_CardHolder(t *testing.T) {
	jsonData := `{
		"listaSoggetti": {
			"datiSoggetto": [
				{
					"generalita": {
						"codiceFiscale": {
							"codFiscale": "CODICE FISCALE RICHIEDENTE",
							"validitaCF": "1"
						},
						"cognome": "COGNOME RICHIEDENTE",
						"dataNascita": "1976-09-01",
						"idSchedaSoggettoANPR": "111111",
						"luogoNascita": {
							"comune": {
								"codiceIstat": "111111",
								"nomeComune": "COMUNE RICHIEDENTE",
								"siglaProvinciaIstat": "PI"
							}
						},
						"nome": "NOME RICHIEDENTE",
						"sesso": "M"
					},
					"legameSoggetto": {
						"codiceLegame": "1", 
						"dataDecorrenza": "2006-10-07",
						"tipoLegame": "CardHolder"
					},
					"identificativi": {
						"idANPR": "ID ANPR RICHIEDENTE"
					}
				},
				{
					"generalita": {
						"codiceFiscale": {
							"codFiscale": "CODICE FISCALE PADRE",
							"validitaCF": "1"
						},
						"cognome": "COGNOME PADRE",
						"dataNascita": "1950-01-01",
						"idSchedaSoggettoANPR": "222222",
						"luogoNascita": {
							"comune": {
								"codiceIstat": "222222",
								"nomeComune": "COMUNE PADRE",
								"siglaProvinciaIstat": "PI"
							}
						},
						"nome": "NOME PADRE",
						"sesso": "M"
					},
					"legameSoggetto": {
						"codiceLegame": "6", 
						"dataDecorrenza": "1950-01-01",
						"tipoLegame": "Padre"
					},
					"identificativi": {
						"idANPR": "ID ANPR PADRE"
					}
				}
			]
		},
		"idOperazioneANPR": "1111111"
	}`

	var statoFamiglia anpr.StatoFamiglia
	err := json.Unmarshal([]byte(jsonData), &statoFamiglia)
	if err != nil {
		t.Fatalf("Errore durante la decodifica del JSON: %v", err)
	}

	builder := &StatoFamigliaDataBuilder{
		StatoFamigliaResponse: statoFamiglia,
		FiscalCodeRichiedente: "CODICE FISCALE RICHIEDENTE",
	}

	result, err := builder.buildDataResponseFormatOcGenitori()

	expectedParents := []anpr.Person{
		{
			BirthDate:  "01/01/1950",
			BirthPlace: "COMUNE PADRE",
			FamilyName: "COGNOME PADRE",
			GivenName:  "NOME PADRE",
			Gender:     "maschio",
			TaxID:      "CODICE FISCALE PADRE",
		},
	}

	if !reflect.DeepEqual(result.Parents, expectedParents) {
		t.Errorf("Expected %v, got %v", expectedParents, result.Parents)
	}

	if err != nil {
		t.Errorf("Expected no error but got: %v", err)
	}
}

func TestBuildDataResponseFormatOcGenitori_parentIsCarHolder(t *testing.T) {
	jsonData := `{
    "listaSoggetti": {
        "datiSoggetto": [
            {
                "generalita": {
                    "codiceFiscale": {
                        "codFiscale": "CODICE FISCALE FIGLIO",
                        "validitaCF": "1"
                    },
                    "cognome": "COGNOME FIGLIO",
                    "dataNascita": "1990-01-27",
                    "idSchedaSoggettoANPR": "ID FIGLIO",
                    "luogoNascita": {
                        "comune": {
                            "codiceIstat": "CODICE ISTAT FIGLIO",
                            "nomeComune": "COMUNE FIGLIO",
                            "siglaProvinciaIstat": "TN"
                        }
                    },
                    "nome": "NOME FIGLIO",
                    "sesso": "F"
                },
                "legameSoggetto": {
                    "codiceLegame": "3",
                    "dataDecorrenza": "2011-09-05",
                    "tipoLegame": "3"
                },
                "identificativi": {
                    "idANPR": "ID FIGLIO"
                }
            },
            {
                "generalita": {
                    "codiceFiscale": {
                        "codFiscale": "CODICE FISCALE INTESTATARIO",
                        "validitaCF": "1"
                    },
                    "cognome": "COGNOME INTESTATARIO",
                    "dataNascita": "1962-03-01",
                    "idSchedaSoggettoANPR": "ID INTESTATARIO",
                    "idSchedaSoggettoComune": {
                        "idSchedaSoggetto": "ID SCHEDA INTESTATARIO",
                        "idSchedaSoggettoComuneIstat": "ID SCHEDA SOGGETTO INTESTATARIO"
                    },
                    "luogoNascita": {
                        "comune": {
                            "codiceIstat": "022117",
                            "nomeComune": "COMUNE INTESTATARIO",
                            "siglaProvinciaIstat": "TN"
                        }
                    },
                    "nome": "NOME INTESTATARIO",
                    "sesso": "F"
                },
                "legameSoggetto": {
                    "codiceLegame": "1",
                    "dataDecorrenza": "2011-09-05",
                    "tipoLegame": "3"
                },
                "identificativi": {
                    "idANPR": "ID ANPR INTESTATARIO"
                }
            },
            {
                "generalita": {
                    "codiceFiscale": {
                        "codFiscale": "CODICE FISCALE FIGLIO 2",
                        "validitaCF": "1"
                    },
                    "cognome": "COGNOME FIGLIO 2",
                    "dataNascita": "1998-02-22",
                    "idSchedaSoggettoANPR": "SCHEDA SOGGETTO FIGLIO 2",
                    "idSchedaSoggettoComune": {
                        "idSchedaSoggetto": "ID SCHEDA SOGGETTO FIGLIO 2",
                        "idSchedaSoggettoComuneIstat": "12344"
                    },
                    "luogoNascita": {
                        "comune": {
                            "codiceIstat": "1234",
                            "nomeComune": "COMUNE FIGLIO 2",
                            "siglaProvinciaIstat": "TN"
                        }
                    },
                    "nome": "NOME FIGLIO 2",
                    "sesso": "M"
                },
                "legameSoggetto": {
                    "codiceLegame": "3",
                    "dataDecorrenza": "2011-09-05",
                    "tipoLegame": "3"
                },
                "identificativi": {
                    "idANPR": "ID ANPR FIGLIO 2"
                }
            },
            {
                "generalita": {
                    "codiceFiscale": {
                        "codFiscale": "CODICE FISCALE CONVIVENTE",
                        "validitaCF": "1"
                    },
                    "cognome": "COGNOME CONVIVENTE'",
                    "dataNascita": "1967-01-26",
                    "idSchedaSoggettoANPR": "12345",
                    "idSchedaSoggettoComune": {
                        "idSchedaSoggetto": "1234",
                        "idSchedaSoggettoComuneIstat": "12345"
                    },
                    "luogoNascita": {
                        "comune": {
                            "codiceIstat": "022117",
                            "nomeComune": "COMUNE CONVIVENTE",
                            "siglaProvinciaIstat": "TN"
                        }
                    },
                    "nome": "NOME CONVIVENTE",
                    "sesso": "M"
                },
                "legameSoggetto": {
                    "codiceLegame": "23",
                    "dataDecorrenza": "2011-09-05",
                    "tipoLegame": "3"
                },
                "identificativi": {
                    "idANPR": "ID ANPR CONVIVENTE"
                }
            }
        ]
    },
    "idOperazioneANPR": "12345",
    "listaAnomalie": [
        {
            "codiceErroreAnomalia": "EN148",
            "testoErroreAnomalia": "Il D.M. Ministero dell'Interno 3 marzo 2023 (art.3 comma 3) prevede l'accesso ad ANPR esclusivamente con identificativo unico nazionale (ID ANPR), si  rappresenta pertanto l'urgenza di provvedere all'integrazione dei propri sistemi.",
            "tipoErroreAnomalia": "W"
        }
    ]
}`

	var statoFamiglia anpr.StatoFamiglia
	err := json.Unmarshal([]byte(jsonData), &statoFamiglia)
	if err != nil {
		t.Fatalf("Errore durante la decodifica del JSON: %v", err)
	}

	builder := &StatoFamigliaDataBuilder{
		StatoFamigliaResponse: statoFamiglia,
		FiscalCodeRichiedente: "CODICE FISCALE FIGLIO",
	}

	result, err := builder.buildDataResponseFormatOcGenitori()

	expectedParents := []anpr.Person{
		{
			BirthDate:  "01/03/1962",
			BirthPlace: "COMUNE INTESTATARIO",
			FamilyName: "COGNOME INTESTATARIO",
			GivenName:  "NOME INTESTATARIO",
			Gender:     "femmina",
			TaxID:      "CODICE FISCALE INTESTATARIO",
		},
	}

	if !reflect.DeepEqual(result.Parents, expectedParents) {
		t.Errorf("Expected %v, got %v", expectedParents, result.Parents)
	}

	if err != nil {
		t.Errorf("Expected no error but got: %v", err)
	}
}

func TestBuildDataResponseFormatOcGenitori_parentIsCarHolderAndHasSpouse(t *testing.T) {
	jsonData := `{
    "listaSoggetti": {
        "datiSoggetto": [
            {
                "generalita": {
                    "codiceFiscale": {
                        "codFiscale": "CODICE FISCALE FIGLIO",
                        "validitaCF": "1"
                    },
                    "cognome": "COGNOME FIGLIO",
                    "dataNascita": "1990-01-27",
                    "idSchedaSoggettoANPR": "ID FIGLIO",
                    "luogoNascita": {
                        "comune": {
                            "codiceIstat": "CODICE ISTAT FIGLIO",
                            "nomeComune": "COMUNE FIGLIO",
                            "siglaProvinciaIstat": "TN"
                        }
                    },
                    "nome": "NOME FIGLIO",
                    "sesso": "F"
                },
                "legameSoggetto": {
                    "codiceLegame": "3",
                    "dataDecorrenza": "2011-09-05",
                    "tipoLegame": "3"
                },
                "identificativi": {
                    "idANPR": "ID FIGLIO"
                }
            },
            {
                "generalita": {
                    "codiceFiscale": {
                        "codFiscale": "CODICE FISCALE INTESTATARIO",
                        "validitaCF": "1"
                    },
                    "cognome": "COGNOME INTESTATARIO",
                    "dataNascita": "1962-03-01",
                    "idSchedaSoggettoANPR": "ID INTESTATARIO",
                    "idSchedaSoggettoComune": {
                        "idSchedaSoggetto": "ID SCHEDA INTESTATARIO",
                        "idSchedaSoggettoComuneIstat": "ID SCHEDA SOGGETTO INTESTATARIO"
                    },
                    "luogoNascita": {
                        "comune": {
                            "codiceIstat": "022117",
                            "nomeComune": "COMUNE INTESTATARIO",
                            "siglaProvinciaIstat": "TN"
                        }
                    },
                    "nome": "NOME INTESTATARIO",
                    "sesso": "F"
                },
                "legameSoggetto": {
                    "codiceLegame": "1",
                    "dataDecorrenza": "2011-09-05",
                    "tipoLegame": "3"
                },
                "identificativi": {
                    "idANPR": "ID ANPR INTESTATARIO"
                }
            },
            {
                "generalita": {
                    "codiceFiscale": {
                        "codFiscale": "CODICE FISCALE FIGLIO 2",
                        "validitaCF": "1"
                    },
                    "cognome": "COGNOME FIGLIO 2",
                    "dataNascita": "1998-02-22",
                    "idSchedaSoggettoANPR": "SCHEDA SOGGETTO FIGLIO 2",
                    "idSchedaSoggettoComune": {
                        "idSchedaSoggetto": "ID SCHEDA SOGGETTO FIGLIO 2",
                        "idSchedaSoggettoComuneIstat": "12344"
                    },
                    "luogoNascita": {
                        "comune": {
                            "codiceIstat": "1234",
                            "nomeComune": "COMUNE FIGLIO 2",
                            "siglaProvinciaIstat": "TN"
                        }
                    },
                    "nome": "NOME FIGLIO 2",
                    "sesso": "M"
                },
                "legameSoggetto": {
                    "codiceLegame": "3",
                    "dataDecorrenza": "2011-09-05",
                    "tipoLegame": "3"
                },
                "identificativi": {
                    "idANPR": "ID ANPR FIGLIO 2"
                }
            },
            {
                "generalita": {
                    "codiceFiscale": {
                        "codFiscale": "CODICE FISCALE PARTNER",
                        "validitaCF": "1"
                    },
                    "cognome": "COGNOME PARTNER",
                    "dataNascita": "1967-01-26",
                    "idSchedaSoggettoANPR": "12345",
                    "idSchedaSoggettoComune": {
                        "idSchedaSoggetto": "1234",
                        "idSchedaSoggettoComuneIstat": "12345"
                    },
                    "luogoNascita": {
                        "comune": {
                            "codiceIstat": "022117",
                            "nomeComune": "COMUNE PARTNER",
                            "siglaProvinciaIstat": "TN"
                        }
                    },
                    "nome": "NOME PARTNER",
                    "sesso": "M"
                },
                "legameSoggetto": {
                    "codiceLegame": "2",
                    "dataDecorrenza": "2011-09-05",
                    "tipoLegame": "3"
                },
                "identificativi": {
                    "idANPR": "ID ANPR PARTNER"
                }
            }
        ]
    },
    "idOperazioneANPR": "12345",
    "listaAnomalie": [
        {
            "codiceErroreAnomalia": "EN148",
            "testoErroreAnomalia": "Il D.M. Ministero dell'Interno 3 marzo 2023 (art.3 comma 3) prevede l'accesso ad ANPR esclusivamente con identificativo unico nazionale (ID ANPR), si  rappresenta pertanto l'urgenza di provvedere all'integrazione dei propri sistemi.",
            "tipoErroreAnomalia": "W"
        }
    ]
}`

	var statoFamiglia anpr.StatoFamiglia
	err := json.Unmarshal([]byte(jsonData), &statoFamiglia)
	if err != nil {
		t.Fatalf("Errore durante la decodifica del JSON: %v", err)
	}

	builder := &StatoFamigliaDataBuilder{
		StatoFamigliaResponse: statoFamiglia,
		FiscalCodeRichiedente: "CODICE FISCALE FIGLIO",
	}

	result, err := builder.buildDataResponseFormatOcGenitori()

	expectedParents := []anpr.Person{
		{
			BirthDate:  "01/03/1962",
			BirthPlace: "COMUNE INTESTATARIO",
			FamilyName: "COGNOME INTESTATARIO",
			GivenName:  "NOME INTESTATARIO",
			Gender:     "femmina",
			TaxID:      "CODICE FISCALE INTESTATARIO",
		},
		{
			BirthDate:  "26/01/1967",
			BirthPlace: "COMUNE PARTNER",
			FamilyName: "COGNOME PARTNER",
			GivenName:  "NOME PARTNER",
			Gender:     "maschio",
			TaxID:      "CODICE FISCALE PARTNER",
		},
	}

	if !reflect.DeepEqual(result.Parents, expectedParents) {
		t.Errorf("Expected %v, got %v", expectedParents, result.Parents)
	}

	if err != nil {
		t.Errorf("Expected no error but got: %v", err)
	}
}

func TestBuildDataResponseFormatOcGenitori_SpouseWithTwoParentsInLaw(t *testing.T) {
	jsonData := `{
		"listaSoggetti": {
			"datiSoggetto": [
				{
					"generalita": {
						"codiceFiscale": {
							"codFiscale": "CODICE FISCALE RICHIEDENTE",
							"validitaCF": "1"
						},
						"cognome": "COGNOME RICHIEDENTE",
						"dataNascita": "1980-05-01",
						"idSchedaSoggettoANPR": "123456",
						"luogoNascita": {
							"comune": {
								"codiceIstat": "123456",
								"nomeComune": "COMUNE RICHIEDENTE",
								"siglaProvinciaIstat": "PI"
							}
						},
						"nome": "NOME RICHIEDENTE",
						"sesso": "F"
					},
					"legameSoggetto": {
						"codiceLegame": "2", 
						"dataDecorrenza": "2006-10-07",
						"tipoLegame": "Spouse"
					},
					"identificativi": {
						"idANPR": "ID ANPR RICHIEDENTE"
					}
				},
				{
					"generalita": {
						"codiceFiscale": {
							"codFiscale": "CODICE FISCALE SUOCERO",
							"validitaCF": "1"
						},
						"cognome": "COGNOME SUOCERO",
						"dataNascita": "1950-01-01",
						"idSchedaSoggettoANPR": "654321",
						"luogoNascita": {
							"comune": {
								"codiceIstat": "654321",
								"nomeComune": "COMUNE SUOCERO",
								"siglaProvinciaIstat": "PI"
							}
						},
						"nome": "NOME SUOCERO",
						"sesso": "M"
					},
					"legameSoggetto": {
						"codiceLegame": "17", 
						"dataDecorrenza": "1950-01-01",
						"tipoLegame": "ParentInLaw"
					},
					"identificativi": {
						"idANPR": "ID ANPR SUOCERO"
					}
				},
				{
					"generalita": {
						"codiceFiscale": {
							"codFiscale": "CODICE FISCALE SUOCERA",
							"validitaCF": "1"
						},
						"cognome": "COGNOME SUOCERA",
						"dataNascita": "1952-03-15",
						"idSchedaSoggettoANPR": "789101",
						"luogoNascita": {
							"comune": {
								"codiceIstat": "789101",
								"nomeComune": "COMUNE SUOCERA",
								"siglaProvinciaIstat": "PI"
							}
						},
						"nome": "NOME SUOCERA",
						"sesso": "F"
					},
					"legameSoggetto": {
						"codiceLegame": "17", 
						"dataDecorrenza": "1952-03-15",
						"tipoLegame": "ParentInLaw"
					},
					"identificativi": {
						"idANPR": "ID ANPR SUOCERA"
					}
				}
			]
		},
		"idOperazioneANPR": "1111111"
	}`

	var statoFamiglia anpr.StatoFamiglia
	err := json.Unmarshal([]byte(jsonData), &statoFamiglia)
	if err != nil {
		t.Fatalf("Errore durante la decodifica del JSON: %v", err)
	}

	builder := &StatoFamigliaDataBuilder{
		StatoFamigliaResponse: statoFamiglia,
		FiscalCodeRichiedente: "CODICE FISCALE RICHIEDENTE",
	}

	result, err := builder.buildDataResponseFormatOcGenitori()

	expectedParentsInLaw := []anpr.Person{
		{
			BirthDate:  "01/01/1950",
			BirthPlace: "COMUNE SUOCERO",
			FamilyName: "COGNOME SUOCERO",
			GivenName:  "NOME SUOCERO",
			Gender:     "maschio",
			TaxID:      "CODICE FISCALE SUOCERO",
		},
		{
			BirthDate:  "15/03/1952",
			BirthPlace: "COMUNE SUOCERA",
			FamilyName: "COGNOME SUOCERA",
			GivenName:  "NOME SUOCERA",
			Gender:     "femmina",
			TaxID:      "CODICE FISCALE SUOCERA",
		},
	}

	if !reflect.DeepEqual(result.Parents, expectedParentsInLaw) {
		t.Errorf("Expected %v, got %v", expectedParentsInLaw, result.Parents)
	}

	if err != nil {
		t.Errorf("Expected no error but got: %v", err)
	}
}

func TestBuildDataResponseFormatOcConiuge_RichiedenteIsCardHolderWithSpouse(t *testing.T) {
	jsonData := `{
		"listaSoggetti": {
			"datiSoggetto": [
				{
					"generalita": {
						"codiceFiscale": {
							"codFiscale": "CODICE FISCALE RICHIEDENTE",
							"validitaCF": "1"
						},
						"cognome": "COGNOME RICHIEDENTE",
						"dataNascita": "1970-05-10",
						"idSchedaSoggettoANPR": "123456",
						"luogoNascita": {
							"comune": {
								"codiceIstat": "123456",
								"nomeComune": "COMUNE RICHIEDENTE",
								"siglaProvinciaIstat": "PI"
							}
						},
						"nome": "NOME RICHIEDENTE",
						"sesso": "M"
					},
					"legameSoggetto": {
						"codiceLegame": "1",
						"dataDecorrenza": "2000-01-01",
						"tipoLegame": "CardHolder"
					},
					"identificativi": {
						"idANPR": "ID ANPR RICHIEDENTE"
					}
				},
				{
					"generalita": {
						"codiceFiscale": {
							"codFiscale": "CODICE FISCALE CONIUGE",
							"validitaCF": "1"
						},
						"cognome": "COGNOME CONIUGE",
						"dataNascita": "1975-04-01",
						"idSchedaSoggettoANPR": "654321",
						"luogoNascita": {
							"comune": {
								"codiceIstat": "654321",
								"nomeComune": "COMUNE CONIUGE",
								"siglaProvinciaIstat": "PI"
							}
						},
						"nome": "NOME CONIUGE",
						"sesso": "F"
					},
					"legameSoggetto": {
						"codiceLegame": "2",
						"dataDecorrenza": "2000-01-01",
						"tipoLegame": "Spouse"
					},
					"identificativi": {
						"idANPR": "ID ANPR CONIUGE"
					}
				}
			]
		},
		"idOperazioneANPR": "1111111"
	}`

	var statoFamiglia anpr.StatoFamiglia
	err := json.Unmarshal([]byte(jsonData), &statoFamiglia)
	if err != nil {
		t.Fatalf("Errore durante la decodifica del JSON: %v", err)
	}

	builder := &StatoFamigliaDataBuilder{
		StatoFamigliaResponse: statoFamiglia,
		FiscalCodeRichiedente: "CODICE FISCALE RICHIEDENTE",
	}

	result, err := builder.buildDataResponseFormatOcConiuge()

	expectedSpouse := anpr.StatoFamigliaFormattedDataOcConiuge{
		BirthDate:  StringPtr("01/04/1975"),
		BirthPlace: StringPtr("COMUNE CONIUGE"),
		FamilyName: StringPtr("COGNOME CONIUGE"),
		GivenName:  StringPtr("NOME CONIUGE"),
		Gender:     StringPtr("femmina"),
		TaxID:      StringPtr("CODICE FISCALE CONIUGE"),
	}

	if !((result.BirthDate != nil && expectedSpouse.BirthDate != nil) && (*result.BirthDate == *expectedSpouse.BirthDate)) {
		t.Errorf("Expected BirthDate %v, got %v", *expectedSpouse.BirthDate, *result.BirthDate)
	}

	if !((result.BirthPlace != nil && expectedSpouse.BirthPlace != nil) && (*result.BirthPlace == *expectedSpouse.BirthPlace)) {
		t.Errorf("Expected BirthPlace %v, got %v", *expectedSpouse.BirthPlace, *result.BirthPlace)
	}

	if !((result.FamilyName != nil && expectedSpouse.FamilyName != nil) && (*result.FamilyName == *expectedSpouse.FamilyName)) {
		t.Errorf("Expected FamilyName %v, got %v", *expectedSpouse.FamilyName, *result.FamilyName)
	}

	if !((result.GivenName != nil && expectedSpouse.GivenName != nil) && (*result.GivenName == *expectedSpouse.GivenName)) {
		t.Errorf("Expected GivenName %v, got %v", *expectedSpouse.GivenName, *result.GivenName)
	}

	if !((result.Gender != nil && expectedSpouse.Gender != nil) && (*result.Gender == *expectedSpouse.Gender)) {
		t.Errorf("Expected Gender %v, got %v", *expectedSpouse.Gender, *result.Gender)
	}

	if !((result.TaxID != nil && expectedSpouse.TaxID != nil) && (*result.TaxID == *expectedSpouse.TaxID)) {
		t.Errorf("Expected TaxID %v, got %v", *expectedSpouse.TaxID, *result.TaxID)
	}

	if err != nil {
		t.Errorf("Expected no error but got: %v", err)
	}
}

func TestBuildDataResponseFormatOcConiuge_RichiedenteIsSpouseWithCardHolder(t *testing.T) {
	jsonData := `{
		"listaSoggetti": {
			"datiSoggetto": [
				{
					"generalita": {
						"codiceFiscale": {
							"codFiscale": "CODICE FISCALE RICHIEDENTE",
							"validitaCF": "1"
						},
						"cognome": "COGNOME RICHIEDENTE",
						"dataNascita": "1970-05-10",
						"idSchedaSoggettoANPR": "123456",
						"luogoNascita": {
							"comune": {
								"codiceIstat": "123456",
								"nomeComune": "COMUNE RICHIEDENTE",
								"siglaProvinciaIstat": "PI"
							}
						},
						"nome": "NOME RICHIEDENTE",
						"sesso": "M"
					},
					"legameSoggetto": {
						"codiceLegame": "2",
						"dataDecorrenza": "2000-01-01",
						"tipoLegame": "CardHolder"
					},
					"identificativi": {
						"idANPR": "ID ANPR RICHIEDENTE"
					}
				},
				{
					"generalita": {
						"codiceFiscale": {
							"codFiscale": "CODICE FISCALE CONIUGE",
							"validitaCF": "1"
						},
						"cognome": "COGNOME CONIUGE",
						"dataNascita": "1975-04-01",
						"idSchedaSoggettoANPR": "654321",
						"luogoNascita": {
							"comune": {
								"codiceIstat": "654321",
								"nomeComune": "COMUNE CONIUGE",
								"siglaProvinciaIstat": "PI"
							}
						},
						"nome": "NOME CONIUGE",
						"sesso": "F"
					},
					"legameSoggetto": {
						"codiceLegame": "1",
						"dataDecorrenza": "2000-01-01",
						"tipoLegame": "Spouse"
					},
					"identificativi": {
						"idANPR": "ID ANPR CONIUGE"
					}
				}
			]
		},
		"idOperazioneANPR": "1111111"
	}`

	var statoFamiglia anpr.StatoFamiglia
	err := json.Unmarshal([]byte(jsonData), &statoFamiglia)
	if err != nil {
		t.Fatalf("Errore durante la decodifica del JSON: %v", err)
	}

	builder := &StatoFamigliaDataBuilder{
		StatoFamigliaResponse: statoFamiglia,
		FiscalCodeRichiedente: "CODICE FISCALE RICHIEDENTE",
	}

	result, _ := builder.buildDataResponseFormatOcConiuge()

	expectedSpouse := anpr.StatoFamigliaFormattedDataOcConiuge{
		BirthDate:  StringPtr("01/04/1975"),
		BirthPlace: StringPtr("COMUNE CONIUGE"),
		FamilyName: StringPtr("COGNOME CONIUGE"),
		GivenName:  StringPtr("NOME CONIUGE"),
		Gender:     StringPtr("femmina"),
		TaxID:      StringPtr("CODICE FISCALE CONIUGE"),
	}

	if !((result.BirthDate != nil && expectedSpouse.BirthDate != nil) && (*result.BirthDate == *expectedSpouse.BirthDate)) {
		t.Errorf("Expected BirthDate %v, got %v", *expectedSpouse.BirthDate, *result.BirthDate)
	}

	if !((result.BirthPlace != nil && expectedSpouse.BirthPlace != nil) && (*result.BirthPlace == *expectedSpouse.BirthPlace)) {
		t.Errorf("Expected BirthPlace %v, got %v", *expectedSpouse.BirthPlace, *result.BirthPlace)
	}

	if !((result.FamilyName != nil && expectedSpouse.FamilyName != nil) && (*result.FamilyName == *expectedSpouse.FamilyName)) {
		t.Errorf("Expected FamilyName %v, got %v", *expectedSpouse.FamilyName, *result.FamilyName)
	}

	if !((result.GivenName != nil && expectedSpouse.GivenName != nil) && (*result.GivenName == *expectedSpouse.GivenName)) {
		t.Errorf("Expected GivenName %v, got %v", *expectedSpouse.GivenName, *result.GivenName)
	}

	if !((result.Gender != nil && expectedSpouse.Gender != nil) && (*result.Gender == *expectedSpouse.Gender)) {
		t.Errorf("Expected Gender %v, got %v", *expectedSpouse.Gender, *result.Gender)
	}

	if !((result.TaxID != nil && expectedSpouse.TaxID != nil) && (*result.TaxID == *expectedSpouse.TaxID)) {
		t.Errorf("Expected TaxID %v, got %v", *expectedSpouse.TaxID, *result.TaxID)
	}

}

func TestBuildDataResponseFormatOcConiuge_RichiedenteIsParentWithCardHolder(t *testing.T) {
	jsonData := `{
		"listaSoggetti": {
			"datiSoggetto": [
				{
					"generalita": {
						"codiceFiscale": {
							"codFiscale": "CODICE FISCALE RICHIEDENTE",
							"validitaCF": "1"
						},
						"cognome": "COGNOME RICHIEDENTE",
						"dataNascita": "1970-05-10",
						"idSchedaSoggettoANPR": "123456",
						"luogoNascita": {
							"comune": {
								"codiceIstat": "123456",
								"nomeComune": "COMUNE RICHIEDENTE",
								"siglaProvinciaIstat": "PI"
							}
						},
						"nome": "NOME RICHIEDENTE",
						"sesso": "M"
					},
					"legameSoggetto": {
						"codiceLegame": "6",
						"dataDecorrenza": "2000-01-01",
						"tipoLegame": "CardHolder"
					},
					"identificativi": {
						"idANPR": "ID ANPR RICHIEDENTE"
					}
				},
				{
					"generalita": {
						"codiceFiscale": {
							"codFiscale": "CODICE FISCALE CONIUGE",
							"validitaCF": "1"
						},
						"cognome": "COGNOME CONIUGE",
						"dataNascita": "1975-04-01",
						"idSchedaSoggettoANPR": "654321",
						"luogoNascita": {
							"comune": {
								"codiceIstat": "654321",
								"nomeComune": "COMUNE CONIUGE",
								"siglaProvinciaIstat": "PI"
							}
						},
						"nome": "NOME CONIUGE",
						"sesso": "F"
					},
					"legameSoggetto": {
						"codiceLegame": "6",
						"dataDecorrenza": "2000-01-01",
						"tipoLegame": "Spouse"
					},
					"identificativi": {
						"idANPR": "ID ANPR CONIUGE"
					}
				},
                 {
					"generalita": {
						"codiceFiscale": {
							"codFiscale": "CODICE FISCALE INTESTARIO",
							"validitaCF": "1"
						},
						"cognome": "COGNOME INTESTARIO",
						"dataNascita": "1998-04-01",
						"idSchedaSoggettoANPR": "654321",
						"luogoNascita": {
							"comune": {
								"codiceIstat": "654321",
								"nomeComune": "COMUNE INTESTARIO",
								"siglaProvinciaIstat": "PI"
							}
						},
						"nome": "NOME INTESTARIO",
						"sesso": "F"
					},
					"legameSoggetto": {
						"codiceLegame": "1",
						"dataDecorrenza": "2000-01-01",
						"tipoLegame": "intestatario"
					},
					"identificativi": {
						"idANPR": "ID ANPR INTESTARIO"
					}
				}
			],
		"idOperazioneANPR": "1111111"
	}}`

	var statoFamiglia anpr.StatoFamiglia
	err := json.Unmarshal([]byte(jsonData), &statoFamiglia)
	if err != nil {
		t.Fatalf("Errore durante la decodifica del JSON: %v", err)
	}

	builder := &StatoFamigliaDataBuilder{
		StatoFamigliaResponse: statoFamiglia,
		FiscalCodeRichiedente: "CODICE FISCALE RICHIEDENTE",
	}

	result, _ := builder.buildDataResponseFormatOcConiuge()

	expectedSpouse := anpr.StatoFamigliaFormattedDataOcConiuge{
		BirthDate:  StringPtr("01/04/1975"),
		BirthPlace: StringPtr("COMUNE CONIUGE"),
		FamilyName: StringPtr("COGNOME CONIUGE"),
		GivenName:  StringPtr("NOME CONIUGE"),
		Gender:     StringPtr("femmina"),
		TaxID:      StringPtr("CODICE FISCALE CONIUGE"),
	}
	if !((result.BirthDate != nil && expectedSpouse.BirthDate != nil) && (*result.BirthDate == *expectedSpouse.BirthDate)) {
		t.Errorf("Expected BirthDate %v, got %v", *expectedSpouse.BirthDate, *result.BirthDate)
	}

	if !((result.BirthPlace != nil && expectedSpouse.BirthPlace != nil) && (*result.BirthPlace == *expectedSpouse.BirthPlace)) {
		t.Errorf("Expected BirthPlace %v, got %v", *expectedSpouse.BirthPlace, *result.BirthPlace)
	}

	if !((result.FamilyName != nil && expectedSpouse.FamilyName != nil) && (*result.FamilyName == *expectedSpouse.FamilyName)) {
		t.Errorf("Expected FamilyName %v, got %v", *expectedSpouse.FamilyName, *result.FamilyName)
	}

	if !((result.GivenName != nil && expectedSpouse.GivenName != nil) && (*result.GivenName == *expectedSpouse.GivenName)) {
		t.Errorf("Expected GivenName %v, got %v", *expectedSpouse.GivenName, *result.GivenName)
	}

	if !((result.Gender != nil && expectedSpouse.Gender != nil) && (*result.Gender == *expectedSpouse.Gender)) {
		t.Errorf("Expected Gender %v, got %v", *expectedSpouse.Gender, *result.Gender)
	}

	if !((result.TaxID != nil && expectedSpouse.TaxID != nil) && (*result.TaxID == *expectedSpouse.TaxID)) {
		t.Errorf("Expected TaxID %v, got %v", *expectedSpouse.TaxID, *result.TaxID)
	}
}

func TestBuildDataResponseFormatOcNucleoFamiliare(t *testing.T) {
	jsonData := `{
    "listaSoggetti": {
        "datiSoggetto": [
            {
                "generalita": {
                    "codiceFiscale": {
                        "codFiscale": "CODICE FISCALE RICHIEDENTE",
                        "validitaCF": "1"
                    },
                    "cognome": "COGNOME RICHIEDENTE",
                    "dataNascita": "1976-09-01",
                    "idSchedaSoggettoANPR": "111111",
                    "luogoNascita": {
                        "comune": {
                            "codiceIstat": "111111",
                            "nomeComune": "COMUNE RICHIEDENTE",
                            "siglaProvinciaIstat": "PI"
                        }
                    },
                    "nome": "NOME RICHIEDENTE",
                    "sesso": "M"
                },
                "legameSoggetto": {
                    "codiceLegame": "2",
                    "dataDecorrenza": "2006-10-07",
                    "tipoLegame": "3"
                },
                "identificativi": {
                    "idANPR": "ID ANPR RICHIEDENTE"
                }
            },
            {
                "generalita": {
                    "codiceFiscale": {
                        "codFiscale": "FIGLIO RICHIEDENTE",
                        "validitaCF": "1"
                    },
                    "cognome": "COGNOME FIGLIO",
                    "dataNascita": "2009-07-30",
                    "idSchedaSoggettoANPR": "11111",
                    "idSchedaSoggettoComune": {
                        "idSchedaSoggetto": "1111",
                        "idSchedaSoggettoComuneIstat": "111"
                    },
                    "luogoNascita": {
                        "comune": {
                            "codiceIstat": "11",
                            "nomeComune": "COMUNE FIGLIO",
                            "siglaProvinciaIstat": "PI"
                        }
                    },
                    "nome": "NOME FIGLIO",
                    "sesso": "M"
                },
                "legameSoggetto": {
                    "codiceLegame": "3",
                    "dataDecorrenza": "2009-07-30",
                    "tipoLegame": "3"
                },
                "identificativi": {
                    "idANPR": "111111"
                }
            },
            {
                "generalita": {
                    "codiceFiscale": {
                        "codFiscale": "CODICE FISCALE CONIUGE",
                        "validitaCF": "1"
                    },
                    "cognome": "COGNOME CONIUGE",
                    "dataNascita": "1976-11-18",
                    "idSchedaSoggettoANPR": "111111",
                    "idSchedaSoggettoComune": {
                        "idSchedaSoggetto": "1111",
                        "idSchedaSoggettoComuneIstat": "1111"
                    },
                    "luogoNascita": {
                        "comune": {
                            "codiceIstat": "1111",
                            "nomeComune": "COMUNE CONIUGE",
                            "siglaProvinciaIstat": "PI"
                        }
                    },
                    "nome": "NOME CONIUGE",
                    "sesso": "F"
                },
                "legameSoggetto": {
                    "codiceLegame": "1",
                    "dataDecorrenza": "2004-08-28",
                    "tipoLegame": "3"
                },
                "identificativi": {
                    "idANPR": "11111"
                }
            },
            {
                "generalita": {
                    "codiceFiscale": {
                        "codFiscale": "FIGLIA RICHIEDENTE",
                        "validitaCF": "1"
                    },
                    "cognome": "COGNOME FIGLIA",
                    "dataNascita": "2014-08-05",
                    "idSchedaSoggettoANPR": "11111",
                    "idSchedaSoggettoComune": {
                        "idSchedaSoggetto": "11111",
                        "idSchedaSoggettoComuneIstat": "11111"
                    },
                    "luogoNascita": {
                        "comune": {
                            "codiceIstat": "1111",
                            "nomeComune": "COMUNE FIGLIA",
                            "siglaProvinciaIstat": "PI"
                        }
                    },
                    "nome": "NOME FIGLIA",
                    "sesso": "F"
                },
                "legameSoggetto": {
                    "codiceLegame": "3",
                    "dataDecorrenza": "2014-08-05",
                    "tipoLegame": "3"
                },
                "identificativi": {
                    "idANPR": "1111111"
                }
            }
        ]
    },
    "idOperazioneANPR": "1111111",
    "listaAnomalie": [
        {
            "codiceErroreAnomalia": "EN148",
            "testoErroreAnomalia": "Il D.M. Ministero dell'Interno 3 marzo 2023 (art.3 comma 3) prevede l'accesso ad ANPR esclusivamente con identificativo unico nazionale (ID ANPR), si  rappresenta pertanto l'urgenza di provvedere all'integrazione dei propri sistemi.",
            "tipoErroreAnomalia": "W"
        }
    ]
}`

	var statoFamiglia anpr.StatoFamiglia
	err := json.Unmarshal([]byte(jsonData), &statoFamiglia)
	if err != nil {
		t.Fatalf("Errore durante la decodifica del JSON: %v", err)
	}

	builder := &StatoFamigliaDataBuilder{
		StatoFamigliaResponse: statoFamiglia,
		FiscalCodeRichiedente: "CODICE FISCALE RICHIEDENTE",
	}

	result, _ := builder.buildDataResponseFormatOcNucleoFamiliare()

	expectedMember := []anpr.FamilyMember{
		{
			BirthDate:  "01/09/1976",
			RelatedTo:  "marito/moglie",
			FamilyName: "COGNOME RICHIEDENTE",
			GivenName:  "NOME RICHIEDENTE",
			TaxID:      "CODICE FISCALE RICHIEDENTE",
		},
		{
			BirthDate:  "30/07/2009",
			RelatedTo:  "figlio/figlia",
			FamilyName: "COGNOME FIGLIO",
			GivenName:  "NOME FIGLIO",
			TaxID:      "FIGLIO RICHIEDENTE",
		},
		{
			BirthDate:  "18/11/1976",
			RelatedTo:  "intestatario",
			FamilyName: "COGNOME CONIUGE",
			GivenName:  "NOME CONIUGE",
			TaxID:      "CODICE FISCALE CONIUGE",
		},
		{
			BirthDate:  "05/08/2014",
			RelatedTo:  "figlio/figlia",
			FamilyName: "COGNOME FIGLIA",
			GivenName:  "NOME FIGLIA",
			TaxID:      "FIGLIA RICHIEDENTE",
		},
	}

	if !reflect.DeepEqual(result.FamilyMember, expectedMember) {
		t.Errorf("Expected %v, got %v", expectedMember, result.FamilyMember)
	}
}

package server

import (
	"context"
	"errors"

	"github.com/gofrs/uuid"
	"github.com/swaggest/usecase"
	"github.com/swaggest/usecase/status"
	"gitlab.com/opencontent/stanza-del-cittadino/pdnd-connector/server/models"
)

type configPostInput struct {
	Token      string `header:"Authorization" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	ClientId   string `json:"client_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	EserviceId string `json:"eservice_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	PurposeId  string `json:"purpose_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	TenantId   string `path:"tenant_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	IsActive   *bool  `json:"is_active" description:"..." example:"false"`
}

func CreateConfig(sctx *ServerContext) usecase.Interactor {
	servicesSync := sctx.ServicesSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input configPostInput, output *models.Config) error {
		servicesSync.Lock()
		defer servicesSync.Unlock()

		err := ValidateAdminToken(sctx, input.Token, input.TenantId)
		if err != nil && err.Error() == "empty token" {
			MetricsAdminTokenUnauthenticatedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			return status.Wrap(err, status.Unauthenticated)
		}
		if err != nil {
			MetricsAdminTokenPermissionDeniedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			sctx.LogHttpWarning().Str("http request POST /tenants/"+input.TenantId+"/configs", "").Str("result", "403")
			return status.Wrap(err, status.PermissionDenied)
		}

		if err := ValidateConfigPostInput(sctx, input); err != nil {
			return status.Wrap(err, status.InvalidArgument)
		}

		id, err := uuid.NewV4()
		if err != nil {
			return status.Wrap(err, status.InvalidArgument)
		}

		romeTime := GetCurrentDateTime()
		output.CreatedAt = romeTime
		output.UpdatedAt = romeTime
		output.ID = id.String()
		output.ClientID = input.ClientId
		output.EserviceID = input.EserviceId
		output.PurposeID = input.PurposeId
		output.TenantID = input.TenantId
		output.IsActive = *input.IsActive

		err = buildCallUrl(sctx, output)
		if err != nil {
			return status.InvalidArgument
		}

		err = StoreConfig(sctx, output)
		if err != nil && err.Error() == "invalid data" {
			return status.InvalidArgument
		}
		if err != nil {
			return errors.New("failed to store config:  " + err.Error())
		}

		return nil
	})

	uc.SetTitle("Save Service Configuration")
	uc.SetDescription("...")
	uc.SetTags("Configs")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}

type ConfigInput struct {
	ConfigId string `path:"config_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	TenantId string `path:"tenant_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	Token    string `header:"Authorization" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
}

func GetconfigByID(sctx *ServerContext) usecase.Interactor {
	servicesSync := sctx.ServicesSync()
	configsCache := sctx.ConfigsCache()

	uc := usecase.NewInteractor(func(ctx context.Context, input ConfigInput, output *models.Config) error {
		servicesSync.RLock()
		defer servicesSync.RUnlock()

		/* 		err := ValidateAdminUserToken(sctx, input.Token, input.TenantId)
		   		if err != nil && err.Error() == "empty token" {
		   			MetricsAdminTokenUnauthenticatedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
		   			sctx.LogHttpWarning().Str("http request GET /tenants/"+input.TenantId+"/configs/"+input.ConfigId, "").Str("result", "401")
		   			return status.Wrap(err, status.Unauthenticated)
		   		}
		   		if err != nil {
		   			MetricsAdminTokenPermissionDeniedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
		   			sctx.LogHttpWarning().Str("http request GET /tenants/"+input.TenantId+"/configs/"+input.ConfigId, "").Str("result", "403")
		   			return status.Wrap(err, status.PermissionDenied)
		   		} */

		err := ValidateGetConfigRequestInput(sctx, input)
		if err != nil {
			return status.Wrap(err, status.InvalidArgument)
		}
		config, err := configsCache.Get(ctx, input.ConfigId)
		if err != nil && err.Error() == "value not found in store" {
			return status.NotFound
		}
		if err != nil {
			sctx.LogHttpError().Stack().Err(err).Msg("get service by id error for: " + input.ConfigId)
			return status.Internal
		}
		if config.TenantID != input.TenantId {
			return status.NotFound
		}

		*output = *config

		return nil
	})

	uc.SetTitle("Get Config")
	uc.SetDescription("Get config of specific tenant by id")
	uc.SetTags("Configs")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}

type ConfigsUpdateInput struct {
	Token      string `header:"Authorization" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	ID         string `path:"config_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	ClientID   string `json:"client_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	EserviceID string `json:"eservice_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	PurposeID  string `json:"purpose_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	TenantID   string `path:"tenant_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	IsActive   *bool  `json:"is_active" description:"..." example:"false"`
}

func UpdateConfig(sctx *ServerContext) usecase.Interactor {
	servicesSync := sctx.ServicesSync()
	configsCache := sctx.ConfigsCache()

	uc := usecase.NewInteractor(func(ctx context.Context, input ConfigsUpdateInput, output *models.Config) error {
		servicesSync.Lock()
		defer servicesSync.Unlock()

		err := ValidateAdminToken(sctx, input.Token, input.TenantID)
		if err != nil && err.Error() == "empty token" {
			MetricsAdminTokenUnauthenticatedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			sctx.LogHttpWarning().Str("http request PUT /tenants/"+input.TenantID+"/configs/"+input.ID, "").Str("result", "401")
			return status.Wrap(err, status.Unauthenticated)
		}
		if err != nil {
			sctx.LogHttpWarning().Str("http request PUT /tenants/"+input.TenantID+"/configs/"+input.ID, "").Str("result", "403")
			MetricsAdminTokenPermissionDeniedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			return status.Wrap(err, status.PermissionDenied)
		}

		if err := ValidateConfigPutInput(sctx, input); err != nil {
			return status.Wrap(err, status.InvalidArgument)
		}

		output.ID = input.ID
		output.ClientID = input.ClientID
		output.TenantID = input.TenantID
		output.EserviceID = input.EserviceID
		output.PurposeID = input.PurposeID
		output.IsActive = *input.IsActive
		romeTime := GetCurrentDateTime()
		output.UpdatedAt = romeTime

		config, err := configsCache.Get(ctx, input.ID)
		if err != nil && err.Error() == "value not found in store" {
			output.CreatedAt = romeTime

			err = buildCallUrl(sctx, output)
			if err != nil {
				return status.Wrap(err, status.InvalidArgument)
			}

			err = StoreConfig(sctx, output)
			if err != nil && err.Error() == "invalid data" {
				return status.InvalidArgument
			}
			if err != nil {
				return err
			}
			return nil
		}
		if err != nil {
			sctx.LogHttpError().Stack().Err(err).Msg("get config by id error for: " + input.ID)
			return status.Internal
		}

		output.CreatedAt = config.CreatedAt
		output.CallUrl = config.CallUrl
		err = StoreConfig(sctx, output)
		if err != nil && err.Error() == "invalid data" {
			return status.InvalidArgument
		}
		if err != nil {
			return err
		}
		return nil
	})

	uc.SetTitle("Update Config")
	uc.SetDescription("Update config of specific tenant")
	uc.SetTags("Configs")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}

type ConfigsPatchInput struct {
	Token      string `header:"Authorization" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	ID         string `path:"config_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	ClientID   string `json:"client_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	EserviceID string `json:"eservice_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	PurposeID  string `json:"purpose_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	TenantID   string `path:"tenant_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	IsActive   *bool  `json:"is_active" description:"..." example:"false"`
}

func PatchConfig(sctx *ServerContext) usecase.Interactor {
	servicesSync := sctx.ServicesSync()
	configsCache := sctx.ConfigsCache()

	uc := usecase.NewInteractor(func(ctx context.Context, input ConfigsPatchInput, output *models.Config) error {
		servicesSync.Lock()
		defer servicesSync.Unlock()

		err := ValidateAdminToken(sctx, input.Token, input.TenantID)
		if err != nil && err.Error() == "empty token" {
			MetricsAdminTokenUnauthenticatedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			sctx.LogHttpWarning().Str("http request PATCH /tenants/"+input.TenantID+"/configs/"+input.ID, "").Str("result", "401")
			return status.Wrap(err, status.Unauthenticated)
		}
		if err != nil {
			sctx.LogHttpWarning().Str("http request PATCH /tenants/"+input.TenantID+"/configs/"+input.ID, "").Str("result", "403")
			MetricsAdminTokenPermissionDeniedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			return status.Wrap(err, status.PermissionDenied)
		}

		if err := validateConfigPatchPDND(sctx, input); err != nil {
			return status.Wrap(err, status.InvalidArgument)
		}

		config, err := configsCache.Get(ctx, input.ID)
		if err != nil && err.Error() == "value not found in store" {
			return status.NotFound
		}
		if err != nil {
			sctx.LogHttpError().Stack().Err(err).Msg("get config by id error for: " + input.ID)
			return status.Internal
		}
		if config.TenantID != input.TenantID {
			return status.NotFound
		}

		if input.ClientID != "" {
			config.ClientID = input.ClientID
		}
		if input.EserviceID != "" {
			config.EserviceID = input.EserviceID
		}
		if input.ID != "" {
			config.ID = input.ID
		}
		if input.PurposeID != "" {
			config.PurposeID = input.PurposeID
		}
		if input.TenantID != "" {
			config.TenantID = input.TenantID
		}
		if input.IsActive != nil {
			config.IsActive = *input.IsActive
		}
		romeTime := GetCurrentDateTime()
		config.UpdatedAt = romeTime
		*output = *config
		err = StoreConfig(sctx, config)
		if err != nil && err.Error() == "invalid data" {
			return status.InvalidArgument
		}
		if err != nil {
			return err
		}

		return nil
	})

	uc.SetTitle("Patch Config")
	uc.SetDescription("Patch config of specific tenant")
	uc.SetTags("Configs")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}

func DeleteConfig(sctx *ServerContext) usecase.Interactor {
	servicesSync := sctx.ServicesSync()
	configsCache := sctx.ConfigsCache()

	uc := usecase.NewInteractor(func(ctx context.Context, input ConfigInput, _ *struct{}) error {
		servicesSync.Lock()
		defer servicesSync.Unlock()

		err := ValidateAdminToken(sctx, input.Token, input.TenantId)
		if err != nil && err.Error() == "empty token" {
			MetricsAdminTokenUnauthenticatedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			sctx.LogHttpWarning().Str("http request DELETE /tenants/"+input.TenantId+"/configs/"+input.ConfigId, "").Str("result", "401")
			return status.Wrap(err, status.Unauthenticated)
		}
		if err != nil {
			MetricsAdminTokenPermissionDeniedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			sctx.LogHttpWarning().Str("http request DELETE /tenants/"+input.TenantId+"/configs/"+input.ConfigId, "").Str("result", "403")
			return status.Wrap(err, status.PermissionDenied)
		}

		err = validateConfigDeletePDND(sctx, input)
		if err != nil {
			return status.Wrap(err, status.InvalidArgument)
		}
		config, err := configsCache.Get(ctx, input.ConfigId)
		if err != nil && err.Error() == "value not found in store" {
			return status.NotFound
		}
		if err != nil {
			sctx.LogHttpError().Stack().Err(err).Msg("get client by id error for: " + input.ConfigId)
			return status.Internal
		}
		if config.TenantID != input.TenantId {
			return status.NotFound
		}
		err = TrashConfig(sctx, config.ID)
		if err != nil && err.Error() == "invalid data" {
			return status.InvalidArgument
		}
		if err != nil {
			return err
		}
		return nil
	})

	uc.SetTitle("Disable Config")
	uc.SetDescription("Soft deleting config of specific tenant")
	uc.SetTags("Configs")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}

func OptionsConfigById(sctx *ServerContext) usecase.Interactor {
	uc := usecase.NewInteractor(func(ctx context.Context, input ConfigInput, _ *struct{}) error {

		err := ValidateAdminToken(sctx, input.Token, input.TenantId)
		if err != nil && err.Error() == "empty token" {
			MetricsAdminTokenUnauthenticatedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			sctx.LogHttpWarning().Str("http request OPTION /tenants/"+input.TenantId+"/configs/"+input.ConfigId, "").Str("result", "401")
			return status.Wrap(err, status.Unauthenticated)
		}
		if err != nil {
			MetricsAdminTokenPermissionDeniedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			sctx.LogHttpWarning().Str("http request OPTION /tenants/"+input.TenantId+"/configs/"+input.ConfigId, "").Str("result", "403")
			return status.Wrap(err, status.PermissionDenied)
		}

		err = ValidateGetConfigRequestInput(sctx, input)
		if err != nil {
			return status.Wrap(err, status.InvalidArgument)

		}
		return nil
	})

	uc.SetTitle("Options Config")
	uc.SetDescription("Get available operations for the Config resource.")
	uc.SetTags("Configs")

	return uc
}

type configOptionInput struct {
	Token    string `header:"Authorization" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	TenantID string `path:"tenant_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
}

func OptionsConfig(sctx *ServerContext) usecase.Interactor {
	// Create a new Interactor to handle OPTIONS requests
	uc := usecase.NewInteractor(func(ctx context.Context, input configOptionInput, _ *struct{}) error {

		err := ValidateAdminToken(sctx, input.Token, input.TenantID)
		if err != nil && err.Error() == "empty token" {
			MetricsAdminTokenUnauthenticatedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			sctx.LogHttpWarning().Str("http request DELETE /tenants/"+input.TenantID+"/configs/", "").Str("result", "401")
			return status.Wrap(err, status.Unauthenticated)
		}
		if err != nil {
			MetricsAdminTokenPermissionDeniedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
			sctx.LogHttpWarning().Str("http request DELETE /tenants/"+input.TenantID+"/configs/", "").Str("result", "403")
			return status.Wrap(err, status.PermissionDenied)
		}

		err = validateOptionPDND(sctx, input)
		if err != nil {
			return status.Wrap(err, status.InvalidArgument)

		}
		return nil
	})

	// Set the title, description, and tags for the OPTIONS operation
	uc.SetTitle("Options Config")
	uc.SetDescription("Get available operations for the Config resource.")
	uc.SetTags("Configs")

	// Set the Allow header to indicate the supported HTTP methods
	//uc.SetHeader("Allow", "GET, POST, PATCH, PUT, DELETE")
	return uc
}

func ValidateGetConfigRequestInput(sctx *ServerContext, a ConfigInput) error {

	_, err := uuid.FromString(a.ConfigId)
	if err != nil {
		return err
	}
	err = ValidateTenantId(sctx, a.TenantId)
	if err != nil {
		return err
	}
	return nil
}

func ValidateConfigPostInput(sctx *ServerContext, a configPostInput) error {
	_, err := uuid.FromString(a.ClientId)
	if err != nil {
		return errors.New("client id " + err.Error())
	}
	_, err = GetClientPdndByClientId(sctx, a.ClientId)
	if err != nil && err.Error() == "not found" {
		return errors.New("client does not belong to any client")
	}
	if err != nil {
		sctx.LogHttpError().Stack().Err(err).Msg("error checking if client exists. id: " + a.ClientId)
		return status.Internal
	}
	_, err = uuid.FromString(a.EserviceId)
	if err != nil {
		return errors.New("eservice_id " + err.Error())
	}
	_, err = uuid.FromString(a.PurposeId)
	if err != nil {
		return errors.New("purpose_id " + err.Error())
	}
	err = ValidateTenantId(sctx, a.TenantId)
	if err != nil {
		return err
	}
	if a.IsActive == nil {
		return errors.New("is_active can't be null ")
	}
	return nil
}

func ValidateConfigPutInput(sctx *ServerContext, a ConfigsUpdateInput) error {
	_, err := uuid.FromString(a.ID)
	if err != nil {
		return errors.New("config id " + err.Error())
	}
	_, err = uuid.FromString(a.ClientID)
	if err != nil {
		return errors.New("client id " + err.Error())
	}
	_, err = GetClientPdndByClientId(sctx, a.ClientID)
	if err != nil && err.Error() == "not found" {
		return errors.New("client id does not belog to any client")
	}
	if err != nil {
		sctx.LogHttpError().Stack().Err(err).Msg("error checking if client exists. id: " + a.ClientID)
		return status.Internal
	}
	_, err = uuid.FromString(a.EserviceID)
	if err != nil {
		return errors.New("eservice_id " + err.Error())
	}
	_, err = uuid.FromString(a.PurposeID)
	if err != nil {
		return errors.New("purpose_id " + err.Error())
	}
	err = ValidateTenantId(sctx, a.TenantID)
	if err != nil {
		return err
	}
	if a.IsActive == nil {
		return errors.New("is_active can't be null ")
	}
	return nil
}

func validateConfigPatchPDND(sctx *ServerContext, config ConfigsPatchInput) error {
	err := ValidateTenantId(sctx, config.TenantID)
	if err != nil && config.ID != "" {
		return err
	}

	_, err = uuid.FromString(config.ID)
	if err != nil && config.ID != "" {
		return errors.New("config_id invalid uuid")
	}
	_, err = uuid.FromString(config.EserviceID)
	if err != nil && config.EserviceID != "" {
		return errors.New("eservice_id invalid uuid")
	}
	if config.ClientID != "" {
		_, err = uuid.FromString(config.ClientID)
		if err != nil {
			return errors.New("client_id invalid uuid")
		}
		_, err = GetClientPdndByClientId(sctx, config.ClientID)
		if err != nil && err.Error() == "not found" {
			return errors.New("client_id does not belog to any client")
		}
		if err != nil {
			sctx.LogHttpError().Stack().Err(err).Msg("error checking if client exists. id: " + config.ClientID)
			return status.Internal
		}
	}

	_, err = uuid.FromString(config.PurposeID)
	if err != nil && config.PurposeID != "" {
		return errors.New("purpose_id id invalid uuid")
	}
	return nil
}
func validateConfigDeletePDND(sctx *ServerContext, config ConfigInput) error {
	err := ValidateTenantId(sctx, config.TenantId)
	if err != nil {
		return err
	}

	_, err = uuid.FromString(config.ConfigId)
	if err != nil {
		return errors.New("config ID invalid uuid")
	}
	return nil
}

func validateOptionPDND(sctx *ServerContext, config configOptionInput) error {
	err := ValidateTenantId(sctx, config.TenantID)
	if err != nil {
		return err
	}
	return nil
}

func buildCallUrl(sctx *ServerContext, input *models.Config) error {
	eServiceList := sctx.eServiceList
	for _, eservice := range eServiceList.Data {
		if eservice.ID == input.EserviceID {
			eServiceSlug := eservice.Slug
			input.CallUrl = sctx.serverConfig.HttpExternalBasePath + "/" + eServiceSlug + "?config_id=" + input.ID
			return nil
		}
	}
	return errors.New("e-service not found for id :" + input.EserviceID)
}

type GetConfigsByTenantIdInput struct {
	TenantId string `path:"tenant_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	Limit    int    `query:"limit" description:"The limit parameter specifies the maximum number of items to be returned in a single page or request"`
	Sort     string `query:"sort" description:"..."`
	Offset   int    `query:"offset" description:" the starting point or the index from which the data should be retrieved" example:"6"`
	Token    string `header:"Authorization" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
}

func GetConfigsListByTenantId(sctx *ServerContext) usecase.Interactor {
	servicesSync := sctx.ServicesSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input GetConfigsByTenantIdInput, output *models.ConfigListResponse) error {
		servicesSync.RLock()
		defer servicesSync.RUnlock()

		/* 		err := ValidateAdminUserToken(sctx, input.Token, input.TenantId)
		   		if err != nil && err.Error() == "empty token" {
		   			MetricsAdminTokenUnauthenticatedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
		   			sctx.LogHttpWarning().Str("http request GET /tenants/"+input.TenantId+"/configs/", "").Str("result", "401")
		   			return status.Wrap(err, status.Unauthenticated)
		   		}
		   		if err != nil {
		   			MetricsAdminTokenPermissionDeniedRequests.WithLabelValues(sctx.serverConfig.Environment, APPNAME).Inc()
		   			sctx.LogHttpWarning().Str("http request GET /tenants/"+input.TenantId+"/configs/", "").Str("result", "403")
		   			return status.Wrap(err, status.PermissionDenied)
		   		} */

		err := ValidateTenantId(sctx, input.TenantId)
		if err != nil {
			return status.Wrap(err, status.InvalidArgument)
		}

		httpFlowConfigsList := &HttpFlowConfigsList{
			Request: input,
			Sctx:    sctx,
		}

		if !httpFlowConfigsList.Exec() {
			if httpFlowConfigsList.Err != nil {
				sctx.LogHttpError().Str("http request ", "/tenants/"+input.TenantId+"/configs").Str("result", "discarded").
					Str("flow", httpFlowConfigsList.Name).
					Stack().Err(httpFlowConfigsList.Err).
					Msg(httpFlowConfigsList.Msg)
			}
		}
		if httpFlowConfigsList.Err != nil && httpFlowConfigsList.Msg == "not found" {
			return status.Wrap(httpFlowConfigsList.Err, status.NotFound)
		}
		if httpFlowConfigsList.Err != nil && httpFlowConfigsList.Msg == "internal error" {
			return status.Wrap(httpFlowConfigsList.Err, status.Internal)
		}

		*output = httpFlowConfigsList.Response
		return nil
	})

	uc.SetTitle("Get Config list of specific tenant")
	uc.SetDescription("...")
	uc.SetTags("Configs")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}

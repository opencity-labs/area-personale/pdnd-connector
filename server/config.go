package server

import (
	"context"
	"time"

	"github.com/sethvargo/go-envconfig"
)

type ServerConfig struct {
	// Proxy config
	Environment string `env:"ENVIRONMENT,default=local"`
	Debug       bool   `env:"DEBUG,default=false"`
	DefaultInfo bool   `env:"HTTP_DEFAULT_INFO,default=false"`
	// Sentry config
	SentryEnabled bool   `env:"SENTRY_ENABLED,default=true"`
	SentryToken   string `env:"SENTRY_TOKEN,default=https://yourendpoint"`
	//End point for jwtk sdc
	SdcJWKSPublicKeyEnpoint string `env:"SDC_PUBLIC_KEY_ENDPOINT,default=https://your_endpoint_jwks"`
	// HTTP config
	HttpBind             string `env:"HTTP_BIND,default=0.0.0.0"`
	HttpPort             string `env:"HTTP_PORT,default=8000"`
	HttpExternalBasePath string `env:"HTTP_EXTERNAL_BASEPATH,default=0.0.0.0:8000"`
	// Cache config
	CacheExpiration time.Duration `env:"CACHE_EXPIRATION,default=5m"`
	CacheEviction   time.Duration `env:"CACHE_EVICTION,default=10m"`
	// Storage config
	StorageType     string `env:"STORAGE_TYPE,default=s3"` // s3,local,azure
	StorageBucket   string `env:"STORAGE_BUCKET,default=test"`
	StorageBasePath string `env:"STORAGE_BASE_PATH,default=test"`
	// S3 config
	StorageS3Key      string `env:"STORAGE_S3_KEY,default=02tzwnQ8qYBRFdn1zzI8XwEXAMPLE"`
	StorageS3Secret   string `env:"STORAGE_S3_SECRET,default=02tzwnWAIRCBJwA7HtHUnsEXAMPLEKEY"`
	StorageS3Region   string `env:"STORAGE_S3_REGION,default=local"`
	StorageS3Endpoint string `env:"STORAGE_S3_ENDPOINT,default=localhost:9000"`
	StorageS3Ssl      bool   `env:"STORAGE_S3_SSL,default=false"`
	// Azure config
	StorageAzureAccount string `env:"STORAGE_AZURE_ACCOUNT,default=02tzwnQ8qYBRFdn1zzI8XwEXAMPLE"`
	StorageAzureKey     string `env:"STORAGE_AZURE_KEY,default=02tzwnWAIRCBJwA7HtHUnsEXAMPLEKEY"`
	// Local config
	StorageLocalPath           string `env:"STORAGE_LOCAL_PATH,default=/data/pdnd"`
	UserTokenValidationEnabled bool   `env:"USER_TOKEN_VALIDATION_ENABLED,default=true"`
	ValidationDataEnabled      bool   `env:"VALIDATION_DATA_ENABLED,default=true"`
	//PDND
	PdndEnv string `env:"PDND_ENV,default=collaudo"`
}

func LoadConfig(ctx context.Context) (serverConfig ServerConfig, err error) {
	err = envconfig.Process(ctx, &serverConfig)
	return
}

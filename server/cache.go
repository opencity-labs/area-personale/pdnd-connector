package server

import (
	"github.com/eko/gocache/lib/v4/cache"
	goCacheStore "github.com/eko/gocache/store/go_cache/v4"
	goCache "github.com/patrickmn/go-cache"
	"gitlab.com/opencontent/stanza-del-cittadino/pdnd-connector/server/models"
)

func startClientsCache(sctx *ServerContext) *cache.Cache[*models.ClientPDND] {
	serverConfig := sctx.ServerConfig()

	goCache := goCache.New(goCache.NoExpiration, serverConfig.CacheEviction)
	goCacheStore := goCacheStore.NewGoCache(goCache)

	cacheManager := cache.New[*models.ClientPDND](goCacheStore)

	return cacheManager
}

func startTenantsCache(sctx *ServerContext) *cache.Cache[*models.Tenant] {
	serverConfig := sctx.ServerConfig()

	goCache := goCache.New(goCache.NoExpiration, serverConfig.CacheEviction)
	goCacheStore := goCacheStore.NewGoCache(goCache)

	cacheManager := cache.New[*models.Tenant](goCacheStore)

	return cacheManager
}

func startConfigsCache(sctx *ServerContext) *cache.Cache[*models.Config] {
	serverConfig := sctx.ServerConfig()

	goCache := goCache.New(goCache.NoExpiration, serverConfig.CacheEviction)
	goCacheStore := goCacheStore.NewGoCache(goCache)

	cacheManager := cache.New[*models.Config](goCacheStore)

	return cacheManager
}

func startSdcJWKSPublicKeyCache(sctx *ServerContext) *cache.Cache[*models.SdcJWKSPublicKey] {
	serverConfig := sctx.ServerConfig()

	goCacheClient := goCache.New(goCache.NoExpiration, serverConfig.CacheEviction)
	goCacheStore := goCacheStore.NewGoCache(goCacheClient)

	cacheManager := cache.New[*models.SdcJWKSPublicKey](goCacheStore)

	return cacheManager
}

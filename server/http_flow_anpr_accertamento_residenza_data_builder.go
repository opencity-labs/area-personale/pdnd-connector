package server

import (
	"errors"

	"gitlab.com/opencontent/stanza-del-cittadino/pdnd-connector/server/models/anpr"
)

const FORMAT_RESIDENZA = "residenza"
const FORMAT_RESIDENZA_ARCHETIPO = "residenza_archetipo"

var AccertamentoResidenzaFormats = [2]string{FORMAT_RESIDENZA, FORMAT_RESIDENZA_ARCHETIPO}

type AccertamentoResidenzaDataBuilder struct {
	AccertamentoResidenzaResponse anpr.AccertamentoResidenza
}

func (r *AccertamentoResidenzaDataBuilder) GetFormatedData(format string) (*anpr.AccertamentoResidenzaFormattedData, error) {
	switch format {
	case FORMAT_RESIDENZA:
		return r.buildDataResponseFormatResidenza(), nil
	case FORMAT_RESIDENZA_ARCHETIPO:
		return r.buildDataResponseFormatResidenzaArchetipo(), nil
	}
	return nil, errors.New("invalid format for e-service accertamento residenza")
}

func (r *AccertamentoResidenzaDataBuilder) buildDataResponseFormatResidenza() *anpr.AccertamentoResidenzaFormattedData {
	data := anpr.AccertamentoResidenzaFormattedData{}
	if len(r.AccertamentoResidenzaResponse.ListaSoggetti.DatiSoggetto) > 0 {
		aDatiSoggettoData := r.AccertamentoResidenzaResponse.ListaSoggetti.DatiSoggetto[0]
		if len(aDatiSoggettoData.Residenza) > 0 {
			aResidenzaUserData := aDatiSoggettoData.Residenza[0]
			data.Address = StringPtr(aResidenzaUserData.Indirizzo.Toponimo.Specie + " " + aResidenzaUserData.Indirizzo.Toponimo.DenominazioneToponimo)
			data.HouseNumber = r.buildNumeroCivico(aResidenzaUserData.Indirizzo.NumeroCivico)
			data.Municipality = StringPtr(aResidenzaUserData.Indirizzo.Comune.NomeComune)
			data.County = StringPtr(aResidenzaUserData.Indirizzo.Comune.SiglaProvinciaIstat)
			data.PostalCode = StringPtr(aResidenzaUserData.Indirizzo.Cap)

		}
	}
	return &data
}

func (r *AccertamentoResidenzaDataBuilder) buildDataResponseFormatResidenzaArchetipo() *anpr.AccertamentoResidenzaFormattedData {
	data := anpr.AccertamentoResidenzaFormattedData{}
	if len(r.AccertamentoResidenzaResponse.ListaSoggetti.DatiSoggetto) > 0 {
		aDatiSoggettoData := r.AccertamentoResidenzaResponse.ListaSoggetti.DatiSoggetto[0]
		if len(aDatiSoggettoData.Residenza) > 0 {
			aResidenzaUserData := aDatiSoggettoData.Residenza[0]
			data.Address = StringPtr(aResidenzaUserData.Indirizzo.Toponimo.Specie + " " + aResidenzaUserData.Indirizzo.Toponimo.DenominazioneToponimo)
			data.HouseNumber = r.buildNumeroCivico(aResidenzaUserData.Indirizzo.NumeroCivico)
			data.Municipality = StringPtr(aResidenzaUserData.Indirizzo.Comune.NomeComune)
			data.County = StringPtr(aResidenzaUserData.Indirizzo.Comune.SiglaProvinciaIstat)
			if r.IsCountryItaly() {
				data.Country = StringPtr("Italia")
			}
			data.PostalCode = StringPtr(aResidenzaUserData.Indirizzo.Cap)
			data.MunicipalityIstatCode = StringPtr(aResidenzaUserData.Indirizzo.Comune.CodiceIstat)
			data.LocatorWithin = StringPtr(aResidenzaUserData.Indirizzo.NumeroCivico.CivicoInterno.Interno1)
		}
	}
	return &data
}

func (r *AccertamentoResidenzaDataBuilder) buildNumeroCivico(numCivico anpr.NumeroCivicoAR) *string {
	var numeroCivico string = numCivico.Numero
	if numCivico.Lettera != "" {
		numeroCivico += "/" + numCivico.Lettera
	}
	return &numeroCivico
}

func (r *AccertamentoResidenzaDataBuilder) IsCountryItaly() bool {
	for _, soggetto := range r.AccertamentoResidenzaResponse.ListaSoggetti.DatiSoggetto {
		for _, residenza := range soggetto.Residenza {
			if residenza.LocalitaEstera.IndirizzoEstero.Localita.DescrizioneStato != "" {
				return false
			}
		}
	}
	return true
}

package server

import (
	"encoding/json"
	"errors"
	"net/http"
	"strconv"

	"gitlab.com/opencontent/stanza-del-cittadino/pdnd-connector/server/models"
	"gitlab.com/opencontent/stanza-del-cittadino/pdnd-connector/server/models/anpr"
)

type HttpFlowValidateAccertamentoResidenza struct {
	Name           string
	Sctx           *ServerContext
	Client         *http.Client
	Request        ValidateAccertamentoResidenzaInput
	Param          anpr.ParametersAccertamentoResidenza
	SdcTokenClaims models.JwtClaims
	Err            error
	Msg            string
	FlowStatus     bool
	RsaPublicKey   string
	Response       anpr.ValidateAnpresponse
}

func (r *HttpFlowValidateAccertamentoResidenza) Exec() bool {
	r.Name = "HttpFlowValidateAccertamentoResidenza"

	config := r.Sctx.serverConfig
	r.FlowStatus = true &&
		r.validateToken() &&
		r.loadParam() &&
		r.validateInput() &&
		r.loadPublicKey() &&
		r.ValidatePayload()

	if r.FlowStatus {
		MetricsAnprTotalSuccesRequests.WithLabelValues(config.Environment, APPNAME).Inc()
		r.Sctx.LogHttpInfo().Str("request_type", "validazione e-service").Str("e_service_id", r.Param.EserviceID).Str("e_service", r.Param.EserviceName).Str("e_service_version", r.Param.EserviceVersion).Str("purpose_id", r.Param.PurposeID).Str("client_id ", r.Param.ClientID).Str("config_id ", r.Request.ConfigId).Str("tax_code ", obscureTaxCode(r.Request.FiscalCode)).Str("request_format ", r.Request.Format).Str("user_id ", r.SdcTokenClaims.ID).Str("tenant_id ", r.Param.TenantId).Str("client_ip:", GetClientIp(r.Sctx.ctx)).Str("timestamp", GetUTCtimestamp()).Str("flow", r.Name).Str("result", strconv.FormatBool(r.Response.Result)).Msg("")

	} else {
		MetricsAnprTotalErrorRequests.WithLabelValues(config.Environment, APPNAME).Inc()
		r.Sctx.LogHttpInfo().Str("request_type", "validazione e-service").Str("e_service_id", r.Param.EserviceID).Str("e_service", r.Param.EserviceName).Str("e_service_version", r.Param.EserviceVersion).Str("purpose_id", r.Param.PurposeID).Str("client_id ", r.Param.ClientID).Str("config_id ", r.Request.ConfigId).Str("tax_code ", obscureTaxCode(r.Request.FiscalCode)).Str("request_format ", r.Request.Format).Str("user_id ", r.SdcTokenClaims.ID).Str("tenant_id ", r.Param.TenantId).Str("client_ip:", GetClientIp(r.Sctx.ctx)).Str("timestamp", GetUTCtimestamp()).Str("flow", r.Name).Str("result", "ko").Str("error", r.Err.Error()).Msg("")
	}

	return r.FlowStatus
}

func (r *HttpFlowValidateAccertamentoResidenza) validateToken() bool {
	if r.Request.Token == "" {
		r.Err = errors.New("missing token")
		r.Msg = "missing token"
		return false
	}

	token := r.Request.Token

	if r.Sctx.serverConfig.UserTokenValidationEnabled {
		err := ValidateUserToken(r.Sctx, token, r.Request.FiscalCode)
		if err != nil {
			r.Err = err
			r.Msg = "invalid token"
			return false
		}
	}
	sdcTokenClaims, err := GetTokenClaims(token)
	if err != nil {
		r.Err = err
		r.Msg = "invalid token"
		return false
	}
	r.SdcTokenClaims = sdcTokenClaims

	return true
}

func (r *HttpFlowValidateAccertamentoResidenza) loadParam() bool {

	config, err := GetConfigById(r.Sctx, r.Request.ConfigId)
	if err != nil && err.Error() == "not found" {
		r.Sctx.LogHttpError().Stack().Err(err).Msg("client e-service config not found for config id : " + r.Request.ConfigId)
		r.Err = errors.New("config not found")
		r.Msg = "not found"
		return false
	}
	eServiceGeneralConfig, err := GetEserviceGeneralConfig(r.Sctx, config.EserviceID)
	if err != nil && err.Error() == "not found" {
		r.Sctx.LogHttpError().Stack().Err(err).Msg("e-service general config not found in list for e-service id: " + config.EserviceID)
		r.Err = errors.New("e-service general config not found in list")
		r.Msg = "not found"
		return false
	}

	client, err := GetClientPdndByClientId(r.Sctx, config.ClientID)
	if err != nil && err.Error() == "not found" {
		r.Sctx.LogHttpError().Stack().Err(err).Msg("client config not found for client id : ")
		r.Err = errors.New("client config not found")
		r.Msg = "not found"
		return false
	}

	r.Param.ClientID = client.ID
	r.Param.PurposeID = config.PurposeID
	r.Param.EserviceName = eServiceGeneralConfig.Name
	r.Param.EserviceVersion = eServiceGeneralConfig.Version
	r.Param.TenantId = client.TenantID
	r.Param.EserviceID = config.EserviceID
	return true
}

func (r *HttpFlowValidateAccertamentoResidenza) validateInput() bool {
	err := checkValidateAccertamentoResidenzaInput(r.Request)
	if err != nil {
		r.Err = err
		r.Msg = "bad request"
		return false
	}

	return true
}

func (r *HttpFlowValidateAccertamentoResidenza) loadPublicKey() bool {
	config, err := GetConfigById(r.Sctx, r.Request.ConfigId)
	if err != nil && err.Error() == "not found" {
		r.Sctx.LogHttpError().Stack().Err(err).Msg("client e-service config not found for config id : " + r.Request.ConfigId)
		r.Err = errors.New("client e-service config not found")
		r.Msg = "not found"
		return false
	}
	client, err := GetClientPdndByClientId(r.Sctx, config.ClientID)
	if err != nil && err.Error() == "not found" {
		r.Sctx.LogHttpError().Stack().Err(err).Msg("client config not found for client id : ")
		r.Err = errors.New("client config not found")
		r.Msg = "not found"
		return false
	}
	if err != nil {
		r.Sctx.LogHttpError().Stack().Err(err)
		r.Err = err
		r.Msg = "internal error"
		return false
	}
	r.RsaPublicKey = client.PublicKey
	return true
}

func (r *HttpFlowValidateAccertamentoResidenza) ValidatePayload() bool {
	if !r.Sctx.serverConfig.ValidationDataEnabled {
		r.Response.Result = true
		return true
	}

	JsonData, err := json.Marshal(r.Request.Data)
	if err != nil {
		r.Err = errors.New("error marshalling response jsonData: " + err.Error())
		r.Msg = "internal error"
		return false
	}
	err = UnsignDigest(r.RsaPublicKey, string(JsonData), *r.Request.Meta.Signature)
	if err != nil && err.Error() == "crypto/rsa: verification error" {
		r.Response.Result = false
		return true
	}
	if err != nil && err.Error() != "crypto/rsa: verification error" {
		r.Err = errors.New("error verifying the signature: " + err.Error())
		r.Msg = "internal error"
		return false
	}
	r.Response.Result = true
	return true
}

package server

import (
	"encoding/json"
	"errors"
	"io"
	"os"

	"gitlab.com/opencontent/stanza-del-cittadino/pdnd-connector/server/models"
)

/*
todo
devo unire le due funzioni seguenti
ricordarsi di creare il path all'interno del volume
*/
func GetEserviceGeneralConfig(sctx *ServerContext, eserviceId string) (models.EService, error) {
	eServiceList := sctx.eServiceList
	for _, eService := range eServiceList.Data {
		if eService.ID == eserviceId {
			return eService, nil
		}
	}
	return models.EService{}, errors.New("value not found")
}

func GetEserviceListByEnv(sctx *ServerContext) (*models.EServiceList, error) {
	serverConfig := sctx.ServerConfig()
	fileStorage := sctx.FileStorage()
	basePath := serverConfig.StorageBasePath + "/e_services/"
	var eserviceList *models.EServiceList

	itemPath := basePath + serverConfig.PdndEnv + ".json"
	item, err := fileStorage.Item(itemPath)
	if err != nil {
		return eserviceList, err
	}

	itemContent, err := item.Open()
	if err != nil {
		return eserviceList, err
	}
	defer itemContent.Close()

	itemBytes, err := io.ReadAll(itemContent)
	if err != nil {
		return eserviceList, err
	}

	if err := json.Unmarshal(itemBytes, &eserviceList); err != nil {
		return eserviceList, err
	}

	return eserviceList, nil
}

func GetEserviceList(sctx *ServerContext) (models.EServiceList, error) {

	var eserviceList models.EServiceList
	env := sctx.serverConfig.PdndEnv
	if env == "produzione" {
		file, err := os.Open("e_service_configuration/interoperabilita.json")
		if err != nil {
			return eserviceList, err
		}
		defer file.Close()
		decoder := json.NewDecoder(file)
		err = decoder.Decode(&eserviceList)
		if err != nil {
			return eserviceList, err
		}
		return eserviceList, nil
	}

	if env == "collaudo" {
		file, err := os.Open("e_service_configuration/collaudo.json")
		if err != nil {
			return eserviceList, err
		}

		defer file.Close()
		decoder := json.NewDecoder(file)
		err = decoder.Decode(&eserviceList)
		if err != nil {
			return eserviceList, err
		}
		return eserviceList, nil
	}

	return eserviceList, errors.New("client environment not supported. Accepted values are 'collaudo', 'produzione' ")
}

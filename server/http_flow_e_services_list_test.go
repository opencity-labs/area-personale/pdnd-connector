package server

import (
	"reflect"
	"testing"

	"gitlab.com/opencontent/stanza-del-cittadino/pdnd-connector/server/models"
)

func TestHttpFlowEservicesList_getPaginatedList(t *testing.T) {
	eservices := []models.EService{
		{ID: "1", Version: "v1", Name: "Service 1", AudClientAssertion: "client_assertion_1", Audience: "audience_1", Endpoint: "endpoint_1"},
		{ID: "2", Version: "v1", Name: "Service 2", AudClientAssertion: "client_assertion_2", Audience: "audience_2", Endpoint: "endpoint_2"},
		{ID: "3", Version: "v1", Name: "Service 3", AudClientAssertion: "client_assertion_3", Audience: "audience_3", Endpoint: "endpoint_3"},
		{ID: "4", Version: "v1", Name: "Service 4", AudClientAssertion: "client_assertion_4", Audience: "audience_4", Endpoint: "endpoint_4"},
	}
	r := &HttpFlowEservicesList{}

	// Test case 1
	startIndex := 1
	limit := 2
	expected := []models.EService{
		{ID: "2", Version: "v1", Name: "Service 2", AudClientAssertion: "client_assertion_2", Audience: "audience_2", Endpoint: "endpoint_2"},
		{ID: "3", Version: "v1", Name: "Service 3", AudClientAssertion: "client_assertion_3", Audience: "audience_3", Endpoint: "endpoint_3"},
	}
	result := r.getPaginatedList(startIndex, limit, eservices)
	if !reflect.DeepEqual(result, expected) {
		t.Errorf("Expected %v, but got %v", expected, result)
	}

	// Test case 2
	startIndex = 0
	limit = 2
	expected = []models.EService{
		{ID: "1", Version: "v1", Name: "Service 1", AudClientAssertion: "client_assertion_1", Audience: "audience_1", Endpoint: "endpoint_1"},
		{ID: "2", Version: "v1", Name: "Service 2", AudClientAssertion: "client_assertion_2", Audience: "audience_2", Endpoint: "endpoint_2"},
	}
	result = r.getPaginatedList(startIndex, limit, eservices)
	if !reflect.DeepEqual(result, expected) {
		t.Errorf("Expected %v, but got %v", expected, result)
	}

	// Test case 3: endIndex > len(eserviceArray), should adjust to len(eserviceArray)
	startIndex = 2
	limit = 2
	expected = []models.EService{
		{ID: "3", Version: "v1", Name: "Service 3", AudClientAssertion: "client_assertion_3", Audience: "audience_3", Endpoint: "endpoint_3"},
		{ID: "4", Version: "v1", Name: "Service 4", AudClientAssertion: "client_assertion_4", Audience: "audience_4", Endpoint: "endpoint_4"},
	}
	result = r.getPaginatedList(startIndex, limit, eservices)
	if !reflect.DeepEqual(result, expected) {
		t.Errorf("Expected %v, but got %v", expected, result)
	}
	// Test case 4
	startIndex = 1
	limit = 1
	expected = []models.EService{
		{ID: "2", Version: "v1", Name: "Service 2", AudClientAssertion: "client_assertion_2", Audience: "audience_2", Endpoint: "endpoint_2"},
	}
	result = r.getPaginatedList(startIndex, limit, eservices)
	if !reflect.DeepEqual(result, expected) {
		t.Errorf("Expected %v, but got %v", expected, result)
	}
	// Test case 5
	startIndex = 4
	limit = 1
	expected = []models.EService{}
	result = r.getPaginatedList(startIndex, limit, eservices)
	if !reflect.DeepEqual(result, expected) {
		t.Errorf("Expected %v, but got %v", expected, result)
	}

	// Test case 5: empty eserviceArray
	startIndex = 0
	eservices = []models.EService{}
	expected = []models.EService{}
	result = r.getPaginatedList(startIndex, limit, eservices)
	if !reflect.DeepEqual(result, expected) {
		t.Errorf("Expected %v, but got %v", expected, result)
	}
}

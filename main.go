package main

import "gitlab.com/opencontent/stanza-del-cittadino/pdnd-connector/server"

func main() {
	serverContext := server.InitServerContext()

	serverContext.StartProcess("http server", server.StartHttpServer)

	serverContext.Daemonize()
}

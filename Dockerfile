FROM golang:1.23-bookworm AS builder

ARG APP_NAME="pdnd-connector"
ENV APP_NAME=$APP_NAME

ARG HTTP_PORT="8000"
ENV HTTP_PORT=$HTTP_PORT

WORKDIR $GOPATH/src/$APP_NAME

COPY go.mod go.sum ./

RUN go mod download
RUN go mod verify

COPY . .

RUN go build -a -v -o /go/bin/$APP_NAME

###

FROM debian:bookworm-slim

RUN apt-get update \
  && apt-get install -y --no-install-recommends ca-certificates curl

RUN update-ca-certificates

ARG APP_NAME="pdnd-connector"
ENV APP_NAME=$APP_NAME

ARG HTTP_PORT="8000"
ENV HTTP_PORT=$HTTP_PORT

#COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

COPY --from=builder /go/bin/$APP_NAME /go/bin/$APP_NAME
COPY --from=builder /go/src/$APP_NAME/e_service_configuration /e_service_configuration

CMD ["/go/bin/pdnd-connector"]

EXPOSE $HTTP_PORT

HEALTHCHECK --interval=30s --timeout=5s CMD curl -f http://localhost:$HTTP_PORT/status || exit 1
